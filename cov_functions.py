import pandas as pd
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
from datetime import datetime, date, timedelta
import os
import subprocess
from scipy.spatial.distance import correlation as Dcorr
from scipy.stats import zscore, pearsonr
import patsy
from sklearn.linear_model import LinearRegression as lm
from statsmodels.regression.linear_model import OLS
from statsmodels.tools import add_constant
from groo.groo import get_root
root_dir = get_root(".root_dir_covid")



def report_basic_stats(lg, prolific_uk_d=pd.DataFrame(), prolific_uk2_d=pd.DataFrame(), prolific_be_d=pd.DataFrame(), sess=999, root_dir=""):
    if sess==1:
        frames = [prolific_uk_d, prolific_uk2_d, prolific_be_d]
        frames_uk = [prolific_uk_d, prolific_uk2_d]
        prolific_uk_d = pd.concat(frames_uk)
        prolific_d = pd.concat(frames)
    else:
        frames = [prolific_uk_d, prolific_be_d]
        prolific_d = pd.concat(frames)

    lg.info(" == DATA FROM PROLIFIC ==")
    lg.info(" Prolific: UK approved subjects:"+str(prolific_uk_d[prolific_uk_d.status=="APPROVED"].shape[0]) )
    lg.info(" Prolific: Berlin approved subjects:"+str(prolific_be_d[prolific_be_d.status=="APPROVED"].shape[0]) )
    lg.info(" Prolific: Overall approved subjects:"+str(prolific_d[prolific_d.status=="APPROVED"].shape[0]) )

    #if sess==1:
        #err_d = pd.read_csv(os.path.join(root_dir, "data", "session1", "accepted_but_exclude.csv"))
        #lg.info(" Participants that were accepted (paid) but are excluded (mostly due to initial errors): "+str(err_d.shape[0]))
    return lg


def corrfunc(x, y, tests=["pearson"], drop_missing=False, ax=None, **kws):
    if (ax is None):
        ax = plt.gca()
        
        
    if drop_missing:
        d = pd.DataFrame({'x':np.array(x), 'y':np.array(y)})
        d = d.dropna()
        x = d["x"]
        y = d["y"]

    #["pearson", "spearman", "kendall", "distcor"]
    ycoord = 0.1*len(tests) + 0.05
    if "pearson" in tests:
        r,p = stats.pearsonr(x, y)
        t = plt.text(0.4, ycoord, "Pearson r = {:.2f}, p={:.2g}".format(r,p), transform=ax.transAxes, fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='purple'))
        ycoord = ycoord-0.1
        
    if "spearman" in tests:
        r,p = stats.spearmanr(x, y)
        t = plt.text(0.4, ycoord, "Spearman r = {:.2f}, p={:.2g}".format(r,p), transform=ax.transAxes, fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='purple'))
        ycoord = ycoord-0.1
    
    if "distcor" in tests:
        r2 = Dcorr(x,y)
        t = plt.text(0.4, 0.05, "Dist. corr = {:.2f}".format(r2), transform=ax.transAxes, fontsize=10)
        t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='blue'))
    
def corrfunc_r(x, y, **kws):
    r, _ = stats.pearsonr(x, y)
    ax = plt.gca()
    ax.annotate("r = {:.2f}".format(r),
                xy=(.1, .9), xycoords=ax.transAxes)
    
def demean(x):
    x = x - np.nanmean(x)
    
def zscore(x):
    x = x - np.nanmean(x) / np.nanstd(x)

def calc_delta_multiple_vars(df, vec):
    for v in vec:
        p_t = np.array(df[v].iloc[:-1])
        p_t_plus1 = np.array(df[v].iloc[1:])
        delta = p_t_plus1 - p_t
        delta = np.append(delta, np.nan)
        df[v] = delta 
    return df
    
def run_VAR_single(dfin, depvar, indvar, window, datatype="data"):
    """
        depvar ... dependent variable (Y in regression equation)
        indvar ... single column that will be regressed with terms for t-window
        window ... how back in time should the regression run
        datatype ... "data" = just data; "changes" = changes between data points
        
        returns regression coefficients for intercept, t, t-1, t-window
    """
    labels = ['t_min'+str(x) for x in range(window,-1,-1)]
    
    if sum(np.isnan(dfin[indvar])) >= 1: #np.round(dfin[indvar].shape[0]/2):
        outdf = pd.DataFrame(np.zeros(window+2)*np.nan) 
        aic = np.nan
        r2 = np.nan
    else:
        dfin = dfin.reset_index()
        if datatype == "changes":
            dfin = calc_delta_multiple_vars(dfin, [depvar, indvar])
          
        N = dfin[indvar].shape[0] #
        x = np.array(dfin[indvar].values)
        df = pd.DataFrame(rolling_window(x, N-window)).T
        df.columns = labels
        df.index = range(window,N,1)
        df.loc[range(window,N,1), depvar] =dfin.loc[range(window,N,1),depvar] 
        eq = depvar + ' ~ ' + ' + '.join(labels)
        [Y,X] = patsy.dmatrices(eq, df)
        regr = OLS(Y, add_constant(X)).fit()
        
        # Calculate AIC
        #k = len(regr.params)
        aic = regr.aic #-2*(OLS(Y, add_constant(X)).loglike(regr.params)) + 2*(k)
        r2 = regr.rsquared#OLS(Y, add_constant(X)).score(regr.params)

        outdf = pd.DataFrame(regr.params)
        
    outdf = outdf.T
    outdf.columns = ["intercept"]+labels
    
    # change the order of the colunms
    reorder_labels = ['t_min'+str(x) for x in range(0,window+1,1,)]
    outdf = outdf.loc[:,["intercept"]+reorder_labels]
    outdf["aic"] = aic
    outdf["r2"]  = r2
    return outdf.reset_index()
    
def rolling_window(a, window):
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def pearsonr_ci(x,y,alpha=0.05):
    ''' calculate Pearson correlation along with the confidence interval using scipy and numpy
    Parameters
    ----------
    x, y : iterable object such as a list or np.array
      Input for correlation calculation
    alpha : float
      Significance level. 0.05 by default
    Returns
    -------
    r : float
      Pearson's correlation coefficient
    pval : float
      The corresponding p value
    lo, hi : float
      The lower and upper bound of confidence intervals
    '''

    r, p = stats.pearsonr(x,y)
    r_z = np.arctanh(r)
    se = 1/np.sqrt(x.size-3)
    z = stats.norm.ppf(1-alpha/2)
    lo_z, hi_z = r_z-z*se, r_z+z*se
    lo, hi = np.tanh((lo_z, hi_z))
    return r, p, lo, hi
    
def clean_maind(data, conf,  lg):
    data_path = os.path.join(conf["root_dir"], "data", conf["sess_str"]+"session"+str(conf["session"])) +"/" # "../../data/"+conf["sess_str"]+"session"+str(conf["session"])+"/"
    trat_q_db = pd.read_csv(os.path.join(conf["root_dir"], "output", "questionnaires", "trait_questionnaires_questions_overview.csv"))
    
    bashCommand = 'mkdir '+data_path+"individual_questionnaires/"
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

    # remove duplicates
    # get unique IDs

    data=data.set_index("PROLIFICID")
    dup = data.groupby("PROLIFICID").filter(lambda x: len(x) > 1).index.unique()
    lg.info(" Number of unique entries: "+str( len(data.index.unique())  ))
    temp_d = pd.DataFrame()
    #data.to_csv(data_path+'working_data/temp_data.csv')
    # find duplicates and keep only those that got to page 8/12
    # if there are more than 1 finished cases take the first one
    if conf["session_type"] == "long":
        lastpage = 12
    elif conf["session_type"] == "short":
        lastpage = 8
    elif conf["session_type"] == "extra":
        lastpage = 7

    for sid in dup:
        dupd = data.loc[[sid]]
        dupd_fltd = dupd[dupd.lastpage==lastpage]
        # If they did the assessment more times and succeeded take the first one one
        if dupd_fltd.shape[0] > 1:
            dupd_fltd = dupd_fltd.iloc[0,:]
        if dupd_fltd.shape[0] == 1:
            temp_d = pd.concat([temp_d, dupd_fltd], axis=0)


    # drop duplicates from main data frame and merge with temp_d
    data = pd.concat([data.drop(dup,axis=0), temp_d])
    df_pt1 = data.loc[:, ["submitdate", "lastpage"]]
    df_pt2 = data.loc[:,"SESSIONID":"Q8I5" ]
    df = df_pt1.join(df_pt2)# pd.concat([df_pt1, df_pt2], axis=0, sort=False)
    # Rename columns to be more meaningful
    df = df.rename(columns={"Q1":"sr_age", "Q2":"sr_gender", "Q3":"sr_postcode", "Q6I1[SQ001]":"q6_me_inf",
                            "Q6I1[SQ002]":"q6_close_person_inf", "Q6I1[SQ003]":"q6_close_person_died",
                            "Q6I2[SQ001]":"q6_econ_impact_me", "Q6I2[SQ002]":"q6_econ_impact_closep",
                            "Q6I2[SQ003]": "q6_work_home", "Q6I2[SQ004]":"q6_apply_soc_dist", "Q6I2[SQ005]":"q6_risk_group",
                            "Q6I2[SQ006]": "q6_risk_group_closep", "Q6I3": "q6_houshold_membs", "Q6I4": "q6_media_freq",
                            "Q6I5": "q6_media_name", "Q6I6[SQ001]": "q6_media_valence", "Q7I1[SQ001]": "q7_worry_infected",
                            "Q7I1[SQ002]": "q7_worry_die", "Q7I1[SQ003]": "q7_worry_econ_impact", "Q7I1[SQ004]": "q7_worry_sthg_bad",
                            "Q7I1[SQ005]": "q7_worry_insuf_help", "Q7I1[SQ006]": "q7_worry_closep_inf", "Q7I1[SQ007]": "q7_closep_die",
                            "Q7I1[SQ008]": "q7_worry_shortage", "Q7I2[SQ001]": "q7_period_rel_danger", "Q7I3[SQ001]": "q7_period_rel_safety",
                            "Q7I4[SQ001]": "q7_initial_surprise", "Q7I5[SQ001]": "q7_initial_scared",
                            "Q7I6[SQ001]": "q7_people_overreact", "Q7I7[SQ001]": "q7_vir_not_as_dangerous",
                            "Q7I8[SQ001]": "q7_vir_made_lab", "Q7I9": "q7_inf_worry_frequency", "Q7I10": "q7_diff_beh_freq",
                            "Q7I11[SQ001]": "q7_beh_wash_hands", "Q7I11[SQ002]": "q7_beh_avoid_ppl", "Q7I11[SQ003]": "q7_beh_avoid_public_places",
                            "Q7I12[SQ001]": "q7_anx_touching_surf", "Q7I12[SQ002]": "q7_anx_stand_close_to_ppl",
                            "Q7I12[SQ003]": "q7_anx_eating_food_out", "Q7I12[SQ004]": "q7_anx_public_transp",
                            "Q7I12[SQ005]": "q7_anx_visit_doc", "Q7I12[SQ006]": "q7_anx_another_beh",
                            "Q8I1[SQ001]": "q8_prob_inf_me", "Q8I1[SQ002]": "q8_prob_die_me", "Q8I1[SQ003]": "q8_prob_econ_imp_me",
                            "Q8I1[SQ004]": "q8_prob_inf_closep", "Q8I1[SQ005]": "q8_prob_die_closep", "Q8I1[SQ006]": "q8_prob_inf_avgp",
                            "Q8I2": "q8_t_pand_end", "Q8I3": "q8_t_life_back_norm", "Q8I4": "q8_secondw", "Q8I4b": "q8_t_secondw_when",
                            "Q8I5": "q8_t_econ_back_norm"

                        } )
    
    # drop this manual entry - it seems to make mess in the processing
    if "q6_media_name" in df.columns:
        df=df.drop(columns=["q6_media_name"])
    if ((conf["session"] == 3) & ("extra" not in conf["session_type"])):
        df=df.drop(columns=["Q6I9[SQ001]","Q6I9[SQ002]"])
    
    # Re-code variables, so that they can be used for analysis
    df["submitdate"] = pd.to_datetime(df.submitdate).dt.strftime("%Y-%m-%d");
    df = df.replace({"sr_gender":{"Female": "F", "Male": "M"}})
    df["sr_postcode"] = df.sr_postcode.str.upper().str.replace(" ","")

    # check if a column exists (useful for development, otherwise False)
    check = False
    question = "Q7I13[SQ001]"
    if check:
        print(question+" sess "+str(conf["session"])+" "+conf["session_type"])
        if question in df.columns:
            print("   exists")
        else:
            print("   doesn't exist")
    ####################
    
    #### for questions added on session 4
    if ((conf["session"] > 3) & ("extra" not in conf["session_type"])) or ((conf["session"] > 0) & ("extra" in conf["session_type"])):
        df = df.rename(columns={"Q6I7":"q8_est_daily_cases", "Q6I10[SQ001]":"q7_thinkof_nocases", "Q6I8[SQ001]":"q7_memory_initial_worry", "Q6I8[SQ002]":"q7_memory_initial_something_bad",
                               "Q6I9[SQ001]":"q8_memory_prob_infected","Q6I9[SQ002]":"q8_memory_prob_avg_person_infected",
                               "Q6I10[SQ002]":"q7_thinkof_nodeaths", "Q6I10[SQ003]":"q7_thinkof_strain_healthsys", "Q6I10[SQ004]":"q7_thinkof_mishandled", 
                               "Q6I10[SQ005]":"q7_thinkof_famimpact", "Q6I10[SQ006]":"q7_thinkof_jobsimpact", "Q6I10[SQ007]":"q7_thinkof_safety", 
                               "Q6I10[SQ008]":"q7_thinkof_wantitover", "Q6I10[SQ009]":"q7_thinkof_vaccine", 
                               "Q7I13[SQ001]": "q7_angry_cough", "Q7I14[SQ001]": "q7_scared_coughed", "Q7I15[SQ001]": "q7_covid_hoax"})
        
        q_list = ["q7_thinkof_nocases","q7_thinkof_nodeaths", "q7_thinkof_strain_healthsys", "q7_thinkof_mishandled", "q7_thinkof_famimpact", "q7_thinkof_jobsimpact", 
                  "q7_thinkof_safety", "q7_thinkof_wantitover", "q7_thinkof_vaccine"]
        for i in q_list:
            df[i] = df[i].replace({"Yes": 1, "No": 0})
        
        # Q8 estimate of new daily cases over the past week (nice proxy to see how informed they are)
      #  print(np.array(df["q8_est_daily_cases"]))
        df["q8_est_daily_cases"]=df["q8_est_daily_cases"].str.replace('+', '').str.replace('.', '').str.replace(',', '').str.replace(r'\D', '').replace(np.nan,"")#.astype(float)
       # print(np.array(df["q8_est_daily_cases"]))
        #df["q8_est_daily_cases"] = df["q8_est_daily_cases"].astype('int')
        #print(np.array(df["q8_est_daily_cases"].replace(np.nan, '').astype(float)))
        
        q_list = ["q7_memory_initial_worry","q7_memory_initial_something_bad", "q7_angry_cough",  "q7_scared_coughed","q7_covid_hoax" ]
        for i in q_list:
            df[i] = df[i].replace({"1 - Strongly disagree": 1, "2 - Disagree": 2, "3 - Somewhat disagree": 3, "4 - Neither agree nor disagree":4, "5 - Somewhat agree":5, "6 - Agree":6, "7 - Strongly agree":7} )
            
        q_list = ["q8_memory_prob_infected", "q8_memory_prob_avg_person_infected"]
        for i in q_list:
            df[i] = df[i].str.replace(r'\D', '').replace("NaN","").astype(float)
            
            
    #### for questions added on session 6 
    if ((conf["session"] > 5) & ("extra" not in conf["session_type"])) or ((conf["session"] > 10) & ("extra" in conf["session_type"])):
        df = df.rename(columns={"Q6I1b":"q6_diagnosis_type"})
            
        # Q6 those that experienecd covid - how was it diagnosed? 
        df["q6_diagnosis_type"] = df["q6_diagnosis_type"].replace({"I was tested for COVID-19 and the test was positive":"positive_test", 
                                                                   "I was not tested, it was a self-diagnosis based on the symtoms": "self_diagnosis", 
                                                                   "I was not tested, the diagnosis was made by a doctor based on the symptoms": "doctor_diagnosis"})
        
    #### for questions added on session 7 
    if ((conf["session"] > 6) & ("extra" not in conf["session_type"])): 
        df = df.rename(columns={"Q8I6[SQ001]":"q8_second_wave_starting"})
        #print(np.array(df["q8_second_wave_starting"]))
        for i in ["q8_second_wave_starting"]:
            df[i] = df[i].replace({"Not at all": 1, "Somewhat": 2,  "Moderately so": 3, "Very much so": 4})

    #### for questions added on session 14 (but not for extra survey)
    if ((conf["session"] > 13) & ("extra" not in conf["session_type"])):
        df = df.rename(columns={"Q6I11":"q8_exists_vaccine", "Q6I11b":"q8_vaccine_first_ready", "Q6I11c":"q8_vaccine_for_everyone", "Q6I12[SQ001]": "q8_ifvac_willing_vacc", 
                               "Q6I12[SQ002]": "q8_ifvac_relieved", "Q6I12[SQ003]": "q8_ifvac_stop_worrying", "Q6I12[SQ004]": "q8_ifvac_notwilling_vacc_vulngroup", 
                               "Q6I12[SQ005]": "q8_ifvac_notwilling_vacc_unsafe", "Q6I12[SQ006]": "q8_ifvac_notwilling_vacc_other"})
        #for i in ["q8_exists_vaccine"]:
        #    df[i] = df[i].replace({"Yes": 1, "No": 0})
            
        for i in ["q8_vaccine_first_ready", "q8_vaccine_for_everyone"]:
            df[i] = pd.to_datetime(df[i])
            df[i+"_days"] = (df[i]-pd.to_datetime(df["submitdate"])).astype('timedelta64[D]')
        
        q_list =["q8_ifvac_willing_vacc", "q8_ifvac_relieved", "q8_ifvac_stop_worrying", "q8_ifvac_notwilling_vacc_vulngroup", "q8_ifvac_notwilling_vacc_unsafe", "q8_ifvac_notwilling_vacc_other"];
        for i in q_list:
            df[i] = df[i].replace({"1 - Strongly disagree": 1, "2 - Disagree": 2, "3 - Somewhat disagree": 3, "4 - Neither agree nor disagree":4, "5 - Somewhat agree":5, "6 - Agree":6, "7 - Strongly agree":7} )
        
        
    #### for questions present in all surveys
    # Q6 1 - 7
    q_list = ["q6_econ_impact_me", "q6_econ_impact_closep",	"q6_work_home", "q6_apply_soc_dist", "q6_risk_group","q6_risk_group_closep"]
    for i in q_list:
        df[i] = df[i].replace({"1 - Does not apply": 1, "2": 2, "3": 3, "4":4, "5":5, "6":6, "7 - Strongly applies":7} )

    # Q6: household members
    df["q6_houshold_membs"] = df["q6_houshold_membs"].replace({"5+":5})

    # Q6 media cnsumption frequency
    df["q6_media_freq_num"] =  df["q6_media_freq"].replace({"Multiple times per day":5, "Once a day": 4, "3-4 times a week":3, "Once a week": 2, "Few times a month": 1, "Less than few times a month":0})

    # Q6 media valence
    df["q6_media_valence"] = df["q6_media_valence"].replace({"-3 (very negative)":-3, "-2": -2, "-1": 1, "0 (neutral)":0, "1":1, "2":2, "3 (very positive)":3})

    # Q6 media compound
    df["q6_media_total"] = df["q6_media_valence"]*(df["q6_media_freq_num"]/5) # This coding ensures that the values are between -3 and +3
    
    # Q6 median name 
    #df["q6_media_name"] = df["q6_media_name"].str.replace(',', ' ')
    
    # Q7: worries
    q_list = ["q7_worry_infected","q7_worry_die","q7_worry_econ_impact","q7_worry_sthg_bad","q7_worry_insuf_help","q7_worry_closep_inf","q7_closep_die","q7_worry_shortage","q7_period_rel_danger","q7_period_rel_safety","q7_initial_surprise","q7_initial_scared","q7_people_overreact","q7_vir_not_as_dangerous","q7_vir_made_lab"]
    for i in q_list:
        df[i] = df[i].replace({"1 - Strongly disagree": 1, "2 - Disagree": 2, "3 - Somewhat disagree": 3, "4 - Neither agree nor disagree":4, "5 - Somewhat agree":5, "6 - Agree":6, "7 - Strongly agree":7} )

    # Difference between safety and danger
    df["q7_safety_danger_diff"] = df["q7_period_rel_safety"] - (8 - df["q7_period_rel_danger"])
        
    # Q7: worry q7_inf_worry_frequency
    q_list = ["q7_inf_worry_frequency", "q7_diff_beh_freq"]
    for i in q_list:
        df[i+"_num"] = df[i].replace({"Never":0, "On one or several days":1, "On about half the days":2, "Nearly every day (more than half the days)":3})

    # Q7: behaviours + anxiety
    q_list = ["q7_beh_wash_hands", "q7_beh_avoid_ppl", "q7_beh_avoid_public_places","q7_anx_touching_surf", "q7_anx_stand_close_to_ppl", "q7_anx_eating_food_out", "q7_anx_public_transp", 	"q7_anx_visit_doc",	"q7_anx_another_beh"]
    for i in q_list:
        df[i] = df[i].replace({"1 - Strongly disagree": 1, "2 - Disagree": 2, "3 - Somewhat disagree": 3, "4 - Neither agree nor disagree":4, "5 - Somewhat agree":5, "6 - Agree":6, "7 - Strongly agree":7} )

    # Q8 Probability ratings
    q_list = ["q8_prob_inf_me","q8_prob_die_me","q8_prob_econ_imp_me","q8_prob_inf_closep","q8_prob_die_closep","q8_prob_inf_avgp"]
    for i in q_list:
        df[i] = df[i].str.replace(r'\D', '').replace("NaN","").astype(float)

    # Q8 temporal expectations
    q_list = ["q8_t_pand_end",	"q8_t_life_back_norm", "q8_t_secondw_when",	"q8_t_econ_back_norm"]
    for i in q_list:
        df[i] = pd.to_datetime(df[i])
        df[i+"_days"] = (df[i]-pd.to_datetime(df["submitdate"])).astype('timedelta64[D]')

    # Q6 & Q8 and from YES/NO 10 1/0
    q_list = ["q6_me_inf", "q6_close_person_inf", "q6_close_person_died", "q8_secondw"]
    for i in q_list:
        df[i] = df[i].replace({"Yes": 1, "No": 0})
        df[i+'_resc'] = (df[i]*6) + 1  

    # Add collapsed measures
    # objective severity
    df["obj_severity"] = df[["q6_me_inf_resc", "q6_close_person_inf_resc", "q6_close_person_died_resc", "q6_econ_impact_me", "q6_econ_impact_closep"]].sum(axis=1)
    
    if ((conf["session"] > 3) & ("extra" not in conf["session_type"])) or ((conf["session"] > 0) & ("extra" in conf["session_type"])):
        # covid-related thoughts (only since session 4)
        q_list = ["q7_thinkof_nocases","q7_thinkof_nodeaths", "q7_thinkof_strain_healthsys", "q7_thinkof_mishandled", "q7_thinkof_famimpact", "q7_thinkof_jobsimpact", 
                  "q7_thinkof_safety", "q7_thinkof_wantitover", "q7_thinkof_vaccine"]
        df["covid_thoughts"] = df[q_list].sum(axis=1)
        
        q_list =["q7_covid_hoax", "q7_people_overreact", "q7_vir_not_as_dangerous", "q7_vir_made_lab"]
        df["skepticism"] = df[q_list].sum(axis=1)
    
    # COVID-related worry
    df["covid_worry"] = df.loc[:,"q7_worry_infected":"q7_worry_shortage"].sum(axis=1)

    # COVID-related avoidance behaviours
    df["avoid_beh"] = df.loc[:,"q7_beh_wash_hands":"q7_beh_avoid_public_places"].sum(axis=1)

    # COVID-related avoidance anxiety
    df["avoid_anx"] = df.loc[:,"q7_anx_touching_surf":"q7_anx_another_beh"].sum(axis=1)
    
    # COVID-related avoidance overall
    df["avoid_all"] = df.loc[:,"q7_beh_wash_hands":"q7_anx_another_beh"].sum(axis=1)
    
    # COVID-probability estimates
    df["prob_est"] = df.loc[:,"q8_prob_inf_me":"q8_prob_inf_avgp"].sum(axis=1)

    # COVID-end time estimate - only one left at mean, it's not a questionnaire score
    df["covid_end_est"] = df.loc[:, df.columns.intersection(["q8_t_pand_end_days", "q8_t_econ_back_norm", "q8_t_life_back_norm_days"]) ].mean(axis=1)

    # centered measure of objective "state"
    #df["state_severity"] = df["q6_me_inf_resc"]-0.5 + df["q6_close_person_inf_resc"]-0.5 + df["q6_close_person_died_resc"]

    if conf["session_type"] == "long":
        # Trait anxiety STAI
        ta = data.loc[:, "Q9I1[SQ001]":"Q9I20[SQ001]"]

        ta = ta.dropna()
        for c in ta.columns:
            ta[c] = ta[c].replace({"1 - Almost never": 1, "2 - Sometimes": 2,  "3 - Often": 3, "4 - Almost always": 4})
        stai_scoring = [-1, 1, -1, 1, 1, -1, -1, 1, 1, -1, 1, 1, -1, -1, 1 ,-1, 1, 1, -1, 1] #1=normal -1=reverse
        stai_key = { 'item': np.arange(21,41), 'scoring': stai_scoring}
        stai_key = pd.DataFrame.from_dict(stai_key)
    
        ta_unrecoded = ta.copy()
        for i, sub in enumerate(ta.index):
            idx = np.array(stai_key.scoring.isin([-1])).astype(bool)
            d = np.array(ta.loc[sub,])
            d[idx] = 5 - d[idx]
            ta.loc[sub,] = d

        
        ta["ta"] = ta.sum(axis=1,  min_count=1)
        ta_unrecoded["ta"] = ta_unrecoded.sum(axis=1,  min_count=1)
        
        # rename colums to the actual names
        ta_names = trat_q_db.loc[trat_q_db["agg_manual"]=="stai_trait",:]
        ta = ta.rename(columns=dict(zip(ta_names.varname_old, ta_names.varname)))
        ta_unrecoded = ta_unrecoded.rename(columns=dict(zip(ta_names.varname_old, ta_names.varname)))
        
        ta.to_csv(data_path+'individual_questionnaires/stai_trait.csv')
        ta_unrecoded.to_csv(data_path+'individual_questionnaires/stai_trait_unrec.csv')
        df["stai_ta"] = ta["ta"]
        df["stai_ta_s" + str(conf["session"])] = ta["ta"]

    if conf["session_type"] != "extra":
        # State anxiety STAI
        sa = data.loc[:, "Q4I1[SQ001]":"Q4I20[SQ001]"]
        sa.to_csv(data_path+'working_data/stai_state.csv')
        sa = sa.dropna()
        for c in sa.columns:
            sa[c] = sa[c].replace({"1 - Not at all": 1, "2 - Somewhat": 2,  "3 - Moderately so": 3, "4 - Very much so": 4})
        stai_scoring = [-1, -1, 1, 1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1, -1, -1, 1, 1, -1, -1] #1=normal -1=reverse
        stai_key = { 'item': np.arange(1,21), 'scoring': stai_scoring}
        stai_key = pd.DataFrame.from_dict(stai_key)

        sa.to_csv(data_path+'working_data/stai_state.csv')
        for i, sub in enumerate(sa.index):
            idx2 = np.array(stai_key.scoring.isin([-1])).astype(bool)
            d2 = np.array(sa.loc[sub,])
            sa.loc[sub,idx2] = 5 - d2[idx2]
        sa["sa"] = sa.sum(axis=1,  min_count=1)
        # np.sum(sa, axis=1)
        
        # rename colums to the actual names
        sa_names = trat_q_db.loc[trat_q_db["agg_manual"]=="stai_state",:]
        sa = sa.rename(columns=dict(zip(sa_names.varname_old, sa_names.varname)))
        
        sa.to_csv(data_path+'individual_questionnaires/stai_state.csv')
        df["stai_sa"] = sa["sa"]


    # Sticsa cognitive and somatic sub-scales
    st_subsc = np.array(['s','s','c','c','c','s','s','s','c','c','c','s','c','s','s','c','c','s','c','s','s'])


    # STICSA TRAIT
    if conf["session_type"] == "long":
        stta = data.loc[:, "Q10I1[SQ001]":"Q10I21[SQ001]"]
        stta = stta.dropna()
        for c in stta.columns:
            stta[c] = stta[c].replace({"1. Not at all":1, "2. A little": 2, "3. Moderately": 3,  "4. Very much so": 4})

        # rename colums to the actual names
        stta_names = trat_q_db.loc[trat_q_db["agg_manual"]=="sticsa_trait",:]
        stta = stta.rename(columns=dict(zip(stta_names.varname_old, stta_names.varname)))
        
        
        stta_cog = stta.iloc[:,np.where(st_subsc=='c')[0]]
        stta_som = stta.iloc[:,np.where(st_subsc=='s')[0]]

        stta["stta"] = stta.sum(axis=1,  min_count=1)
        stta.to_csv(data_path+'individual_questionnaires/sticsa_trait.csv')

        stta_cog["stta_cog"] = stta_cog.sum(axis=1,  min_count=1)
        stta_cog.to_csv(data_path+'individual_questionnaires/sticsa_trait_cog_subscale.csv')

        stta_som["stta_som"] = stta_som.sum(axis=1,  min_count=1)
        stta_som.to_csv(data_path+'individual_questionnaires/sticsa_trait_som_subscale.csv')
        # "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta", "sticsa_sa", "sticsa_cog_sa", "sticsa_som_sa", "bdi", "cat", "stai_sa", "stai_ta"
        
        df["sticsa_ta"] = stta["stta"]
        df["sticsa_cog_ta"] = stta_cog["stta_cog"]
        df["sticsa_som_ta"] = stta_som["stta_som"]
        
        df["sticsa_ta_s" + str(conf["session"])] = stta["stta"]
        df["sticsa_cog_ta_s" + str(conf["session"])] = stta_cog["stta_cog"]
        df["sticsa_som_ta_s" + str(conf["session"])] = stta_som["stta_som"]

    # STICSA STATE
    stsa = data.loc[:, "Q5I1[SQ001]":"Q5I21[SQ001]"]
    stsa = stsa.dropna()
    for c in stsa.columns:
        stsa[c] = stsa[c].replace({"1. Not at all":1, "2. A little": 2, "3. Moderately": 3,  "4. Very much so": 4})

    # rename colums to the actual names
    stsa_names = trat_q_db.loc[trat_q_db["agg_manual"]=="sticsa_state",:]
    stsa = stsa.rename(columns=dict(zip(stsa_names.varname_old, stsa_names.varname)))
        
    stsa_cog = stsa.iloc[:,np.where(st_subsc=='c')[0]]
    stsa_som = stsa.iloc[:,np.where(st_subsc=='s')[0]]

    stsa["stsa"] = stsa.sum(axis=1,  min_count=1)
    stsa.to_csv(data_path+'individual_questionnaires/sticsa_state.csv')

    stsa_cog["stsa_cog"] = stsa_cog.sum(axis=1,  min_count=1)
    stsa_cog.to_csv(data_path+'individual_questionnaires/sticsa_state_cog_subscale.csv')

    stsa_som["stsa_som"] = stsa_som.sum(axis=1,  min_count=1)
    stsa_som.to_csv(data_path+'individual_questionnaires/sticsa_state_som_subscale.csv')

    df["sticsa_sa"] = stsa["stsa"]
    df["sticsa_cog_sa"] = stsa_cog["stsa_cog"]
    df["sticsa_som_sa"] = stsa_som["stsa_som"]
    



    if conf["session_type"] == "long":
        # BDI
        idxs = np.empty([0,0])
        for it in range(1,22):
            idxs = np.append(idxs, "Q11I"+str(it))

        bdi = pd.DataFrame(np.nan, index=data.index, columns=idxs)
        for it in range(1,22):
            if it == 19:
                d = np.array(data.loc[:, "Q11I19"].replace({"0 - I haven’t lost much weight, if any, lately I am purposely trying to lose weight (Yes/No in the comment section) by eating less.":0, "1 - I have lost more than 5 pounds":1, "2 - I have lost more than 10 pounds":2, "3 - I have lost more than 15 pounds":3}))
                bdata = pd.DataFrame(d,  index=data.index, columns=["score"] )
            else:
                bdata = data.loc[:, "Q11I"+str(it)+"[SQ001]":"Q11I"+str(it)+"[SQ004]"]
                for sqi, sq in enumerate(bdata.columns):
                     bdata[sq] = bdata[sq].replace({"Yes": sqi, "No":np.nan})
                tempd = np.array(bdata)
                bdata["score"] = np.nanmean(tempd, axis=1 )
            bdi[idxs[it-1]] = bdata["score"]
        bdi=bdi.dropna()
        
        # rename colums to the actual names
        bdi_names = trat_q_db.loc[trat_q_db["agg_manual"]=="bdi",:]
        bdi = bdi.rename(columns=dict(zip(bdi_names.varname_old, bdi_names.varname)))
        
        bdi.to_csv(data_path+'individual_questionnaires/bdi.csv')
        df["bdi"] = bdi.sum(axis=1,  min_count=1)
        df["bdi_s" + str(conf["session"])] = bdi.sum(axis=1,  min_count=1)

    if conf["session_type"] == "long":
        # catastrophizing
        cat = data.loc[:, "Q12I1[SQ001]":"Q12I24[SQ001]"]
        cat=cat.dropna()
        for c in cat.columns:
            cat[c] = cat[c].replace({"never":0, "rarely": 1, "sometimes": 2,  "often": 3, "always": 4})
        cat["cat"] = cat.sum(axis=1,  min_count=1)
        
        # rename colums to the actual names
        cat_names = trat_q_db.loc[trat_q_db["agg_manual"]=="catastrophizing",:]
        cat = cat.rename(columns=dict(zip(cat_names.varname_old, cat_names.varname)))
        
        cat.to_csv(data_path+'individual_questionnaires/catastrophizing.csv')
        df["cat"] = cat["cat"]
        df["cat_s" + str(conf["session"])] = cat["cat"]
        

    #if conf["session_type"] == "long":
    #    ms_vars = ["stai_ta",  "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta", "bdi", "cat"]
    #    ## Do median split
    #    for msv in ms_vars:
    #        me = df[msv].median()
    #        df[msv+"_ms"]= "low"
    #        df[msv+"_ms"][df[msv]>me] = "high"
    clean_data = df
    return clean_data, lg


def append_cases_data(df, ROOT_DIR):
    start_date = '01-01-2020'
    end_date = '31-12-2020'
       
    ### Germany first
    df_de = df.loc[df["GROUP"].isin(["BE"])]
    df_de['submitdate'] = pd.to_datetime(df_de['submitdate'])
    covcase = pd.read_csv(os.path.join(ROOT_DIR, "data", "owid-covid-data.csv")) # data across entire germany
    covcase = covcase.loc[covcase["iso_code"].isin(["DEU"]),:]
    covcase['date'] = pd.to_datetime(covcase['date'])
    ids = (covcase['date'] > start_date) & (covcase['date'] <= end_date)
    covcase = covcase.loc[ids]
    ccase = pd.DataFrame()
    for c in ["DEU"]:
        #select data for a give country
        dfi = covcase.loc[covcase["iso_code"]==c,]
        # calculate rolling mean 
        dfi = standardize_cases_and_differences(dfi, "new_cases", "cases", window=7)
        dfi = standardize_cases_and_differences(dfi, "new_deaths", "deaths", window=7)
        # merge 
        ccase = pd.concat([ccase, dfi])
    ccase["GROUP"] = ccase["iso_code"].replace({"DEU": "BE"})
    ccase["submitdate"] = ccase["date"]
    covcase = ccase.loc[:,["submitdate",  "cases7_diff", "deaths7_diff", "cases7_std", "deaths7_std", "cases7_norm", "deaths7_norm", ]]
    df_de = df_de.merge(covcase, on=["submitdate"], how="left")
    df_de["postcode"] = df_de["sr_postcode"]
    print("Cases7_std NaNs: "+str(df_de["cases7_std"].isna().sum())+" out of entries: "+ str(df_de.shape[0]))
   
    
    ### United Kingdom next
    df_uk = df.loc[df["GROUP"].isin(["UK"])]
    df_uk['date'] = pd.to_datetime(df_uk['submitdate'])

    #df_cases = pd.read_csv(os.path.join(ROOT_DIR, "data", "covid_cases", "cases_by_postcode.csv")).loc[:,["date", "postcode", "cases7", "deaths7"]]
    df_uk["sr_postcode"] = df_uk["sr_postcode"].str.replace(".", "").str.replace(",", "")

    ## This cleans up manually typed post codes by participants
    pre = []
    post= []
    
    for i in df_uk["sr_postcode"].unique():
        pre.append(i)
        mark=0
        if isinstance(i, float) or i.isnumeric():
            i=""
        if len(i)==6:
            post.append(i[:3])
            mark=1
        elif len(i)==7:
            post.append(i[:4])
            mark=1
        elif len(i)==5:
            #print(i)
            post.append(i[:2])
            mark=1
        elif len(i)==2 or len(i)==3 or len(i)==4:
            post.append(i)
            mark=1   
        if mark == 0:
            post.append("NOT-AVAIL")

    d = {}
    for A, B in zip(pre, post):
        d[A] = B 
    df_uk["postcode"] = df_uk["sr_postcode"].replace(d)
    
    df_uk.to_csv(os.path.join(root_dir, 'output', 'temp.csv'))
    
    ### Get cases data from UK
    df_cases = prepare_cases_by_postcode_uk(ROOT_DIR) 
    
    #df_cases.to_csv(os.path.join(root_dir, 'data', 'covid_cases', 'cases.csv'))
    
    df_uk_fin = pd.merge(df_uk, df_cases, how="left", on=["postcode", "date"])
    print("Cases7_std NaNs (before interpolation): "+str(df_uk_fin["cases7_std"].isna().sum())+" out of entries: "+ str(df_uk_fin.shape[0]))
    df_uk_fin.to_csv(os.path.join(ROOT_DIR, "data", "covid_cases", "cases_UK.csv"))
    
    ## Interpolate missing post codes (take mean of the other regions on the same date)
    idx = df_uk_fin["cases7_std"].isna()
    uk_daily_average = df_cases.groupby("date")["cases7_std", "deaths7_std", "cases7_norm", "deaths7_norm", "cases7_diff", "deaths7_diff"].mean()
    df_uk_fin.set_index("date", drop=False)
    df_uk_fin.loc[idx,:] = df_uk_fin.loc[idx,:].join(uk_daily_average, lsuffix ="_discard", rsuffix="", on="date")
    df_uk_fin.drop(df_uk_fin.filter(regex='_discard$').columns.tolist(),axis=1, inplace=True)
    df_uk_fin.to_csv(os.path.join(ROOT_DIR, "data", "covid_cases", "cases_UK_interpolated.csv"))
    print("Cases7_std NaNs (after interpolation): "+str(df_uk_fin["cases7_std"].isna().sum())+" out of entries: "+ str(df_uk_fin.shape[0]))
    
    df = pd.concat([df_de.reset_index(), df_uk_fin.reset_index() ])
    return df
    

def prepare_cases_by_postcode_uk(ROOT_DIR):    
    ### load data containing the number of covid cases
    cases = pd.read_csv(os.path.join(ROOT_DIR, "data", "covid_cases", "utla_2022-05-04.csv"))
    start_date = '01-01-2020'
    end_date = '31-12-2020'
    cases["date"] = cases["date"].astype('datetime64[ns]')
    cases = cases.loc[(cases['date'] > start_date) & (cases['date'] <= end_date)]
    # postcodes in the UK
    pc = pd.read_csv(os.path.join(ROOT_DIR, "data", "covid_cases", "UK-Postcodes", "postcodes.csv"))

    # some postcodes don't exist 
    df_manual = pd.read_csv(os.path.join(ROOT_DIR, "data", "covid_cases", "manual_renaming.csv"))
    
    cases["areaName"] = cases["areaName"].str.replace(", City of", "", regex=True).str.replace(", County of", "", regex=True)

    ### assign the correct names for which we have post codes (UK only)
    special = []
    df_pc_keys = pd.DataFrame()
    for a in cases["areaName"].unique():

        if pc.loc[(pc["region"].isin([a])) | (pc["town"].isin([a]))]["postcode"].shape[0] < 1:
            names = df_manual.loc[df_manual["old_name"].isin([a])] 
            for i in np.array(names["new_name"]): 
                tdf = pc.loc[(pc["region"].isin([i])) | (pc["town"].isin([i]))]
                tdf["areName"] = i
                if pc.loc[(pc["region"].isin([i])) | (pc["town"].isin([i]))]["postcode"].shape[0] < 1:
                    print("No match found "+i)
        else: 
            tdf = pc.loc[(pc["region"].isin([a])) | (pc["town"].isin([a]))]
            tdf["areaName"] = a
        df_pc_keys = pd.concat([df_pc_keys, tdf])
    df_pc_keys = pd.DataFrame(df_pc_keys[["postcode", "areaName"]])
    
    ### calcualte the number of rollowng mean cases for each area and each date
    df_cases = pd.DataFrame()
    for area in cases["areaName"].unique():
        # select area data and 
        a = cases.loc[cases["areaName"].isin([area])]
        a.sort_values(by='date', inplace=True)
        
        a = standardize_cases_and_differences(a, "newCasesBySpecimenDate", "cases", window=7)
        a = standardize_cases_and_differences(a, "newDeaths28DaysByDeathDate", "deaths", window=7)
        a.to_csv(os.path.join(ROOT_DIR, "data", "covid_cases", "onearea.csv"))
        
        df_temp=df_pc_keys.loc[df_pc_keys["areaName"].isin([area])].set_index("areaName").join(a.set_index("areaName")).reset_index() 
        df_cases = pd.concat([df_cases, df_temp])
    df_cases.to_csv(os.path.join(ROOT_DIR, "data", "covid_cases", "cases_by_postcode.csv"))
    df_cases_min = df_cases[["date", "postcode", "cases7_std", "deaths7_std", "cases7_norm", "deaths7_norm", "cases7_diff", "deaths7_diff", ]]
    df_cases_min.to_csv(os.path.join(ROOT_DIR, "data", "covid_cases", "cases_by_postcode_min.csv"))
    return df_cases_min

def standardize_cases_and_differences(df, var_in, var_out, window=7):
    from scipy.stats import zscore
    df[var_out+str(window)] = df[var_in].rolling(window).mean()
    df[var_out+str(window)+"_smooth"] = df[var_out+str(window)].rolling(window).sum()
    df[var_out+str(window)+"_std"] = zscore(df[var_out+str(window)+"_smooth"], nan_policy='omit')
    v  = var_out+str(window)+"_smooth"
    df[var_out+str(window)+"_norm"] = (df[v] - df[v].min()) / (df[v].max() - df[v].min())
    
    
    t = np.array(df[var_out+str(window)+"_std"])
    df[var_out+str(window)+"_diff"] = np.nan
    df[var_out+str(window)+"_diff"][1:] = t[1:] - t[:-1]
    return df