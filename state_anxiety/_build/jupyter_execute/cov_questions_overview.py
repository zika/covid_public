## Demographics questions
1. What's your age?
2. What is your gender?
3. Which post code did you spend most of the last 2 weeks at (the question is voluntary)?
## COVID-related questionnaire
### COVID-related information 
**4. Please tick what applies to you in relation to the coronavirus (can be left bank if none applies):**
[binary answer 0/1]
- I am or was infected.
- A person in my household is or was infected.
- A person close to me fell seriously ill or died from the irus.

---

**5. If you ticked "I am or was infected" above, please specify (otherwise leave blank):**
[binary answer 0/1]
- I was tested for COVID-19 and the test was positive
- I was not tested, the diagnosis was made by a doctor based on the symptoms
- I was not tested, it was a self-diagnosis based on the symtoms

---

**6. Please indicate to what degree do the following apply to you as a consequence of the coronavirus since the last survey (or since the start of the outbreak in February/March 2020 if this is your first session):**
[answer on a scale 1-7]
- I am suffering economic impact (e.g. losing a job or running out of business).
- A close person (family member, relative, friend) is suffering economic impact (e.g. losing a job or running out of business).
- I currently work from home.
- I currently apply social distancing measures.
- I belong to a risk group*.
- A close person (family member, partner, relative, friend) belongs to a risk group.

---

**7. How many people, including you, live in your household:**
1/2/3/4/5+

---

**8. How often did you get information about COVID-19 from the
media?**
- Multiple times per day
- Once a day
- 3-4 times a week
- Once a week
- Few times a month
- Less than few times a month

---

**9. Please type in names of three media outlets in UK/Germany that you use most frequently.**

[open question]

---

**10. Have the news you consumed since the last survey been positive or negative in relation to COVID-19 ?**

[from -3 to 3]

---

**11. Without looking it up, please make a guess how many new daily COVID cases occurred in your country on average over the past week.**
[typed answer]

---

**12. If you think back to the first survey (mid April 2020), to what degree did you worry that:**
[answer on a scale 1-7]
- you will get infected
- something bad will happen to you

---

**13. If you think back to the first survey (mid April 2020), what did you think was the probability that:**
[answer on a scale 0% to 100%]

- you will get infected
- an average person will get infected

---

**14. Please select items that you think of at least once a day (can be left empty if none applies).**
[Yes/No]

- Number of COVID-19 cases in your country
- Number of deaths
- Strain on the health care system
- The situation was/is handled badly by the authorities
- Impact on family
- Impact job/business prospects
- Personal safety
- "I just wish this was over"
- The vaccine

---

**15. To your best knowledge, is there a vaccine for COVID-19?**

- Yes
- No
- I don't know

---

**16. When do you expect a vaccine to be available and ready for first recipients?**
[date answer]

---

**17. When do you expect a vaccine to be widely available to everyone?**
[date answer]

---

**18. If a widely distributed vaccine considered safe by your government would become available:**
[answer on a scale 1-7]

- you would be willing to get vaccinated (giventhat vulnerable groups would be given priority) 
- you would feel relieved
- you would stop worrying about COVID-19
- you would not get vaccinated because you don't belong to a  vulnerable group
- you would not get vaccinated because you don't trust its safety
- you would not get vaccinated for other reason

---

### COVID-specific worries and attitudes
#### Part 1: Worries
**19. Please indicate to what degree do you**
[answer on a scale 1-7]

- worry that you will get infected.
- worry that you will suffer serious medical issues or die.
- worry about the economic impact on me (running out of business, losing a job).
- worry that something bad will happen to you.
- worry that if something happens to you there won't be adequate medical help.
- worry that a close person will get infected.
- worry that a close person will suffer serious medical issues or die.
- worry about short supplies of necessary products, such as food, medication or hygiene products.

---

**20. All COVID-related information considered, we are currently in a relatively dangerous period of the pandemic**
[answer on a scale 1-7]

---

**21. All COVID-related information considered, we are currently in a relatively safe period of the pandemic.**
[answer on a scale 1-7]

---

**22. I was surprised when the coronavirus became a pandemic in my country.**
[answer on a scale 1-7]

---

**23. When the pandemic broke out, I was very scared.**
[answer on a scale 1-7]

---

**24. Many people are overreacting.**
[answer on a scale 1-7]

---

**25. The virus is not as dangerous as it is often portrayed.**
[answer on a scale 1-7]

---

**26. The virus was made in a lab.**
[answer on a scale 1-7]

---

**27. I have been feeling worried or anxious about being infected**
- Never
- On one or several days
- On about half the days
- Nearly every day (more than half the days)

---

**28. I have behaved differently than normal out of worry or anxiety related to the virus** 
- Never
- On one or several days
- On about half the days
- Nearly every day (more than half the days)

---

#### Part 2: Avoidance

**29. Due to COVID-19, I:**
[answer on a scale 1-7]
- Meticulously wash my hands at every opportunity.
- Avoid people as much as I can.
- Avoid going to public places.

---

**30. Due to COVID-19, I:**
[answer on a scale 1-7]
- Touching surfaces outside my house
- Standing closer than 1m to other people
- Eating food prepared in a restaurant/by someone else
- Using public transport
- Visiting the doctor or dentist
- Another specific behavior I did frequently before the virus broke out

---

**31. I would be very angry if a person coughed near me.**
[answer on a scale 1-7]

---

**32. I would be very scared if a person coughed near me.**
[answer on a scale 1-7]

---

**33. The entire COVID thing is a hoax.**
[answer on a scale 1-7]

---

### Probability estimates

**34. Please try to objectively estimate the probability of the following events:**
[answer on a scale 0% to 100%]

- You will get infected with the virus.
- You will die because of the virus.
- You will directly suffer due to the economic impact (for example run out of business, lose a job or investment).
- Somebody you know will get infected by the virus.
- Somebody you know will die because of the virus.
- An average person will get infected

---

**35. Please indicate when do you think the following will happen (or have happened):**
[date]
- The end of the pandemic
- Everyday life comes back to normal.

---

**36. Do you perceive that a second wave might be starting in your country right now?**
- Not at all
- Not quite
- I don't know
- Somewhat
- Very much so

---

**37. Do you expect the pandemic will come back in a third wave?
- Yes
- No

---

**38. If you said "YES" to previous question, please specify when do you expect the third wave to come occur.**
[date]

---

**39. The economy will come back to normal.**
[date]

---

**40. The state of health care will come back to normal.**
[date]




## Personality measures
### STAI-TRAIT
### STAI-STATE
### STICSA-TRAIT
### STICSA-STATE
### BDI
### Catastrophizing