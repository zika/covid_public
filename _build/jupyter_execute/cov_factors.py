#!/usr/bin/env python
# coding: utf-8

# In[1]:


from groo.groo import get_root
root_dir = get_root(".root_dir_covid")
import os
import sys
print(os.path.join(root_dir, "covid-fear", "scripts"))
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))

import pandas as pd


# ### Factors overview
# We ran two factor analyses, one on the covid questionnaires (covid worry, avoidance, probability estimates etc) and one on the trait measures (STAI-TRAIT, STICSA-TRAIT, BDI, Catastrophizing). 
# 
# 
# #### COVID Factors

# In[2]:


df = pd.read_csv(os.path.join(root_dir, 'output', 'questionnaires', 'covid_factors_overview.csv')).filter(regex='^ML', axis=1)



df=df.rename(columns={"ML1":"F3_Economic_Impact_Worry", "ML2":"F5_Worry", "ML3":"F1_Close_Person_Worry", "ML4":"F4_Prob_Estimates", 
                                       "ML5":"F2_Anxiety_Avoidance", "ML6":"F6_Skepticism"})
for i in df.columns:
    df[i]  = df[i].astype(str).str.replace('nan', '')
    
df
# get ind diff via regression, use the pre-cumputed r / full model


# #### Trait factors

# In[3]:


no_trait_factors = 5
raw_trait_factor_names = ["ML"+str(i) for i in range(1,no_trait_factors+1)]
if no_trait_factors == 6:
    trait_factor_names = ["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing",  "TF4_Physiological_Anx",  "TF5_Depression",  "TF6_Cognitive_Anx"]
elif no_trait_factors==5:
    trait_factor_names = ["TF1_Worry", "TF2_Self_Worth", "TF3_Catastrophizing",  "TF4_Anxiety",  "TF5_Depression"]
    
trait_factor_dict = dict(zip(raw_trait_factor_names, trait_factor_names))

df = pd.read_csv(os.path.join(root_dir, 'output', 'questionnaires', 'trait_factors_overview_f'+str(no_trait_factors)+'.csv')).filter(regex='^ML', axis=1)

df=df.rename(columns=trait_factor_dict)
for i in df.columns:
    df[i]  = df[i].astype(str).str.replace('nan', '')
    
df

