#!/usr/bin/env python
# coding: utf-8

# ## Collapsing measures
# - data from different sections of the COVID questionnaire were collapsed into the variables below
# - this is just an initial approach in time there should be a proper factor analysis performed
# - I did all aggregation by summing the values 
# 
# ### Objective severity
# - how impacted was the participant by COVID
# 1. I am/was infected 0/1 -> 1/7
# 2. A person in my household is or was infected. 0/1 -> 1/7 
# 3. A person close to me fell seriously ill or died from the virus. 0/1 -> 1/7
# 4. I am suffering economic impact (e.g. losing a job or running out of business). 1-7
# 5. A close person (family member, relative, friend) is suffering economic impact 1-7
# **aggregated to** `obj_severity`
# 
# ### COVID on mind
# - we asked them whether they think about a list (9 items) of covid-related things at least once a day
# **aggregated to** `covid_thoughts`
# 
# ### COVID-related worries [1-7]
# 1. worry that you will get infected.
# 2. worry that you will suffer serious medical issues or die.
# 3. worry about the economic impact on me (running out of business, losing a job).
# 4. worry that something bad will happen to you.
# 5. worry that if something happens to you there won't be adequate medical help.
# 6. worry that a close person will get infected.
# 7. worry that a close person will suffer serious medical issues or die.
# 8. worry about short supplies of necessary products, such as food, medication or hygiene products.
# **aggregated to** `covid_worry`
# 
# ### Avoidance behaviours [1-7]
# 1. Meticulously wash my hands at every opportunity.
# 2. Avoid people as much as I can.
# 3. Avoid going to public places
# **aggregated to** `avoid_beh`
# 
# ### Avoidance anxiety [1-7]
# 1. Touching surfaces outside my house
# 2. Standing closer than 1m to other people
# 3. Eating food prepared in a restaurant/by someone else
# 4. Using public transport
# 5. Visiting the doctor or dentist
# 6. Another specific behavior I did frequently before the virus broke out
# **aggregated to** `avoid_anx`
# 
# **two previous further aggregated to** `avoid_all`
# 
# ### Probability estimate of negative events [0%-100%]
# - this aggreagtion doesn't make too much sense
# *Estimate the probability that...*
# 1. You will get infected with the virus.
# 2. You will die because of the virus.
# 3. You will directly suffer due to the economic impact (for example run out of business, lose a job or investment).
# 4. Somebody you know will get infected by the virus.
# 5. Somebody you know will die because of the virus.
# 6. An average person will get infected.
# **aggregated to** `prob_est`
# 
# ### Skepticism about COVID
# 1. Many people are overreacting
# 2. The virus is not as dangerous as it is often portrayed.
# 3. The virus was made in a lab.
# 4. The entire COVID thing is a hoax.
# **aggregated to** `skepticism`
