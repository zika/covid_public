#!/usr/bin/env python
# coding: utf-8

# In[1]:


### few links of LME models in Python
# https://www.kaggle.com/ojwatson/mixed-models - can also work with seaborn, so, nice for quick fits
# tensorflow https://www.tensorflow.org/probability/examples/Linear_Mixed_Effects_Models
# https://github.com/kshedden/Statsmodels-MixedLM
# https://github.com/bambinos/bambi
# https://arviz-devs.github.io/arviz/ - is meant to work with bambi which is based entirely on pymc3 and theano

### temporal analyses and tricks
#https://www.dataquest.io/blog/tutorial-time-series-analysis-with-pandas/
#https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.resample.html
# peak alignment https://ourcodingclub.github.io/tutorials/pandas-time-series/
# https://tslearn.readthedocs.io/en/stable/quickstart.html python toolbox to work with time series


# ## Inferential stats: Part I
# 

# In[2]:


# import modules needed
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
import numpy as np
import pandas as pd
import sklearn as sk
import os
import statsmodels.api as sm
import statsmodels.formula.api as smf
import seaborn as sns
from tslearn.metrics import dtw
import warnings
warnings.filterwarnings('ignore')


# In[3]:


if os.getcwd()=="/data/drive/postdoc/Project4_covid/covid-fear/scripts":
    prefix=""
elif os.getcwd()=="/data/drive/postdoc/Project4_covid/covid-fear/final_overview/content":
    prefix = "../"
# load main dataset
df = pd.read_csv("../../data/full_dataset_only_complete.csv")
df.info()


# ### Relationships within-session 
# **What we are interested to predict?**
# COVID-related worries, thoughts, avoidance and probability estimates 
# - regresssion is somewhat messy, it might be better to explore the data a bit first 
# #### Session 1 (tbd)

# In[4]:


#sdf = df.loc[df["SESSIONID"]=="1.0",["sr_age","sr_gender", "q6_work_home", "q6_apply_soc_dist", "q6_media_valence",
#                                     "stai_ta", "stai_sa", "sticsa_ta", 
#                                     "sticsa_sa", "bdi", "cat", "obj_severity", "covid_worry", "avoid_beh", 
#                                     "avoid_anx", "avoid_all", "prob_est", "GROUP"]].dropna()
#
#md = smf.mixedlm("covid_worry ~ stai_ta*obj_severity", sdf, groups=sdf["GROUP"])
#mdf = md.fit()
#print(mdf.summary())
cluster = 1
get_sick = 1


# ## Exploratory analyses
# ### Clustering timecourses
# In this anlysis, we group data into 5 clusters using k-Means. For now, the N of clusters is fixed, so 5 are *imposed*, however I will implement a model selection procedure which chooses the most likely amount of clusters. 
# The plots are colored according to a secondary variable, in the first case, the color corresponds to trait anxiety. Such analysis should help us understand whether there is any meaningful clustering of participants and whether this maps onto their personality measures. 
# 

# In[5]:


from tslearn.utils import to_time_series
from tslearn.clustering import TimeSeriesKMeans, silhouette_score

def estimate_clusters(df, dep_var, metric="dtw"):
    tdf = df[["session", dep_var, "PROLIFICID"]]
    tdf = tdf.pivot(index='PROLIFICID', columns='session', values=dep_var).add_prefix('sess').dropna()
    
    # turn to timeseries data type
    ts = to_time_series(tdf)
   
    # specify model
    model = TimeSeriesKMeans(n_clusters=5, metric=metric, max_iter=20, verbose=0, n_init=5) #specify model
    # fit model 
    model.fit(ts) #fit

    ## not sur ewhat's the silhouette score
    #labels = model.fit_predict(ts) # ??
    #silhouette_score(ts, labels, metric=metric) #?? 

    #which cluster each participant belongs to - awesome!
    cl_ids = model.predict(ts) 

    ## back to pandas
    tdf["cl"] = cl_ids
    tdf["id"] = tdf.index
    
    return tdf, model

def visualize_clusters(df, tdf, model, dep_var, huevar):
    # df - overall data set 
    # tdf - data in wide format, output of estimate_clusters() 
    # model - estimated model
    # dep_var - which variable are we clutering
    # huevar - second variable to color clusters by
    
    # get information from the model
    ts = to_time_series(tdf.drop(columns=["cl", "id"]))
    cl_ids = model.predict(ts) 
    clusters = np.transpose(np.squeeze(model.cluster_centers_))
    (unique, counts) = np.unique(cl_ids, return_counts=True)
    
    # where will labels be shown 
    y = clusters[18,:]
    me=tdf[["sess18", "cl"]].groupby("cl").mean()
    yme=np.array(me["sess18"])
    
    # turn back to long format
    tdf = tdf.melt(id_vars=["id", "cl"], value_vars=["sess"+str(x) for x in range(20)]).set_index("id")
    
    # add anxiety to the dataset
    stdf = tdf.join(df.loc[df["SESSIONID"]=="15.0",[huevar, "PROLIFICID"]].set_index("PROLIFICID").dropna())

    # Set your custom color palette
    pal=sns.color_palette("rocket_r", 10)
    
    #get mean anxiety per bin
    anx_cluster = stdf[["cl", huevar]].groupby(["cl"]).mean()

    # create 10 bins based on min/max anxiety
    anx_bins = np.linspace(anx_cluster[huevar].min(), anx_cluster[huevar].max(), 10)

    # get index of which anxity bin does each cluster belong to 
    anx_idx = np.digitize(anx_cluster[huevar],anx_bins)

    newpal = [pal[ival-1] for idx, ival in enumerate(anx_idx)]
    
    plt.figure(figsize=(10,7))
    ax = sns.lineplot(data=tdf, x="session", y="value", hue="cl", palette=newpal, legend=False)
    ax.set_xticklabels([str(i) for i in range(20)])
    plt.ylabel(dep_var)
    x = 18
    for i,v,c,a,y in zip(np.array(anx_cluster.index), unique, counts, anx_cluster[huevar], yme):
        ax.annotate("n="+str(c)+" "+huevar+"="+str(np.round(a)), (x,y),
                   bbox=dict(boxstyle="round", fc="w", edgecolor=newpal[i]))
    
    plt.show()



# In[6]:


var = ["obj_severity", "covid_worry", "avoid_beh", "avoid_anx", "avoid_all", "prob_est"]
# qs not included because they start later in the project: "covid_thoughts", "skepticism"
if cluster == 1:
    for dep_var in var:
        tdf, model = estimate_clusters(df, dep_var)
        visualize_clusters(df, tdf, model, dep_var, "stai_ta")               


# ### Influence of infection 
# In this section I will look at how getting infected influenced the different measures
# 
# There are quite a few people who indicated that they had covid on 1 or 2 sessions but then on subsequent sessions didn't indicate it any more. 

# In[59]:


tdf = df[["session", "q6_me_inf", "PROLIFICID"]]
ntdf = tdf.pivot(index='PROLIFICID', columns='session', values="q6_me_inf").add_prefix('sess')
ntdf["when_got_sick"] = ntdf.sum(axis=1)
    
# turn to timeseries data type
#ts = to_time_series(tdf)
ntdf= ntdf.loc[ntdf["when_got_sick"]>0,:]
infected = np.array(ntdf.index)

#df = df.set_index("PROLIFICID")
#idf = df.loc[df.index.intersection(infected),:]
nptdf = np.array(ntdf.drop(columns=["when_got_sick"]))
y,x = np.where(nptdf == 1) 
gotsick = np.empty([])
gotsick = np.empty((nptdf.shape[0]))
gotsick[:] = np.nan
for iy in np.unique(y):
    gotsick[iy] = np.min(x[y==iy])
    #print(sep)
ntdf["sess_got_sick"] = gotsick

inf_df_wide = ntdf

## bring  info about when they got infected back 
inf_df = df.set_index("PROLIFICID")
inf_df = inf_df.loc[inf_df.index.intersection(infected),:]


# In[85]:


var = ["obj_severity", "covid_worry", "avoid_beh", "avoid_anx", "avoid_all", "prob_est"]
for v in var:
    tdf = df[["session", v, "PROLIFICID"]]
    tdf = tdf.pivot(index='PROLIFICID', columns='session', values=v).add_prefix('sess')
    tdf = tdf.loc[tdf.index.intersection(infected),:]
    ntdf = np.array(tdf)
    locked_d = np.empty((45,20))
    locked_d[:] =np.nan
    for row in range(ntdf.shape[0]):
        locked_d[row,0:20-int(gotsick[row])] = ntdf[row, int(gotsick[row]):20]
    locked_df = pd.DataFrame(locked_d)
    locked_df = locked_df.melt()
    plt.figure()
    sns.lineplot(data=locked_df, x="variable", y="value", ci=68, err_style='band')
    plt.xlabel("sessions since getting infected")
    plt.ylabel(v)

