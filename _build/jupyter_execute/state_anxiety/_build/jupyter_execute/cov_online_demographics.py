#!/usr/bin/env python
# coding: utf-8

# ## Online Demographics Data 

# In[1]:


import pandas as pd
import ptitprince as pt
import os
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import scipy as sp
from scipy import stats
from scipy.stats import pearsonr


# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib as mpl
mpl.rcParams['patch.force_edgecolor'] = True


# In[3]:


os.chdir("../../")


# In[4]:


# Checks current directory
cwd = os.getcwd() 
print("Current working directory is:", cwd) 


# ##### Loading the data  

# In[5]:


#Loading the data for session 1

session1 = pd.read_csv('data/session1/clean_dataset.csv')
session1 = session1.set_index('PROLIFICID')

#Loading the data for session 8

session8 = pd.read_csv('data/session8/clean_dataset.csv')
session8 = session8.set_index('PROLIFICID')

#Loading the data for session 15

session15 = pd.read_csv('data/session15/clean_dataset.csv')
session15 = session15.set_index('PROLIFICID')


# ### Session1  

# In[6]:


session1.info()


# ##### Overall Distribution

# In[7]:


ivs = ["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]; 
#ivs = ['ta_overall']
labels = ['STAI trait anxiety', 'STAI state anxiety', 'STICSA trait anxiety', 'STICSA state anxiety', 'BDI', 'Catastophizing']

for (v,l) in zip(ivs, labels):
    print(l)
    fig, axs = plt.subplots(ncols=1, figsize=(10,5))
    sns.histplot(data=session1[session1['GROUP'] == 'UK'], x=v, bins=12, stat='density', kde=True, element="step",
                color=sns.color_palette("crest")[2])
    sns.histplot(data=session1[session1['GROUP'] == 'BE'], x=v, bins=12, stat='density', kde=True, element="step",
                color=sns.color_palette("magma")[2])
    axs.set_xlabel(l)
    axs.legend(['BE', 'UK'])


# In[8]:


sdf = session1.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", 'GROUP']]
g = sns.pairplot(sdf, corner=True, diag_kind="kde", kind="reg", hue = 'GROUP')
plt.subplots_adjust(top=0.9)


# In[9]:


df = session1.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]]


# In[10]:


rho = df.corr()
pval = df.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(*rho.shape)
p = pval.applymap(lambda x: ''.join(['*' for t in [0.01,0.05,0.1] if x<=t]))
rho.round(3).astype(str) + p


# In[11]:


mpl.rcParams['figure.figsize'] = 14,5
mpl.rcParams['font.size'] = 14
sns.heatmap(rho, cmap = 'viridis', linecolor = 'black', linewidth = 1, annot = True)


# ##### Summary Table

# In[12]:


dec = 3 #number of decimal points

header = pd.MultiIndex.from_product([['Male','Female'],
                                     ['M', '%', 'SD']])
summary = pd.DataFrame([['',str(round((session1['sr_gender'][session1['sr_gender'] == 'M'].count() / session1['sr_gender'].count()) * 100,2)) + ' %','','',str(round((session1['sr_gender'][session1['sr_gender'] == 'F'].count() / session1['sr_gender'].count()) * 100,2)) + ' %',''],
                   [round(session1['sr_age'][session1['sr_gender'] == 'M'].mean(),2),'',round(session1['sr_age'][session1['sr_gender'] == 'M'].std(),2),round(session1['sr_age'][session1['sr_gender'] == 'F'].mean(),2),'',round(session1['sr_age'][session1['sr_gender'] == 'F'].std(),2)],
                   [round(session1['stai_ta'][session1['sr_gender'] == 'M'].mean(),2),'',round(session1['stai_ta'][session1['sr_gender'] == 'M'].std(),2),round(session1['stai_ta'][session1['sr_gender'] == 'F'].mean(),2),'',round(session1['stai_ta'][session1['sr_gender'] == 'F'].std(),2)],
                   [round(session1['stai_sa'][session1['sr_gender'] == 'M'].mean(),2),'',round(session1['stai_sa'][session1['sr_gender'] == 'M'].std(),2),round(session1['stai_sa'][session1['sr_gender'] == 'F'].mean(),2),'',round(session1['stai_sa'][session1['sr_gender'] == 'F'].std(),2)],
                   [round(session1['sticsa_ta'][session1['sr_gender'] == 'M'].mean(),2),'',round(session1['sticsa_ta'][session1['sr_gender'] == 'M'].std(),2),round(session1['sticsa_ta'][session1['sr_gender'] == 'F'].mean(),2),'',round(session1['sticsa_ta'][session1['sr_gender'] == 'F'].std(),2)],
                   [round(session1['sticsa_sa'][session1['sr_gender'] == 'M'].mean(),2),'',round(session1['sticsa_sa'][session1['sr_gender'] == 'M'].std(),2),round(session1['sticsa_sa'][session1['sr_gender'] == 'F'].mean(),2),'',round(session1['sticsa_sa'][session1['sr_gender'] == 'F'].std(),2)],
                   [round(session1['bdi'][session1['sr_gender'] == 'M'].mean(),2),'',round(session1['bdi'][session1['sr_gender'] == 'M'].std(),2),round(session1['bdi'][session1['sr_gender'] == 'F'].mean(),2),'',round(session1['bdi'][session1['sr_gender'] == 'F'].std(),2)],
                   [round(session1['cat'][session1['sr_gender'] == 'M'].mean(),2),'',round(session1['cat'][session1['sr_gender'] == 'M'].std(),2),round(session1['cat'][session1['sr_gender'] == 'F'].mean(),2),'',round(session1['cat'][session1['sr_gender'] == 'F'].std(),2)]], 
                  index=['Gender', 'Age', 'Trait Anxiety (STAIT)', 'State Anxiety (STAIS)', 'Trait Cognitive and Somatic Anxiety (STICSAT)', 'State Cognitive and Somatic Anxiety (STICSAS)', 'Beck Depression Inventory (BDI)', 'Catastrophizing'], 
                  columns=header)
header1 = pd.MultiIndex.from_product([[''],
                                     ['P']])
df1 = pd.DataFrame([[''],
                    [round(stats.ttest_ind(session1['sr_age'][session1['sr_gender'] == 'F'], session1['sr_age'][session1['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session1['stai_ta'][session1['sr_gender'] == 'F'], session1['stai_ta'][session1['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session1['stai_sa'][session1['sr_gender'] == 'F'], session1['stai_sa'][session1['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session1['sticsa_ta'][session1['sr_gender'] == 'F'], session1['sticsa_ta'][session1['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session1['sticsa_sa'][session1['sr_gender'] == 'F'], session1['sticsa_sa'][session1['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session1['bdi'][session1['sr_gender'] == 'F'], session1['bdi'][session1['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session1['cat'][session1['sr_gender'] == 'F'], session1['cat'][session1['sr_gender'] == 'M'], nan_policy='omit')[1], dec)]],
                   index=['Gender', 'Age', 'Trait Anxiety (STAIT)', 'State Anxiety (STAIS)', 'Trait Cognitive and Somatic Anxiety (STICSAT)', 'State Cognitive and Somatic Anxiety (STICSAS)', 'Beck Depression Inventory (BDI)', 'Catastrophizing'], columns=header1)

session1_online_demographics = summary.join(df1)

session1_online_demographics.to_excel('output/session1_online_demographics.xlsx')


# In[13]:


session1_online_demographics


# ### Session8

# In[14]:


session8.info()


# ##### Overall Distribution

# In[15]:


ivs = ["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]; 
#ivs = ['ta_overall']
labels = ['STAI trait anxiety', 'STAI state anxiety', 'STICSA trait anxiety', 'STICSA state anxiety', 'BDI', 'Catastophizing']

for (v,l) in zip(ivs, labels):
    print(l)
    fig, axs = plt.subplots(ncols=1, figsize=(10,5))
    sns.histplot(data=session8[session8['GROUP'] == 'UK'], x=v, bins=12, stat='density', kde=True, element="step",
                color=sns.color_palette("icefire")[2])
    sns.histplot(data=session8[session8['GROUP'] == 'BE'], x=v, bins=12, stat='density', kde=True, element="step",
                color=sns.color_palette("viridis")[2])
    axs.set_xlabel(l)
    axs.legend(['BE', 'UK'])


# ##### Cross correlations and distributions

# In[16]:


sdf = session8.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", 'GROUP']]
g = sns.pairplot(sdf, corner=True, diag_kind="kde", kind="reg", hue = 'GROUP')
plt.subplots_adjust(top=0.9)


# In[17]:


df = session8.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]]


# In[18]:


rho = df.corr()
pval = df.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(*rho.shape)
p = pval.applymap(lambda x: ''.join(['*' for t in [0.01,0.05,0.1] if x<=t]))
rho.round(3).astype(str) + p


# In[19]:


mpl.rcParams['figure.figsize'] = 14,5
mpl.rcParams['font.size'] = 14
sns.heatmap(rho, cmap = 'magma', linecolor = 'black', linewidth = 1, annot = True)


# ##### Summary Table

# In[20]:


header = pd.MultiIndex.from_product([['Male','Female'],
                                     ['M', '%', 'SD']])
summary = pd.DataFrame([['',str(round((session8['sr_gender'][session8['sr_gender'] == 'M'].count() / session8['sr_gender'].count()) * 100,2)) + ' %','','',str(round((session8['sr_gender'][session8['sr_gender'] == 'F'].count() / session8['sr_gender'].count()) * 100,2)) + ' %',''],
                   [round(session8['sr_age'][session8['sr_gender'] == 'M'].mean(),2),'',round(session8['sr_age'][session8['sr_gender'] == 'M'].std(),2),round(session8['sr_age'][session8['sr_gender'] == 'F'].mean(),2),'',round(session8['sr_age'][session8['sr_gender'] == 'F'].std(),2)],
                   [round(session8['stai_ta'][session8['sr_gender'] == 'M'].mean(),2),'',round(session8['stai_ta'][session8['sr_gender'] == 'M'].std(),2),round(session8['stai_ta'][session8['sr_gender'] == 'F'].mean(),2),'',round(session8['stai_ta'][session8['sr_gender'] == 'F'].std(),2)],
                   [round(session8['stai_sa'][session8['sr_gender'] == 'M'].mean(),2),'',round(session8['stai_sa'][session8['sr_gender'] == 'M'].std(),2),round(session8['stai_sa'][session8['sr_gender'] == 'F'].mean(),2),'',round(session8['stai_sa'][session8['sr_gender'] == 'F'].std(),2)],
                   [round(session8['sticsa_ta'][session8['sr_gender'] == 'M'].mean(),2),'',round(session8['sticsa_ta'][session8['sr_gender'] == 'M'].std(),2),round(session8['sticsa_ta'][session8['sr_gender'] == 'F'].mean(),2),'',round(session8['sticsa_ta'][session8['sr_gender'] == 'F'].std(),2)],
                   [round(session8['sticsa_sa'][session8['sr_gender'] == 'M'].mean(),2),'',round(session8['sticsa_sa'][session8['sr_gender'] == 'M'].std(),2),round(session8['sticsa_sa'][session8['sr_gender'] == 'F'].mean(),2),'',round(session8['sticsa_sa'][session8['sr_gender'] == 'F'].std(),2)],
                   [round(session8['bdi'][session8['sr_gender'] == 'M'].mean(),2),'',round(session8['bdi'][session8['sr_gender'] == 'M'].std(),2),round(session8['bdi'][session8['sr_gender'] == 'F'].mean(),2),'',round(session8['bdi'][session8['sr_gender'] == 'F'].std(),2)],
                   [round(session8['cat'][session8['sr_gender'] == 'M'].mean(),2),'',round(session8['cat'][session8['sr_gender'] == 'M'].std(),2),round(session8['cat'][session8['sr_gender'] == 'F'].mean(),2),'',round(session8['cat'][session8['sr_gender'] == 'F'].std(),2)]], 
                  index=['Gender', 'Age', 'Trait Anxiety (STAIT)', 'State Anxiety (STAIS)', 'Trait Cognitive and Somatic Anxiety (STICSAT)', 'State Cognitive and Somatic Anxiety (STICSAS)', 'Beck Depression Inventory (BDI)', 'Catastrophizing'], 
                  columns=header)
header1 = pd.MultiIndex.from_product([[''],
                                     ['P']])
df1 = pd.DataFrame([[''],
                    [round(stats.ttest_ind(session8['sr_age'][session8['sr_gender'] == 'F'], session8['sr_age'][session8['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session8['stai_ta'][session8['sr_gender'] == 'F'], session8['stai_ta'][session8['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session8['stai_sa'][session8['sr_gender'] == 'F'], session8['stai_sa'][session8['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session8['sticsa_ta'][session8['sr_gender'] == 'F'], session8['sticsa_ta'][session8['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session8['sticsa_sa'][session8['sr_gender'] == 'F'], session8['sticsa_sa'][session8['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session8['bdi'][session8['sr_gender'] == 'F'], session8['bdi'][session8['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session8['cat'][session8['sr_gender'] == 'F'], session8['cat'][session8['sr_gender'] == 'M'], nan_policy='omit')[1], dec)]],
                   index=['Gender', 'Age', 'Trait Anxiety (STAIT)', 'State Anxiety (STAIS)', 'Trait Cognitive and Somatic Anxiety (STICSAT)', 'State Cognitive and Somatic Anxiety (STICSAS)', 'Beck Depression Inventory (BDI)', 'Catastrophizing'], columns=header1)

session8_online_demographics = summary.join(df1)

session8_online_demographics.to_excel('output/session8_online_demographics.xlsx')


# In[21]:


session8_online_demographics


# ### Session15

# In[22]:


session15.info()


# ##### Overall Distribution

# In[23]:


ivs = ["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]; 
#ivs = ['ta_overall']
labels = ['STAI trait anxiety', 'STAI state anxiety', 'STICSA trait anxiety', 'STICSA state anxiety', 'BDI', 'Catastophizing']

for (v,l) in zip(ivs, labels):
    print(l)
    fig, axs = plt.subplots(ncols=1, figsize=(10,5))
    sns.histplot(data=session15[session15['GROUP'] == 'UK'], x=v, bins=12, stat='density', kde=True, element="step",
                color=sns.color_palette("Blues")[2])
    sns.histplot(data=session15[session15['GROUP'] == 'BE'], x=v, bins=12, stat='density', kde=True, element="step",
                color=sns.color_palette("YlOrBr")[2])
    axs.set_xlabel(l)
    axs.legend(['BE', 'UK'])


# ##### Cross correlations and distributions

# In[24]:


sdf = session15.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", 'GROUP']]
g = sns.pairplot(sdf, corner=True, diag_kind="kde", kind="reg", hue = 'GROUP')
plt.subplots_adjust(top=0.9)


# In[25]:


df = session15.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]]


# In[26]:


rho = df.corr()
pval = df.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(*rho.shape)
p = pval.applymap(lambda x: ''.join(['*' for t in [0.01,0.05,0.1] if x<=t]))
rho.round(3).astype(str) + p


# In[27]:


mpl.rcParams['figure.figsize'] = 14,5
mpl.rcParams['font.size'] = 14
sns.heatmap(rho, cmap = 'icefire', linecolor = 'black', linewidth = 1, annot = True)


# ##### Summary Table

# In[28]:


header = pd.MultiIndex.from_product([['Male','Female'],
                                     ['M', '%', 'SD']])
summary = pd.DataFrame([['',str(round((session15['sr_gender'][session15['sr_gender'] == 'M'].count() / session15['sr_gender'].count()) * 100,2)) + ' %','','',str(round((session15['sr_gender'][session15['sr_gender'] == 'F'].count() / session15['sr_gender'].count()) * 100,2)) + ' %',''],
                   [round(session15['sr_age'][session15['sr_gender'] == 'M'].mean(),2),'',round(session15['sr_age'][session15['sr_gender'] == 'M'].std(),2),round(session15['sr_age'][session15['sr_gender'] == 'F'].mean(),2),'',round(session15['sr_age'][session15['sr_gender'] == 'F'].std(),2)],
                   [round(session15['stai_ta'][session15['sr_gender'] == 'M'].mean(),2),'',round(session15['stai_ta'][session15['sr_gender'] == 'M'].std(),2),round(session15['stai_ta'][session15['sr_gender'] == 'F'].mean(),2),'',round(session15['stai_ta'][session15['sr_gender'] == 'F'].std(),2)],
                   [round(session15['stai_sa'][session15['sr_gender'] == 'M'].mean(),2),'',round(session15['stai_sa'][session15['sr_gender'] == 'M'].std(),2),round(session15['stai_sa'][session15['sr_gender'] == 'F'].mean(),2),'',round(session15['stai_sa'][session15['sr_gender'] == 'F'].std(),2)],
                   [round(session15['sticsa_ta'][session15['sr_gender'] == 'M'].mean(),2),'',round(session15['sticsa_ta'][session15['sr_gender'] == 'M'].std(),2),round(session15['sticsa_ta'][session15['sr_gender'] == 'F'].mean(),2),'',round(session15['sticsa_ta'][session15['sr_gender'] == 'F'].std(),2)],
                   [round(session15['sticsa_sa'][session15['sr_gender'] == 'M'].mean(),2),'',round(session15['sticsa_sa'][session15['sr_gender'] == 'M'].std(),2),round(session15['sticsa_sa'][session15['sr_gender'] == 'F'].mean(),2),'',round(session15['sticsa_sa'][session15['sr_gender'] == 'F'].std(),2)],
                   [round(session15['bdi'][session15['sr_gender'] == 'M'].mean(),2),'',round(session15['bdi'][session15['sr_gender'] == 'M'].std(),2),round(session15['bdi'][session15['sr_gender'] == 'F'].mean(),2),'',round(session15['bdi'][session15['sr_gender'] == 'F'].std(),2)],
                   [round(session15['cat'][session15['sr_gender'] == 'M'].mean(),2),'',round(session15['cat'][session15['sr_gender'] == 'M'].std(),2),round(session15['cat'][session15['sr_gender'] == 'F'].mean(),2),'',round(session15['cat'][session15['sr_gender'] == 'F'].std(),2)]], 
                  index=['Gender', 'Age', 'Trait Anxiety (STAIT)', 'State Anxiety (STAIS)', 'Trait Cognitive and Somatic Anxiety (STICSAT)', 'State Cognitive and Somatic Anxiety (STICSAS)', 'Beck Depression Inventory (BDI)', 'Catastrophizing'], 
                  columns=header)
header1 = pd.MultiIndex.from_product([[''],
                                     ['P']])
df1 = pd.DataFrame([[''],
                    [round(stats.ttest_ind(session15['sr_age'][session15['sr_gender'] == 'F'], session15['sr_age'][session15['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session15['stai_ta'][session15['sr_gender'] == 'F'], session15['stai_ta'][session15['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session15['stai_sa'][session15['sr_gender'] == 'F'], session15['stai_sa'][session15['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session15['sticsa_ta'][session15['sr_gender'] == 'F'], session15['sticsa_ta'][session15['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session15['sticsa_sa'][session15['sr_gender'] == 'F'], session15['sticsa_sa'][session15['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session15['bdi'][session15['sr_gender'] == 'F'], session15['bdi'][session15['sr_gender'] == 'M'], nan_policy='omit')[1], dec)],
                    [round(stats.ttest_ind(session15['cat'][session15['sr_gender'] == 'F'], session15['cat'][session15['sr_gender'] == 'M'], nan_policy='omit')[1], dec)]],
                   index=['Gender', 'Age', 'Trait Anxiety (STAIT)', 'State Anxiety (STAIS)', 'Trait Cognitive and Somatic Anxiety (STICSAT)', 'State Cognitive and Somatic Anxiety (STICSAS)', 'Beck Depression Inventory (BDI)', 'Catastrophizing'], columns=header1)

session15_online_demographics = summary.join(df1)

session15_online_demographics.to_excel('output/session15_online_demographics.xlsx')


# In[29]:


session15_online_demographics


# In[ ]:




