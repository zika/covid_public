#!/usr/bin/env python
# coding: utf-8

# ## Severity: Main Modelling
# 
# 

# In[1]:


from groo.groo import get_root
root_dir = get_root(".root_dir_covid")
import os
os.chdir(root_dir)
import sys
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))

import numpy as np
import pandas as pd
from cov_functions import *
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
import bambi as bmb
import arviz as az
import json
import pickle
#import cov_model_management as mm
sys.path.append(os.path.join(root_dir, "covid-fear", "lib")) 
import bammm.bammm as mm
import patsy
from IPython import display


# In[ ]:





# $$
# \alpha
# $$

# In[2]:


# data import
df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete.csv"))
# hyperparameters
corr_type = "spearman" 
factors =[ "F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism"]
trait_factor_names = ["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing","TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"]


# ### How traits predict correlation of covid-state-measure and objective severity 

# In[3]:


data = df.groupby(by="PROLIFICID")[["PROLIFICID", 'state_severity']+trait_factor_names].mean()

for f in factors:
    data["r_"+f] = np.nan
    for s in df["PROLIFICID"].unique():
        data["r_"+f][s] = df.loc[df["PROLIFICID"].isin([s])][["state_severity", f]].corr(corr_type).loc["state_severity"][f]
data


# #### look at correlations

# In[4]:


dfl = data.dropna()
# calculate correlation matrix
corr = dfl.corr(corr_type)

# Generate a mask for the upper triangle
mask = np.triu(np.ones_like(corr, dtype=bool))

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(250, 15, s=75, l=40,
                             n=9, center="light", 
                             as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=1, center=0,
            square=True, annot=True,linewidths=.5, cbar_kws={"shrink": .5})


# ### Model with correlation as DV
# 
# $r_{state, severity}$ ... correlation of state severity and covid-related state measure (e.g. covid worry) within participant across time
# 
# $$
# r_{state, severity} = \beta_0 + \beta_1TF1  + \beta_2TF2 + \beta_3TF3 + \beta_4TF4 + \beta_5TF5 + \beta_6TF6 
# $$
# 
# **Downside** 
# - doesn't distinguish between trait effect and trait\*severity interaction

# In[5]:


model_constant = "alltraits"
for f_idx, f in enumerate(factors): 
    dfl = data.loc[:,["r_"+f]+trait_factor_names].dropna()
    models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
    if "F"+str(f_idx+1)+"_"+model_constant+"_2_3000" in models.keys():
        mod = models["F"+str(f_idx+1)+"_"+model_constant+"_2_3000"]
    else:
        mod = mm.get_template()
    mod["type"] = "lmm"
    mod["lmm"]["dep_var"] = "r_"+f
    mod["lmm"]["fxeff"] = trait_factor_names 
    mod["lmm"]["rneff"] = []
    mod["est"]["nchains"] = 2
    mod["est"]["nsamples"] = 3000
    mod["name"] = "F"+str(f_idx+1)+"_"+model_constant+"_"+str(mod["est"]["nchains"])+"_"+str(mod["est"]["nsamples"])
    mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"]) 
    mod["location"] = os.path.join("output", "models", "model_data", "traits_predicting_severity_correlations", mod["name"]+".dic" )
    mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
    mod, res, m = mm.estimate_lmm(mod=mod, data=dfl, override=0)
    models[mod["name"]] = mod
    mm.save_model_info(models, os.path.join(root_dir, "output", "models", "model_database.json"))
    
    
    # PLOT
    tdf = pd.DataFrame()
    for tf in trait_factor_names:
        d = res.posterior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf

    fig, axs = plt.subplots(nrows=1, ncols=1, figsize=(7, 4))
    # plot the CI97 posterior for the interaction with anxiety
    axs = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs.vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs.axhline(0, alpha=0.2, color='gray')
    axs.set_title("corr of "+f+" and severity")
    axs.set_ylabel("beta (ci95)")
    axs.set_xlabel("")
    axs.set_xticklabels(trait_factor_names, rotation = 30)


# In[6]:


df


# ### Model 0: traits + severity + traits * severity + (session|ID)
# e.g.
# $$
# Worry \sim  Severity + TF_1 + TF_1*Severity  + TF_2 + TF_2*Severity + session + (session|subject) 
# $$

# In[7]:


from IPython import display
#factors_temp = ["F1_Close_Person_Worry"]
model_constant = "severity_onlysession_slope"
estimate1 = 0
if estimate1:
    for f_idx, f in enumerate(factors): 
        dfl = df.loc[:,[f, "state_severity", "PROLIFICID", "session"]+trait_factor_names].dropna()
        models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
        if "F"+str(f_idx+1)+"_"+model_constant+"_2_3000" in models.keys():
            mod = models["F"+str(f_idx+1)+"_"+model_constant+"_2_3000"]
        else:
            mod = mm.get_template()
        mod["type"] = "lmm"
        mod["lmm"]["dep_var"] = f
        mod["lmm"]["fxeff"] = ["state_severity"]+trait_factor_names+['state_severity*'+i for i in trait_factor_names]+["session"]
        mod["lmm"]["rneff"] = ["1|PROLIFICID"]
        mod["est"]["nchains"] = 2
        mod["est"]["nsamples"] = 3000
        mod["est"]["ncores"] = 2
        mod["name"] = "F"+str(f_idx+1)+"_"+model_constant+"_"+str(mod["est"]["nchains"])+"_"+str(mod["est"]["nsamples"])
        mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"]) 
        mod["location"] = os.path.join("output", "models", "model_data", "traits_predicting_severity_correlations", mod["name"]+".dic" )
        mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
        #mod, res, m = mm.estimate_lmm(mod=mod, data=dfl, override=1)
        models[mod["name"]] = mod
        print(mod["lmm"]["eq"])
        #mm.save_model_info(models, os.path.join(root_dir, "output", "models", "model_database.json"))
    



# In[8]:


model_constant = "severity_onlysession_slope"
ylims = [[-0.7,0.7], [-0.15, 0.15], [-0.02, 0.02]]
folder = "traits_predicting_severity_correlations"
for f_idx, f in enumerate(factors): 
    models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
    mod = models["F"+str(f_idx+1)+"_"+model_constant+"_4_3000"]
    # assumes that models have been estimated
    mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
    print(mod["current_sys_location"])
    mod, res,m  = mm.estimate_lmm(mod, [], override=0)
    # add diagnostics
    #az.plot_trace(res)
    display.display(az.summary(res).head(15))

    # PLOT
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))
    for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                        ['state_severity:'+i for i in trait_factor_names]]):
        tdf = pd.DataFrame()
        for tf in beta_group:
            d = res.posterior[tf].stack(draws=("chain", "draw"))
            dt = pd.DataFrame(d, columns=["sample"])
            dt.loc[:,"trait_factor"] = tf
            tdf = pd.concat([tdf, dt])
        tdf
        axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                      data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                      markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
        bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
        axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
        axs[gr_idx].axhline(0, alpha=0.2, color='gray')
        if gr_idx == 0:
            axs[gr_idx].set_title(f)
        elif gr_idx == 1:
            axs[gr_idx].set_title("corr of "+f+" and severity")
        axs[gr_idx].set_ylabel("beta (ci95)")
        axs[gr_idx].set_xlabel("")
        axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
        axs[gr_idx].set_ylim(ylims[gr_idx])


# ### Model 1: traits + severity + traits * severity + (session|ID)
# e.g.
# $$
# Worry \sim  Severity + TF_1 + TF_1*Severity  + TF_2 + TF_2*Severity + (session|subject) 
# $$
# 
# I also fitted this in R and the results are nearly identical

# 

# In[9]:


trait_factor_names+['state_severity']
# include state severity in the model !
#["state_severity"]+trait_factor_names+['state_severity*'+i for i in trait_factor_names]
df.loc[:,trait_factor_names+['state_severity']].describe().T


# In[10]:


from IPython import display
#factors_temp = ["F1_Close_Person_Worry"]
model_constant = "severity_noslope_ND"
estimate1 = 0
if estimate1:
    for f_idx, f in enumerate(factors): 
        dfl = df.loc[:,[f, "state_severity", "PROLIFICID", "session"]+trait_factor_names].dropna()
        models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
        if "F"+str(f_idx+1)+"_"+model_constant+"_2_3000" in models.keys():
            mod = models["F"+str(f_idx+1)+"_"+model_constant+"_2_3000"]
        else:
            mod = mm.get_template()
        mod["type"] = "lmm"
        mod["lmm"]["dep_var"] = f
        mod["lmm"]["fxeff"] = ["state_severity"]+trait_factor_names+['state_severity*'+i for i in trait_factor_names]
        mod["lmm"]["rneff"] = ["session|PROLIFICID"]
        mod["est"]["nchains"] = 2
        mod["est"]["nsamples"] = 3000
        mod["est"]["ncores"] = 2
        mod["name"] = "F"+str(f_idx+1)+"_"+model_constant+"_"+str(mod["est"]["nchains"])+"_"+str(mod["est"]["nsamples"])
        mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"]) 
        mod["location"] = os.path.join("output", "models", "model_data", "traits_predicting_severity_correlations", mod["name"]+".dic" )
        mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
        mod, res, m = mm.estimate_lmm(mod=mod, data=dfl, override=0)
        models[mod["name"]] = mod
        print(mod["lmm"]["eq"])
        mm.save_model_info(models, os.path.join(root_dir, "output", "models", "model_database.json"))
    



# In[11]:


model_constant = "severity_noslope_ND"
folder = "traits_predicting_severity_correlations"
for f_idx, f in enumerate(factors): 
    models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
    mod = models["F"+str(f_idx+1)+"_"+model_constant+"_4_3000"]
    # assumes that models have been estimated
    mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
    mod, res, m = mm.estimate_lmm(mod, [], override=0)
    # add diagnostics
    #az.plot_trace(res)
    display.display(az.summary(res).head(15))

    # PLOT
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(14, 6))
    for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],['state_severity:'+i for i in trait_factor_names]]):
        tdf = pd.DataFrame()
        for tf in beta_group:
            d = res.posterior[tf].stack(draws=("chain", "draw"))
            dt = pd.DataFrame(d, columns=["sample"])
            dt.loc[:,"trait_factor"] = tf
            tdf = pd.concat([tdf, dt])
        tdf
        axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                      data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                      markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
        bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
        axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
        axs[gr_idx].axhline(0, alpha=0.2, color='gray')
        if gr_idx == 0:
            axs[gr_idx].set_title(f)
        elif gr_idx == 1:
            axs[gr_idx].set_title("corr of "+f+" and severity")
        axs[gr_idx].set_ylabel("beta (ci95)")
        axs[gr_idx].set_xlabel("")
        axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")


# In[ ]:





# 

# In[ ]:





# In[10]:


from IPython import display
pd.set_option('display.max_rows', 500)
results_table = az.summary(res)
results_table.head(20)
# model comparison, nesting might not makes sense, sess only 1 data point
#sess into slpe
m


# 

# ### With random slopes
# 
# 
# #### How to best structure the random effects structure
# 
# Simplified example, if we had only two trait factors $TF_1$ and $TF_2$. 
# $St$ = state severity
# 
# Model 1 (above) ("noslopes")
# $$
# Worry \sim St + TF_1 + St*TF_1 + TF_2 + St*TF_2 + (sess| id) 
# $$
# 
# 
# 
# Model 2 - with slopes
# 
# $$
# Worry \sim St + TF_1 + St*TF_1 + TF_2 + St*TF_2 + (1 + TF_1 + TF_2 + sess | id) 
# $$
# 
# Model 3 - also with interaction slopes
# 
# $$
# Worry \sim St + TF_1 + St*TF_1 + TF_2 + St*TF_2 + (1 + TF_1 + St*TF_1 + TF_2 + St*TF_2 + sess | id) 
# $$
# 
# 
# Model 4  
# The random slopes for traits didn't make sense beacuse they are fixed within participant.
# What does make sense thought is to model the session as var of interest
# 
# $$
# Worry \sim St + TF_1 + St*TF_1 + TF_2 + St*TF_2 + TF_1*sess + St*TF_1*sess + TF_2*sess + St*TF_2*sess + (1 + sess | id) 
# $$
# 
# 

# ### Model 2: random slopes  severity + trait + severity\*trait + (1 + trait + session|subject)

# In[11]:


model_constant = "severity_slopes_ND"
folder = "traits_predicting_severity_correlations"
for f_idx, f in enumerate(factors): 
    models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
    mod = models["F"+str(f_idx+1)+"_"+model_constant+"_4_3000"]
    # assumes that models have been estimated
    mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
    mod, res, m = mm.estimate_lmm(mod, [], override=0)
    # add diagnostics
    #az.plot_trace(res)
    display.display(az.summary(res).head(15))

    # PLOT
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(14, 6))
    for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],['state_severity:'+i for i in trait_factor_names]]):
        tdf = pd.DataFrame()
        for tf in beta_group:
            d = res.posterior[tf].stack(draws=("chain", "draw"))
            dt = pd.DataFrame(d, columns=["sample"])
            dt.loc[:,"trait_factor"] = tf
            tdf = pd.concat([tdf, dt])
        tdf
        axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                      data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                      markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
        bounds = tdf.groupby("trait_factor")['sample'].quantile((0.03,0.97)).unstack().reset_index().sort_values(by="trait_factor")
        axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
        axs[gr_idx].axhline(0, alpha=0.2, color='gray')
        if gr_idx == 0:
            axs[gr_idx].set_title(f)
        elif gr_idx == 1:
            axs[gr_idx].set_title("corr of "+f+" and severity")
        axs[gr_idx].set_ylabel("beta (ci95)")
        axs[gr_idx].set_xlabel("")
        axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")


# ### Model 3: random slopes and interactions  severity + trait + severity\*trait + (1 + trait + session + trait\*severity|subject)

# In[12]:


model_constant = "severity_maxmodel_ND"
folder = "traits_predicting_severity_correlations"
for f_idx, f in enumerate(factors): 
    models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
    mod = models["F"+str(f_idx+1)+"_"+model_constant+"_4_3000"]
    # assumes that models have been estimated
    mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
    mod, res, m = mm.estimate_lmm(mod, [], override=0)
    # add diagnostics
    #az.plot_trace(res)
    display.display(az.summary(res).head(15))

    # PLOT
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(14, 6))
    for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],['state_severity:'+i for i in trait_factor_names]]):
        tdf = pd.DataFrame()
        for tf in beta_group:
            d = res.posterior[tf].stack(draws=("chain", "draw"))
            dt = pd.DataFrame(d, columns=["sample"])
            dt.loc[:,"trait_factor"] = tf
            tdf = pd.concat([tdf, dt])
        tdf
        axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                      data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                      markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
        bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
        axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
        axs[gr_idx].axhline(0, alpha=0.2, color='gray')
        if gr_idx == 0:
            axs[gr_idx].set_title(f)
        elif gr_idx == 1:
            axs[gr_idx].set_title("corr of "+f+" and severity")
        axs[gr_idx].set_ylabel("beta (ci95)")
        axs[gr_idx].set_xlabel("")
        axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")


# ### Model4: No random slopes (except session) but session as fixed    
# ### *severity + trait + trait\*sess + (sess|subejct)*
# 

# In[1]:


ylims = [[-1,1], [-0.3, 0.3], [-0.02, 0.02]]
model_constant = "severity_slopes_and_session_ND"
folder = "traits_predicting_severity_correlations"
for f_idx, f in enumerate(factors): 
    models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
    mod = models["F"+str(f_idx+1)+"_"+model_constant+"_4_3000"]
    # assumes that models have been estimated
    mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
    print(mod["current_sys_location"])
    mod, res, m = mm.estimate_lmm(mod, [], override=0)
    # add diagnostics
    #az.plot_trace(res)
    display.display(az.summary(res).head(15))

    # PLOT
    fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
    for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                        ['state_severity:'+i for i in trait_factor_names],
                                        ['state_severity:'+i+":session" for i in trait_factor_names]]):
        tdf = pd.DataFrame()
        for tf in beta_group:
            d = res.posterior[tf].stack(draws=("chain", "draw"))
            dt = pd.DataFrame(d, columns=["sample"])
            dt.loc[:,"trait_factor"] = tf
            tdf = pd.concat([tdf, dt])
        tdf
        axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                      data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                      markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
        bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
        axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
        axs[gr_idx].axhline(0, alpha=0.2, color='gray')
        if gr_idx == 0:
            axs[gr_idx].set_title(f)
        elif gr_idx == 1:
            axs[gr_idx].set_title("corr of "+f+" and severity")
        axs[gr_idx].set_ylabel("beta (ci95)")
        axs[gr_idx].set_xlabel("")
        axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
        axs[gr_idx].set_ylim(ylims[gr_idx])


# ### Model comparison 
# 
# **NOTE (!)** higher WAIC/LOO values are better - see Vehtari et al 2017 or https://arviz-devs.github.io/arviz/api/generated/arviz.compare.html#arviz.compare 
# 

# In[7]:


model_constants = ["severity_noslope",  "severity_maxmodel"]
folder = "traits_predicting_severity_correlations"
models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
for f_idx, f in enumerate(factors):    
    mod1 = models["F"+str(f_idx+1)+"_severity_noslope_ND_4_3000"]
    mod1["current_sys_location"] = os.path.join(root_dir, mod1["location"])
    mod1, res_m1, m = mm.estimate_lmm(mod1, [], override=0)
    
    mod2 = models["F"+str(f_idx+1)+"_severity_slopes_ND_4_3000"]
    mod2["current_sys_location"] = os.path.join(root_dir, mod2["location"])
    mod2, res_m2, m = mm.estimate_lmm(mod2, [], override=0)
    
    mod3 = models["F"+str(f_idx+1)+"_severity_maxmodel_ND_4_3000"]
    mod3["current_sys_location"] = os.path.join(root_dir, mod3["location"])
    mod3, res_m3, m = mm.estimate_lmm(mod3, [], override=0)
    
    mod4 = models["F"+str(f_idx+1)+"_severity_slopes_and_session_ND_4_3000"]
    mod4["current_sys_location"] = os.path.join(root_dir, mod4["location"])
    mod4, res_m4, m = mm.estimate_lmm(mod4, [], override=0)
    
    comp = az.compare({"noslopes": res_m1, "slopes": res_m2, "maximal": res_m3, "session_ints": res_m4}, ic="waic",method="pseudo-BMA")
    display.display(comp)
    az.plot_compare(comp, insample_dev=False);


# ### example of prior distributions

# In[2]:


eq = mod["lmm"]["eq"]
m = bmb.Model(eq, dfl, family="gaussian", link="identity")
m.build()
prior = m.prior_predictive(1000)


# In[ ]:


# plot prior
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                    ['state_severity:'+i for i in trait_factor_names],
                                    ['state_severity:'+i+":session" for i in trait_factor_names]]):
    tdf = pd.DataFrame()
    for tf in beta_group:
        d = prior.prior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf
    axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs[gr_idx].axhline(0, alpha=0.2, color='gray')
    if gr_idx == 0:
        axs[gr_idx].set_title(f)
    elif gr_idx == 1:
        axs[gr_idx].set_title("corr of "+f+" and severity")
    axs[gr_idx].set_ylabel("beta (ci95)")
    axs[gr_idx].set_xlabel("")
    axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
    #axs[gr_idx].set_ylim(ylims[gr_idx])

