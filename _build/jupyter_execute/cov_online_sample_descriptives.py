#!/usr/bin/env python
# coding: utf-8

# ## Online Demographics Data 

# In[1]:


import pandas as pd
import ptitprince as pt
import os
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import scipy as sp
from scipy import stats
from scipy.stats import pearsonr


# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib as mpl
mpl.rcParams['patch.force_edgecolor'] = True


# In[3]:


get_ipython().run_line_magic('run', 'functions_online_demographics.ipynb')


# In[4]:


os.chdir("../../")


# In[5]:


# Checks current directory
cwd = os.getcwd() 
print("Current working directory is:", cwd) 


# ##### Loading the data  

# In[6]:


#Loading the data for session 1

session1 = pd.read_csv('data/session1/clean_dataset.csv')
session1 = session1.set_index('PROLIFICID')

#Loading the data for session 8

session8 = pd.read_csv('data/session8/clean_dataset.csv')
session8 = session8.set_index('PROLIFICID')

#Loading the data for session 15

session15 = pd.read_csv('data/session15/clean_dataset.csv')
session15 = session15.set_index('PROLIFICID')


dec = 3


# ### Session1  

# In[7]:


session = session1


# ##### Overall Distribution

# In[8]:


ivs = ["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]

plt.subplots(figsize=(15,5))

for i in enumerate(ivs): 
    plt.subplot(2,3, i[0]+1)
    sns.histplot(session[i[1]][session['GROUP'] == 'UK'], bins = 12,stat='density', kde=True, element="step", color = sns.color_palette("magma")[3])
    sns.histplot(session[i[1]][session['GROUP'] == 'BE'], bins = 12,stat='density', kde=True, element="step", color = sns.color_palette("viridis")[1])
    plt.legend(['UK', 'BE'])

    plt.tight_layout()

mpl.rcParams['font.size'] = 14


# In[9]:


sdf = session1.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", 'GROUP']]
g = sns.pairplot(sdf, corner=True, diag_kind="kde", kind="reg", hue = 'GROUP')
plt.subplots_adjust(top=0.9)


# In[10]:


df = session1.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]]


# In[11]:


rho = df.corr()
pval = df.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(*rho.shape)
p = pval.applymap(lambda x: ''.join(['*' for t in [0.01,0.05,0.1] if x<=t]))
rho.round(3).astype(str) + p


# In[12]:


mpl.rcParams['figure.figsize'] = 14,5
mpl.rcParams['font.size'] = 14
sns.heatmap(rho, cmap = 'viridis', linecolor = 'black', linewidth = 1, annot = True)


# ##### Summary Table

# In[13]:


summary_table(session, 2, dec, 'session1_online_demographics')


# ### Session8
# 

# In[14]:


session = session8


# ##### Overall Distribution

# In[15]:


ivs = ["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]

plt.subplots(figsize=(15,5))

for i in enumerate(ivs): 
    plt.subplot(2,3, i[0]+1)
    sns.histplot(session[i[1]][session['GROUP'] == 'UK'], bins = 12,stat='density', kde=True, element="step", color=sns.color_palette("icefire")[2])
    sns.histplot(session[i[1]][session['GROUP'] == 'BE'], bins = 12,stat='density', kde=True, element="step", color=sns.color_palette("viridis")[2])
    plt.legend(['UK', 'BE'])

    plt.tight_layout()


# ##### Cross correlations and distributions

# In[16]:


sdf = session8.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", 'GROUP']]
g = sns.pairplot(sdf, corner=True, diag_kind="kde", kind="reg", hue = 'GROUP')
plt.subplots_adjust(top=0.9)


# In[17]:


df = session8.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]]


# In[18]:


rho = df.corr()
pval = df.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(*rho.shape)
p = pval.applymap(lambda x: ''.join(['*' for t in [0.01,0.05,0.1] if x<=t]))
rho.round(3).astype(str) + p


# In[19]:


mpl.rcParams['figure.figsize'] = 14,5
mpl.rcParams['font.size'] = 14
sns.heatmap(rho, cmap = 'magma', linecolor = 'black', linewidth = 1, annot = True)


# ##### Summary Table

# In[20]:


summary_table(session, 2, dec, 'session8_online_demographics')


# ### Session15

# ##### Overall Distribution

# In[21]:


session = session15


# In[22]:


ivs = ["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]

plt.subplots(figsize=(15,5))

for i in enumerate(ivs): 
    plt.subplot(2,3, i[0]+1)
    sns.histplot(session[i[1]][session['GROUP'] == 'UK'], bins = 12,stat='density', kde=True, element="step", color=sns.color_palette("Blues")[2])
    sns.histplot(session[i[1]][session['GROUP'] == 'BE'], bins = 12,stat='density', kde=True, element="step", color=sns.color_palette("YlOrBr")[2])
    plt.legend(['UK', 'BE'])

    plt.tight_layout()


# ##### Cross correlations and distributions

# In[23]:


sdf = session15.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", 'GROUP']]
g = sns.pairplot(sdf, corner=True, diag_kind="kde", kind="reg", hue = 'GROUP')
plt.subplots_adjust(top=0.9)


# In[24]:


df = session.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]]


# In[25]:


rho = df.corr()
pval = df.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(*rho.shape)
p = pval.applymap(lambda x: ''.join(['*' for t in [0.01,0.05,0.1] if x<=t]))
rho.round(3).astype(str) + p


# In[26]:


mpl.rcParams['figure.figsize'] = 14,5
mpl.rcParams['font.size'] = 14
sns.heatmap(rho, cmap = 'icefire', linecolor = 'black', linewidth = 1, annot = True)


# ##### Summary Table

# In[27]:


summary_table(session, 2, dec, 'session15_online_demographics')


# #### Pooled Trait Measures 

# In[28]:


pooled_data = [session1, session8, session15]
pooled = pd.concat(pooled_data, axis = 1, keys ='PROLIFICID' )
pooled['STAI_TA_Pooled'] = round((pooled['R']['stai_ta'] + pooled['O']['stai_ta'] + pooled['P']['stai_ta'])/3, dec)
pooled_final = pooled['STAI_TA_Pooled'].to_frame()
pooled_final.columns = ['stai_ta']
pooled_final['stai_sa'] = round((pooled['R']['stai_sa'] + pooled['O']['stai_sa'] + pooled['P']['stai_sa'])/3, dec)
pooled_final['sticsa_ta'] = round((pooled['R']['sticsa_ta'] + pooled['O']['sticsa_ta'] + pooled['P']['sticsa_ta'])/3, dec)
pooled_final['sticsa_sa'] = round((pooled['R']['sticsa_sa'] + pooled['O']['sticsa_sa'] + pooled['P']['sticsa_sa'])/3, dec)
pooled_final['bdi'] = round((pooled['R']['bdi'] + pooled['O']['bdi'] + pooled['P']['bdi'])/3, dec)
pooled_final['cat'] = round((pooled['R']['cat'] + pooled['O']['cat'] + pooled['P']['cat'])/3, dec)
pooled_final['GROUP'] = pooled['R']['GROUP']
pooled_final['sr_gender'] = pooled['R']['sr_gender']
pooled_final['sr_age'] = pooled['R']['sr_age']

pooled_trait_measures = pooled_final
pooled_trait_measures.to_csv('data/pooled_trait_measures.csv')


# In[29]:


pooled_trait_measures


# ##### Overall Distribution

# In[30]:


session = pooled_trait_measures


# In[31]:


ivs = ["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]

plt.subplots(figsize=(15,5))

for i in enumerate(ivs): 
    plt.subplot(2,3, i[0]+1)
    sns.histplot(session[i[1]][session['GROUP'] == 'UK'], bins = 12,stat='density', kde=True, element="step", color=sns.color_palette("icefire")[2])
    sns.histplot(session[i[1]][session['GROUP'] == 'BE'], bins = 12,stat='density', kde=True, element="step", color=sns.color_palette("Spectral")[2])
    plt.legend(['UK', 'BE'])

    plt.tight_layout()


# ##### Cross correlations and distributions

# In[32]:


sdf = pooled_trait_measures.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", 'GROUP']]
g = sns.pairplot(sdf, corner=True, diag_kind="kde", kind="reg", hue = 'GROUP')
plt.subplots_adjust(top=0.9)


# In[33]:


df = pooled_trait_measures.loc[:,["stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat"]]


# In[34]:


rho = df.corr()
pval = df.corr(method=lambda x, y: pearsonr(x, y)[1]) - np.eye(*rho.shape)
p = pval.applymap(lambda x: ''.join(['*' for t in [0.01,0.05,0.1] if x<=t]))
rho.round(3).astype(str) + p


# In[35]:


mpl.rcParams['figure.figsize'] = 14,5
mpl.rcParams['font.size'] = 14
sns.heatmap(rho, cmap = 'vlag', linecolor = 'black', linewidth = 1, annot = True)


# ##### Summary Table

# In[36]:


summary_table(session, 2, dec, 'pooled_trait_measures_summary_table')


# In[ ]:




