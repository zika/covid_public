#!/usr/bin/env python
# coding: utf-8

# ## Sensitivity to objective severity
# This ntbk analyzes how objective measures relate to variations in covid related worry/anxiety/avoidance, and later how this sensitivity relates to individual differences
# 
# 

# In[1]:


from groo.groo import get_root
root_dir = get_root(".root_dir_covid")

import os

print(root_dir)
import sys
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))


import numpy as np
import pandas as pd
from cov_functions import *
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
import warnings
import itertools
warnings.filterwarnings('ignore')
from scipy.spatial.distance import correlation as Dcorr
import bambi as bmb
import arviz as az
import json
import pickle
import bammm.bammm as mm




# load main dataset
df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete.csv"))
sys.path.insert(1, os.path.join(root_dir, "covid-fear", "scripts", "lib", "hoeffding"))
from XtendedCorrel import hoeffding


# In[2]:


### Meta parameters
corr_type = "spearman" # used in all analyses

#data = df.groupby(by=["PROLIFICID"])["stai_sa", "state_severity"].corr()
factors =[ "F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism"]
print("State seveirty NaNs: "+str(df["state_severity"].isna().sum())+" out of entries: "+ str(df.shape[0]))

no_trait_factors = 5

if no_trait_factors == 6:
        trait_factor_names = ["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing",  "TF4_Physiological_Anx",  "TF5_Depression",  "TF6_Cognitive_Anx"]
elif no_trait_factors==5:
    trait_factor_names = ["TF1_Worry", "TF2_Self_Worth", "TF3_Catastrophizing",  "TF4_Anxiety",  "TF5_Depression"]


# ### Description of objective measures
# 
# #### Cases data from UK (Source: JHU)

# !["../../data/imgs/true_cases_UK.png"](attachment:d741f756-ade8-49f8-aad7-1750ef6f5d10.png)

# #### Cases/Deaths for our sample 
# - for UK we have cases/deaths by post code 
# - for Germany I only have data for the entire counry 
# 
# look at statista.de

# In[3]:


var = ["cases7_std", "deaths7_std", "cases7_norm", "deaths7_norm", "cases7_diff", "deaths7_diff"]
lbl = ["cases7_std", "deaths7_std", "cases7_norm", "deaths7_norm", "cases7_diff", "deaths7_diff"]
# "norm" is standardized (ranged to 0-1), "std" is zscored

time_var = "stai_ta"

for vidx, v in enumerate(var):
   
    #country
    fig, axs = plt.subplots( figsize=(7,5))
    pal = sns.color_palette("rocket")
    sns.pointplot(x="session", y=v,
                  data=df, dodge=.532, join=True, color=pal[3],
                  markers="d", scale=.75, ci=68, ax=axs);
    plt.title(lbl[vidx]);
    sns.lineplot(x="session", y=v, hue="PROLIFICID", alpha=0.2, color="gray",
                  data=df,     markers="d",ax=axs);
    axs.get_legend().remove()


# #### Objective severity measure 
# **reminder what went into it**
# 
# 1. I am/was infected `0`=`not`; `1`=`yes` $-0.5$  `Data range:` $[-0.5, 0.5]$
# 2. A person in my household is or was infected. `0`=`not`; `1`=`yes` $-0.5$  `Data range:` $[-0.5, 0.5]$
# 3. A person close to me fell seriously ill or died from the virus. `0`=`not`; `1`=`yes` $-0.5$  `Data range:` $[-0.5, 0.5]$
# 4. I am suffering economic impact (e.g. losing a job or running out of business). $1-7$ `->` zscored
# 5. A close person (family member, relative, friend) is suffering economic impact $1-7$ `->` zscored
# 6. Z-scored change number of cases/deaths (depends on version) by postcode (UK) or in Germany (Germany)
# 
# These are summed

# In[4]:


var = ["state_severity", "deaths7_norm", "q6_me_inf", "q6_close_person_inf_resc","q6_close_person_died", "q6_econ_impact_me_ranged", "q6_econ_impact_closep_ranged"]
lbl = ["state_severity", "deaths7_norm", "q6_me_inf", "q6_close_person_inf_resc","q6_close_person_died", "q6_econ_impact_me_ranged", "q6_econ_impact_closep_ranged"]
       

time_var = "stai_ta"

for vidx, v in enumerate(var):
   
    #country
    fig, axs = plt.subplots( figsize=(7,5))
    pal = sns.color_palette("rocket")
    sns.pointplot(x="session", y=v,
                  data=df, dodge=.532, join=True, color=pal[3],
                  markers="d", scale=.75, ci=68, ax=axs);
    plt.title(lbl[vidx]);
    sns.lineplot(x="session", y=v, hue="PROLIFICID", alpha=0.2, color="gray",
                  data=df,     markers="d",ax=axs);
    axs.get_legend().remove()
plt.figure()



# In[5]:


# Initialize the FacetGrid object
pal = sns.cubehelix_palette(20, rot=-.25, light=.7)
g = sns.FacetGrid(df, row="session", hue="session", aspect=15, height=.5, palette=pal)

# Draw the densities in a few steps
g.map(sns.kdeplot, "state_severity",
      bw_adjust=.5, clip_on=False,
      fill=True, alpha=1, linewidth=1.5)
g.map(sns.kdeplot, "state_severity", clip_on=False, color="w", lw=2, bw_adjust=.5)

# passing color=None to refline() uses the hue mapping
g.refline(y=0, linewidth=2, linestyle="-", color=None, clip_on=False)


# Define and use a simple function to label the plot in axes coordinates
def label(x, color, label):
    ax = plt.gca()
    ax.text(0, .2, label, fontweight="bold", color=color,
            ha="left", va="center", transform=ax.transAxes)


g.map(label, "state_severity")

# Set the subplots to overlap
g.figure.subplots_adjust(hspace=-0.05)

# Remove axes details that don't play well with overlap
#g.set_ylabel("SESSION")
#print(len(g.collections))
#g.set_alpha(0)
g.set_titles("")
g.set(yticks=[], ylabel="")
g.despine(bottom=True, left=True)


# In[6]:


for i in var:
    plt.figure()
    df[i].hist()


# ### Average acrosss all sessions (parametric correlation)
# 
# **Observations**
# - people high in skepticism don't worry or avoid 
# - skpeticism is unrelated state severity 
# - objective severity is closely related to worry about close person (r=0.66), not as much to other measures (although all are positive), this is likely because 2/5 questions that go to the measure related to whether a close person was severely sick or died

# In[7]:


#trait_factor_names = [ "stai_ta", "bdi","cat", "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta"]


data = df.groupby(by="PROLIFICID")[["PROLIFICID", "state_severity", "q7_period_rel_danger", "q7_period_rel_safety"]+trait_factor_names+factors].mean()
print(data.mean())
print("State seveirty NaNs: "+str(data["state_severity"].isna().sum())+" out of entries: "+ str(data.shape[0]))

# calculate correlation matrix
corr = data.corr(corr_type)

# Generate a mask for the upper triangle
mask = np.triu(np.ones_like(corr, dtype=bool))

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(250, 15, s=75, l=40,
                             n=9, center="light", 
                             as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=1, center=0,
            square=True, annot=True,linewidths=.5, cbar_kws={"shrink": .5})

## plot the interesting bits
plt.figure()
sns.scatterplot(data=data, x="F1_Close_Person_Worry", y="state_severity")


# ### Average acrosss all sessions (non-monotonic correlation)

# In[8]:


#data['distance']=data[['x1','y1','x2','y2']].apply(lambda df: great_circle(df['x1'],df['y1'],df['x2'],df['y2']).miles, axis=1)
data = df.groupby(by="PROLIFICID")[["PROLIFICID", "state_severity"]+trait_factor_names+factors].mean().dropna()

corr_d = pd.DataFrame(np.nan, index=data.columns, columns=data.columns)
for i in data.columns:
    for j in data.columns:
        corr_d[i][j] = Dcorr(data[i], data[j])
        
# Generate a mask for the upper triangle
mask = np.triu(np.ones_like(corr_d, dtype=bool))

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(250, 15, s=75, l=40,
                             n=9, center="light", 
                             as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr_d, mask=mask, cmap=cmap, vmax=1, center=0,
            square=True, annot=True,linewidths=.5, cbar_kws={"shrink": .5})
plt.title("non-monotonic (distance) correlation")



# #### Plot all scatters

# In[9]:


plt.figure()
g = sns.pairplot(data, corner=True, diag_kind="kde", kind="reg")
g.map_lower(corrfunc, tests=[corr_type])


# ### Within-sub correlations with severity (across sessions)
# 
# 
# **Observations**
# - why are there many negative r scores? 
# - in general, anxious people don't seem more sensitive to variations in objective severity
# - high SA people seem to have generally more negative association between severity and worry about economic impact 

# In[10]:


data = df.groupby(by="PROLIFICID")[["PROLIFICID", "q7_period_rel_danger"]+trait_factor_names].mean()
for f in factors:
    data["r_"+f] = np.nan
    for s in df["PROLIFICID"].unique():
        data["r_"+f][s] = df.loc[df["PROLIFICID"].isin([s])][["state_severity",   f]].corr(corr_type).loc["state_severity"][f]
fig = plt.figure(figsize = (9,9))
ax = fig.gca()
data.hist(ax = ax)    
#g = sns.pairplot(data.dropna(), corner=True, diag_kind="kde", kind="reg")
#g.map_lower(corrfunc)
corr=data.corr(corr_type)

# Generate a mask for the upper triangle
mask = np.triu(np.ones_like(corr, dtype=bool))

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(250, 15, s=75, l=40,
                             n=9, center="light", 
                             as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=1, center=0,
            square=True, annot=True,linewidths=.5, cbar_kws={"shrink": .5})


# In[11]:


#np.array(df.columns)


# ### Correlation with severity over time
# - for a detailed breakdown and plots for each of the subplots below go to `cov_sensitivity_to_severity_extensive_raw_data_exploration`
# 

# In[12]:


for tt in trait_factor_names:
    splitvar = tt+"_ts" #"stai_ta_ms" #"TF4_Physiological_Anx_ms" # "stai_ta_ms" 
    data = df.groupby(["session", splitvar])[[ "state_severity"]+factors].corr(corr_type)
    data = data.reset_index().rename(columns={"level_2": "variable"})
    plt.figure()
    fig, ax = plt.subplots(ncols=6,nrows=1,figsize=(20,5))
    for f_idx, f in enumerate(factors):
        g = sns.lineplot(data=data.loc[data["variable"].isin([f])], x="session", y="state_severity", hue=splitvar, hue_order=["low", "mid", "high"], palette=sns.color_palette("mako_r", 3), ax=ax[f_idx])
        g.set_title(f)
        g.set_ylabel("Correlation with state severity")
        g.set_ylim(-0.2,1)


# ### Checking for ceiling effects

# In[ ]:





# In[13]:


for tt in trait_factor_names:
    splitvar = tt+"_ms" #"stai_ta_ms" #"TF4_Physiological_Anx_ms" # "stai_ta_ms" 
    data = df.groupby(["session", splitvar])[[ "state_severity"]+factors].mean()
    plt.figure()
    fig, ax = plt.subplots(ncols=6,nrows=1,figsize=(20,3))
    #plt.subplots_adjust(wspace=0.4)
    for f_idx, f in enumerate(factors):
        g = sns.lineplot(data=data, x="session", y="state_severity", hue=splitvar, hue_order=["low", "high"], palette=sns.color_palette("mako_r", 2), ax=ax[f_idx])
        g.set_title(f)
        g.set_ylabel("")
        sns.move_legend(ax[f_idx], "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
        if f_idx == 0:
            g.set_ylabel("State severity (dashed)")
        for lidx, l in enumerate(ax[f_idx].lines):
            ax[f_idx].lines[lidx].set_linestyle("--")
        g2 = ax[f_idx].twinx()
        g = sns.lineplot(data=data, x="session", y=f, hue=splitvar, hue_order=["low", "high"], palette=sns.color_palette("mako_r", 2), ax=g2)
        sns.move_legend(g2, "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
        #sns.move_legend(g2, "lower center", bbox_to_anchor=(.5, 1), ncol=3, title=None, frameon=False)
        g.set_ylabel("")
    
        
        


# In[14]:


for tt in [trait_factor_names[2]]:
    splitvar = tt+"_ms" #"stai_ta_ms" #"TF4_Physiological_Anx_ms" # "stai_ta_ms" 
    data = df.groupby(["session", splitvar])[[ "state_severity"]+factors+["q7_period_rel_danger"]].mean()
    plt.figure()
    fig, ax = plt.subplots(ncols=2,nrows=1,figsize=(15,7))
    #plt.subplots_adjust(wspace=0.4)
    f_idx = 0
    f = "q7_period_rel_danger"
    g = sns.lineplot(data=data, x="session", y="state_severity", hue=splitvar, hue_order=["low", "high"], palette=sns.color_palette("mako_r", 2), ax=ax[f_idx])
    g.set_title(f)
    g.set_ylabel("")
    sns.move_legend(ax[f_idx], "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
    if f_idx == 0:
        g.set_ylabel("State severity (dashed)")
    for lidx, l in enumerate(ax[f_idx].lines):
        ax[f_idx].lines[lidx].set_linestyle("--")
    ax[f_idx].grid(False)
    g2 = ax[f_idx].twinx()
    g = sns.lineplot(data=data, x="session", y=f, hue=splitvar, hue_order=["low", "high"], palette=sns.color_palette("mako_r", 2), ax=g2)
    sns.move_legend(g2, "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
    g.set_ylabel("")


# In[15]:


### aggreageted covid factors


# In[16]:


df["covid_affective"] = df.loc[:,["F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F5_Worry"]].mean(axis=1)
df["covid_objective"] = df.loc[:,["F4_Prob_Estimates"]].mean(axis=1)


# In[17]:


#factors =[ "F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism"]

for tt in trait_factor_names:
    splitvar = tt+"_ms" #"stai_ta_ms" #"TF4_Physiological_Anx_ms" # "stai_ta_ms" 
    data = df.groupby(["session", splitvar])[[ "state_severity"]+["covid_affective", "covid_objective"]].mean()
    plt.figure()
    fig, ax = plt.subplots(ncols=2,nrows=1,figsize=(8,3))
    #plt.subplots_adjust(wspace=0.4)
    for f_idx, f in enumerate(["covid_affective", "covid_objective"]):
        g = sns.lineplot(data=data, x="session", y="state_severity", hue=splitvar, hue_order=["low", "high"], palette=sns.color_palette("mako_r", 2), ax=ax[f_idx])
        g.set_title(f)
        g.set_ylabel("")
        sns.move_legend(ax[f_idx], "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
        if f_idx == 0:
            g.set_ylabel("State severity (dashed)")
        for lidx, l in enumerate(ax[f_idx].lines):
            ax[f_idx].lines[lidx].set_linestyle("--")
        g2 = ax[f_idx].twinx()
        g = sns.lineplot(data=data, x="session", y=f, hue=splitvar, hue_order=["low", "high"], palette=sns.color_palette("mako_r", 2), ax=g2)
        sns.move_legend(g2, "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
        #sns.move_legend(g2, "lower center", bbox_to_anchor=(.5, 1), ncol=3, title=None, frameon=False)
        g.set_ylabel("")


# In[ ]:






# In[18]:


for tt in [trait_factor_names[2]]:
    for f in [factors[2]]:
        tdf = df.loc[df[tt+"_ms"]=="low",:]
        g = sns.FacetGrid(tdf, col="session", height=3, col_wrap=10)
        g.map(sns.regplot, f, "state_severity", color="black")
        #g.map(sns.regplot, f, "state_severity", x_estimator=np.mean, color=".3")

        g.map(corrfunc, f, "state_severity", tests=[ "spearman"], drop_missing=True)
        plt.savefig(os.path.join(root_dir, 'output', 'figures', 'objective_severity', 'plot_low.pdf'), dpi=300)
        tdf = df.loc[df[tt+"_ms"]=="high",:]
        g = sns.FacetGrid(tdf, col="session", height=3, col_wrap=10)
        #g.map(sns.regplot, f, "state_severity", x_estimator=np.mean, color=".3")
        g.map(sns.regplot, f, "state_severity", color="red")
        g.map(corrfunc, f, "state_severity", tests=[ "spearman"], drop_missing=True)
        plt.savefig(os.path.join(root_dir, 'output', 'figures', 'objective_severity', 'plot_high.pdf'), dpi=300)


# ### What kind of people do/don't follow reality? 

# In[19]:


#factors = ["F2_Anxiety_Avoidance", "F4_Prob_Estimates", "F5_Worry"]

data = df.groupby(by="PROLIFICID")[["PROLIFICID"]+trait_factor_names].mean()

for f in factors:
    data["r_"+f] = np.nan
    for s in df["PROLIFICID"].unique():
        data["r_"+f][s] = df.loc[df["PROLIFICID"].isin([s])][["state_severity", f]].corr(corr_type).loc["state_severity"][f]
    data["r_"+f+"_ts"] = pd.qcut(data["r_"+f], [0, .33, .66, 1],labels=['neg', 'mid', 'pos'])
    data["r_"+f+"_ms"] = pd.qcut(data["r_"+f], [0, .5, 1],labels=['low', 'high'])
    #data["r_"+f+"_ts"] = pd.qcut(data["r_"+f], [0, .33, .66, 1],labels=['low', 'mid', 'high'])
    #data["r_"+f+"_ms"] = pd.qcut(data["r_"+f], [0, .5, 1],labels=['low', 'high'])
#["F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism"]
#["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing","TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"]
data

new_f_vals = ["r_"+f+"_ms" for f in factors]+["r_"+f+"_ts" for f in factors]
data2 = pd.melt(data, id_vars=new_f_vals, value_vars=trait_factor_names,
                var_name='trait', value_name='trait_value')

data


# **Example how data split to terciles look like**

# In[20]:


fig, ax = plt.subplots(nrows=2,ncols=6, figsize=(19,9))
for f_idx, f in enumerate(factors):   
    sns.histplot(
        data, x="r_"+f, hue="r_"+f+"_ts", element="step",
        stat="count", common_norm=False, binwidth=0.01, ax=ax[0,f_idx]
    )
    sns.histplot(
        data, x="r_"+f, hue="r_"+f+"_ms", element="step",
        stat="count", common_norm=False, binwidth=0.01, ax=ax[1, f_idx]
    )
    #for tidx, trf in enumerate(trait_factor_names):
    #    sns.histplot(
    #        data, x=trf, hue="r_"+f+"_ts", element="step",
    #        stat="count", common_norm=False, binwidth=0.2, ax=ax[1+tidx]
    #    )
    #    sns.move_legend(ax[1+tidx], "upper center", bbox_to_anchor=(1.05, 0.2), ncol=1, title=None, frameon=False)
        #ax[1+idx].legend()

#data.filter(regex='Anxiety_Avoidance', axis=1).hist(by="r_F2_Anxiety_Avoidance_ms")


#data[["TF4_Physiological_Anx", "r_F5_Worry_ms"]].groupby(by="r_F5_Worry_ms").mean()


# consider timelag, t-1
# maybe do positive/neutral/negative
# big picture which state m follows 


# #### Average trait for median-split followers/non-followers
# - the splitting is that on median **absolute** correlations between state measures and severity, aboslute because some of them were negative which made interpretation of some plots below a bit difficulty
# - it's not really about following per se, it's about the association between severity and some state measure (e.g. probability or worry)
# - lower association signals that the state measure is not following the objetive, i.e., it can be always high/low in the middle
# 
# **How to read** 
# "People with low association between state severity and worry for close person are high in self consciousness, anxiety, depression"

# In[21]:


from IPython import display

fig, ax = plt.subplots(nrows=len(factors), ncols=1, figsize=(10, len(factors)*4))
fig.subplots_adjust(hspace=0.5)
for f_idx, f in enumerate(factors):
    display.display(data.loc[:,trait_factor_names+["r_"+f+"_ms"]].groupby(by="r_"+f+"_ms").mean())
    #sns.barplot(x="trait", y="trait_value", hue="r_"+f+"_ts", hue_order = ["low", "mid", "high"],
    sns.barplot(x="trait", y="trait_value", hue="r_"+f+"_ms", hue_order = ["low", "high"],
                  dodge=True, data=data2, alpha=.85, ci=68, 
                  zorder=1, palette=sns.color_palette("rocket", 3), ax=ax[f_idx])
    sns.move_legend(ax[f_idx], "lower center", bbox_to_anchor=(1.05, 0.2), ncol=1, title=None, frameon=False)
    ax[f_idx].set_title(f)
    ax[f_idx].set_ylabel("")
    ax[f_idx].set_xlabel("High/low: Correlation with objective severity")
   

    #plt.legend(bbox_to_anchor=(1.02, 0.15), loc='upper left', borderaxespad=0)
plt.savefig(os.path.join(root_dir, "output", "figures", "objective_severity", "corrs_with_obj_severity_2.pdf"))


# ##### Plotting the same but split by followers/non-followers on the x-axis

# In[22]:


fig, ax = plt.subplots(nrows=len(factors), ncols=1, figsize=(10, len(factors)*4))
fig.subplots_adjust(hspace=0.6)
for f_idx, f in enumerate(factors):
    #sns.barplot(hue="trait", y="trait_value", x="r_"+f+"_ts",# hue_order = ["low", "mid", "high"],
    sns.barplot(hue="trait", y="trait_value", x="r_"+f+"_ms",# hue_order = ["low", "mid", "high"],
                  dodge=True, data=data2, alpha=.85, ci=68, 
                  zorder=1, palette=sns.color_palette("mako_r", 6), ax=ax[f_idx])
    sns.move_legend(ax[f_idx], "lower center", bbox_to_anchor=(0.4, 1.1), ncol=3, title=None, frameon=False)
    ax[f_idx].set_xlabel("Correlation with objective severity")
    ax[f_idx].set_ylabel("")
    ax[f_idx].set_title(f)

    #plt.legend(bbox_to_anchor=(1.02, 0.15), loc='upper left', borderaxespad=0)
plt.savefig(os.path.join(root_dir, "output", "figures", "objective_severity", "corrs_with_obj_severity_1.pdf"))


# Next, we look at the difference in mean *trait* measure between followers and non-followers
#  
# **How to interpret** 
# - positive value means that highly sensitive people have of a given trait
# - negative value neabs that insensitive people have more of a given trait

# In[23]:


ndf = pd.DataFrame()
for f_idx, f in enumerate(factors):
    tdf = data.groupby(by=["r_"+f+"_ms"])[["r_"+f+"_ms"]+trait_factor_names].mean()
    # diff represents the difference in trait variable between sensitive and insensitive

    tdf.loc[f+"_diff", :] = tdf.loc["high", :] - tdf.loc["low", :]
    tdf = tdf.T.drop(columns=["low", "high"]) 
    if f_idx == 0:
        ndf = tdf
    else:
        ndf = ndf.join(tdf)
    #ndf.loc[f+"_diff", :] = tdf.loc["high", :] - tdf.loc["low", :] 
#ndf = ndf.reset_index()
ndf["trait_var"] = ndf.index
#= pd.melt(ndf, value_vars=trait_factor_names,
#                var_name='trait', value_name='trait_value')
ndf = pd.melt(ndf, id_vars = ["trait_var"], value_vars = [f+"_diff" for f in factors],
             var_name="covid_factor", value_name="difference")


fig, ax = plt.subplots(nrows=len(factors), ncols=1, figsize=(10, len(factors)*4))
fig.subplots_adjust(hspace=0.9)
for f_idx, f in enumerate(factors):
    sns.barplot(x="trait_var", y="difference",# hue_order = ["low", "high"],
                  dodge=True, data=ndf.loc[ndf["covid_factor"]==f+"_diff",:], alpha=.85, ci=68, 
                  zorder=1, color=sns.color_palette("rocket", 6)[2], ax=ax[f_idx])
    #sns.move_legend(ax[f_idx], "lower center", bbox_to_anchor=(1.15, 0.2), ncol=1, title=None, frameon=False)
    ax[f_idx].set_title(f)
    ax[f_idx].set_ylabel("tracking > non-tracking")
    ax[f_idx].set_xlabel("")
    ax[f_idx].set_xticklabels(trait_factor_names, rotation = 45)

   

    #plt.legend(bbox_to_anchor=(1.02, 0.15), loc='upper left', borderaxespad=0)
plt.savefig(os.path.join(root_dir, "output", "figures", "objective_severity", "corrs_with_obj_severity_diff.pdf"))


# #### Average trait for tercile-split followers/non-followers
# - Same as above but split followers into low/mid/high

# In[24]:


fig, ax = plt.subplots(nrows=len(factors), ncols=1, figsize=(10, len(factors)*4))
fig.subplots_adjust(hspace=0.5)
for f_idx, f in enumerate(factors):
    sns.barplot(x="trait", y="trait_value", hue="r_"+f+"_ts", hue_order = ["neg", "mid", "pos"],
                  dodge=True, data=data2, alpha=.85, ci=68, 
                  zorder=1, palette=sns.color_palette("rocket", 3), ax=ax[f_idx])
    sns.move_legend(ax[f_idx], "lower center", bbox_to_anchor=(1.05, 0.2), ncol=1, title=None, frameon=False)
    ax[f_idx].set_title(f)
    ax[f_idx].set_ylabel("")
    ax[f_idx].set_xlabel("High/low: Correlation with objective severity")
   

    #plt.legend(bbox_to_anchor=(1.02, 0.15), loc='upper left', borderaxespad=0)
plt.savefig(os.path.join(root_dir, "output", "figures", "objective_severity", "corrs_with_obj_severity_ters_2.pdf"))


# In[25]:


fig, ax = plt.subplots(nrows=len(factors), ncols=1, figsize=(10, len(factors)*4))
fig.subplots_adjust(hspace=0.6)
for f_idx, f in enumerate(factors):
    sns.barplot(hue="trait", y="trait_value", x="r_"+f+"_ts",# hue_order = ["low", "mid", "high"],
    #sns.barplot(hue="trait", y="trait_value", x="r_"+f+"_ms",# hue_order = ["low", "mid", "high"],
                  dodge=True, data=data2, alpha=.85, ci=68, 
                  zorder=1, palette=sns.color_palette("mako_r", 6), ax=ax[f_idx])
    sns.move_legend(ax[f_idx], "lower center", bbox_to_anchor=(0.4, 1.1), ncol=3, title=None, frameon=False)
    ax[f_idx].set_xlabel("Correlation with objective severity")
    ax[f_idx].set_ylabel("")
    ax[f_idx].set_title(f)

    #plt.legend(bbox_to_anchor=(1.02, 0.15), loc='upper left', borderaxespad=0)
plt.savefig(os.path.join(root_dir, "output", "figures", "objective_severity", "corrs_with_obj_severity_terciles_1.pdf"))


# ### Differences between categories

# Here we ask *what's are the traits of participants who follow the objective (probability measures) more closely than affective and vice versa* 
# 
# **How to read** 
# 
# People who follow more (or "are more influenced by") objective than given affective measure have personality measure X (x axis)

# In[26]:


## Prepare data

ndf = pd.DataFrame()
for f_idx, f in enumerate(factors):
    tdf = data.groupby(by=["r_"+f+"_ms"])[["r_"+f+"_ms"]+trait_factor_names].mean()
    # diff represents the difference in trait variable between sensitive and insensitive
    # positive value means that highly sensitive people have of a given trait
    # negative value neabs that insensitive people have more of a given trait
    tdf.loc[f+"_diff", :] = tdf.loc["high", :] - tdf.loc["low", :]
    tdf = tdf.T.drop(columns=["low", "high"]) 
    if f_idx == 0:
        ndf = tdf
    else:
        ndf = ndf.join(tdf)
ndf["trait_var"] = ndf.index
ndf
for c1, c1_short in zip(["F1_Close_Person_Worry_diff", "F2_Anxiety_Avoidance_diff", "F3_Economic_Impact_Worry_diff", "F5_Worry_diff"],
              ["ClosePersonWorry", "AnxietyAvoidance", "EconImpactWorry", "Worry"]):
    ndf.loc[:,"prob_"+c1_short+"_diff"] =  ndf.loc[:, "F4_Prob_Estimates_diff"] - ndf.loc[:, c1] 

ndf = pd.melt(ndf, id_vars = ["trait_var"], value_vars = ["prob_"+f+"_diff" for f in ["ClosePersonWorry", "AnxietyAvoidance", "EconImpactWorry", "Worry"]],
             var_name="covid_factor", value_name="difference")



# In[27]:


fig, ax = plt.subplots(nrows=4, ncols=1, figsize=(10, 4.2*4))
fig.subplots_adjust(hspace=1.1)
for f_idx, f in enumerate(ndf["covid_factor"].unique()):
    sns.barplot(x="trait_var", y="difference",# hue_order = ["low", "high"],
                  dodge=True, data=ndf.loc[ndf["covid_factor"]==f,:], alpha=.85, ci=68, 
                  zorder=1, color=sns.color_palette("rocket", 6)[4], ax=ax[f_idx])
    #sns.move_legend(ax[f_idx], "lower center", bbox_to_anchor=(1.15, 0.2), ncol=1, title=None, frameon=False)
    ax[f_idx].set_title(f+"\nREAD: People who follow objective more than affecitve thend to be X")
    ax[f_idx].set_ylabel("Objective > Affective")
    ax[f_idx].set_xlabel("")
    ax[f_idx].set_xticklabels(trait_factor_names, rotation = 30)

   

    #plt.legend(bbox_to_anchor=(1.02, 0.15), loc='upper left', borderaxespad=0)
plt.savefig(os.path.join(root_dir, "output", "figures", "objective_severity", "corrs_with_obj_severity_diff_from_prob.pdf"))


# #### Zoom: Anxiety/Avoidance and Trait Cog and Phys Anxiety  

# In[ ]:





# In[ ]:





# ### Stats analysis
# 
# #### M1: TA\*severity / sessions
# - estimated as random effects

# In[28]:


#db = os.path.join(root_dir, "output", "models", "model_database.json")
#models2remove = ["F1_state_anx_4_4000", "F2_state_anx_4_4000", "F3_state_anx_4_4000", "F4_state_anx_4_4000", "F5_state_anx_4_4000", "F6_state_anx_4_4000"]
#models2remove = ["F1_state_anx_4_4000"]
#mm.remove_model_from_db(db, models2remove)
trait_factor_names


# In[29]:


# median-split data 
for var, short_identifier in zip(trait_factor_names, ["selfconsc", "positive", "catastr", "physanx", "depr", "coganx"]):
    #var ="TF6_Cognitive_Anx"
    splitvar = var+"_ts"#"stai_ta_ts"
    #short_identifier = "coganx"
    data = df.groupby(["session", splitvar])[[ "state_severity"]+factors].corr()
    data = data.reset_index().rename(columns={"level_2": "variable"})

    for f, f_idx in zip(factors, np.arange(1, len(factors)+1)):
        print("F"+str(f_idx)+"_"+f)
        dfl = df.loc[:,[f, var,"session", "state_severity"]].dropna()
        dfl["session"] = pd.Categorical(dfl["session"])

        #mm.models_init(os.path.join(root_dir, "output", "models", "model_database.json"))

        models = json.load(open(os.path.join(root_dir, "output", "models", "model_database.json"), "r")) 
        if "F"+str(f_idx)+"_state_" + short_identifier + "_2_3000" in models.keys():
            mod = models["F"+str(f_idx)+"_state_" + short_identifier + "_2_3000"]
        else:
            mod = mm.get_template()

        mod["type"] = "lmm"
        mod["lmm"]["dep_var"] = f
        mod["lmm"]["fxeff"] = [] 
        mod["lmm"]["rneff"] = [var+"*state_severity|session"]
        mod["est"]["nchains"] = 2
        mod["est"]["nsamples"] = 3000
        mod["name"] = "F"+str(f_idx)+"_state_" + short_identifier + "_"+str(mod["est"]["nchains"])+"_"+str(mod["est"]["nsamples"])
        mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"]) 
        mod["location"] = os.path.join(root_dir, "output", "models", "model_data", mod["name"]+".dic" )

        mod, res,m = mm.estimate_lmm(mod=mod, data=dfl, override=0)
        models[mod["name"]] = mod
        mm.save_model_info(models, os.path.join(root_dir, "output", "models", "model_database.json"))

        d = res.posterior[var+":state_severity|session"].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d).T
        dt.columns = d.coords["session__factor_dim"]
        dt= dt.melt(var_name="session", value_name="sample").reset_index(drop=True)
        dt["session"] = dt["session"].astype("int")

        fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(12, 4))
        # plot the median-splitd ata
        sns.lineplot(data=data.loc[data["variable"].isin([f])], x="session", y="state_severity", hue=splitvar, palette=sns.color_palette("mako_r", 3), ax=axs[0])

        # plot the CI97 posterior for the interaction with anxiety
        axs[1] = sns.pointplot(x="session", y= "sample",
                      data=dt, dodge=.532, join=False, palette=pal,
                      markers="d", scale=.75, ci=None);

        bounds = dt.groupby('session')['sample'].quantile((0.03,0.97)).unstack().reset_index().sort_values(by="session")
        axs[1].vlines(bounds.session, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
        axs[1].axhline(0, alpha=0.2, color='gray')
        #ax.fill_between(x=bounds.session,y1=bounds.iloc[:,1],y2=bounds.iloc[:,2],alpha=0.1)
        axs[1].set_title(f)
        axs[1].set_ylabel("beta")


# #### M2: *severity as fixed effect (across sessions)

# In[ ]:


#trait_factor_names = []#, "TF2_Positive", "TF3_Catastrophizing","TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"]
trait_factor_names = ["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing","TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"]#,
trait_factor_names = ["TF4_Physiological_Anx"]#,

for tt in trait_factor_names:
    tdf = pd.DataFrame()
    for f, f_idx in zip(factors, np.arange(1, len(factors)+1)):
        print("F"+str(f_idx)+"_"+f)
        dfl = df.loc[:,[f, tt,"session", "state_severity"]].dropna()
        dfl["session"] = pd.Categorical(dfl["session"] )

        #mm.models_init(os.path.join(root_dir, "output", "models", "model_database.json"))

        models = json.load(open(os.path.join(root_dir, "output", "models", "model_database.json"), "r")) 
        if tt+"__"+"F"+str(f_idx)+"_state_anx_fxeff_2_3000" in models.keys():
            mod = models[tt+"__"+"F"+str(f_idx)+"_state_anx_fxeff_2_3000"]
        else:
            mod = mm.get_template()

        mod["type"] = "lmm"
        mod["lmm"]["dep_var"] = f
        mod["lmm"]["fxeff"] = [tt+"*state_severity"] 
        mod["lmm"]["rneff"] = [tt+"*state_severity|session"]
        mod["est"]["nchains"] = 2
        mod["est"]["nsamples"] = 3000
        mod["name"] = tt+"__"+"F"+str(f_idx)+"_state_anx_"+str(mod["est"]["nchains"])+"_"+str(mod["est"]["nsamples"])
        mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"]) 
        mod["location"] = os.path.join(root_dir, "output", "models", "model_data", "state_severity_interactions", mod["name"]+".dic" )

        mod, res, m = mm.estimate_lmm(mod=mod, data=dfl, override=1)
        models[mod["name"]] = mod
        mm.save_model_info(models, os.path.join(root_dir, "output", "models", "model_database.json"))

        # get posterior
        d = res.posterior[tt+":state_severity"].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt["factor"] = f
        tdf = pd.concat([tdf, dt])

    fig, axs = plt.subplots(nrows=1, ncols=1, figsize=(7, 4))
    # plot the CI97 posterior for the interaction with anxiety
    axs = sns.pointplot(x="factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=pal,
                  markers="d", scale=.75, ci=None);

    bounds = tdf.groupby('factor')['sample'].quantile((0.03,0.97)).unstack().reset_index().sort_values(by="factor")
    axs.vlines(bounds.factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs.axhline(0, alpha=0.2, color='gray')
    #ax.fill_between(x=bounds.session,y1=bounds.iloc[:,1],y2=bounds.iloc[:,2],alpha=0.1)
    #axs[0].set_title(f)
    axs.set_ylabel("beta")

   


# In[ ]:





# In[ ]:


az.summary(res)
az.plot_posterior(res, "stai_ta:state_severity")


# In[ ]:


dt = pd.DataFrame(d).T
dt.columns = d.coords["session__factor_dim"]
dt= dt.melt(var_name="session", value_name="sample").reset_index(drop=True)
dt["session"] = dt["session"].astype("int")

fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(12, 4))
# plot the median-splitd ata
sns.lineplot(data=data.loc[data["variable"].isin([f])], x="session", y="state_severity", hue=splitvar, palette=sns.color_palette("mako_r", 3), ax=axs[0])

# plot the CI97 posterior for the interaction with anxiety
axs[1] = sns.pointplot(x="session", y= "sample",
              data=dt, dodge=.532, join=False, palette=pal,
              markers="d", scale=.75, ci=None);

bounds = dt.groupby('session')['sample'].quantile((0.03,0.97)).unstack().reset_index().sort_values(by="session")
axs[1].vlines(bounds.session, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
axs[1].axhline(0, alpha=0.2, color='gray')
#ax.fill_between(x=bounds.session,y1=bounds.iloc[:,1],y2=bounds.iloc[:,2],alpha=0.1)
axs[1].set_title(f)
axs[1].set_ylabel("beta")


# In[ ]:


not(os.path.exists(mod["location"]))


# In[ ]:


mod["location"]


# In[ ]:





# In[ ]:


#az.plot_trace(res)
#az.summary(res)
    
    


# In[ ]:





# In[ ]:





# #### Plot random slopes of TA and State severity

# In[ ]:





# In[ ]:


pd.DataFrame(results.posterior["stai_ta:state_severity|session"].stack(draws=("chain", "draw"))).T.hist()


# In[ ]:


results.posterior["stai_ta:state_severity|session"].stack(draws=("chain", "draw"))

plt.figure()

az.plot_forest(
    results,
    var_names=["stai_ta:state_severity|session"],
    figsize=(8, 12),
    combine_dims={"draw", "chain"}
);

