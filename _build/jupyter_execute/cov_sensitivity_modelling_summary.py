#!/usr/bin/env python
# coding: utf-8

# ## Severity - modelling summary
# 
# Here I picked on the key modelling results

# In[1]:


from groo.groo import get_root
root_dir = get_root(".root_dir_covid")
import os
os.chdir(root_dir)
import sys
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))

import numpy as np
import pandas as pd
from cov_functions import *
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
import bambi as bmb
import arviz as az
import json
import pickle
import bammm.bammm as mm
import patsy
from IPython import display


# ### Covid-related variables being predicted (reminder)

# In[2]:


df = pd.read_csv(os.path.join(root_dir, 'output', 'questionnaires', 'covid_factors_overview.csv')).filter(regex='^ML', axis=1)
df=df.rename(columns={"ML1":"F3_Economic_Impact_Worry", "ML2":"F5_Worry", "ML3":"F1_Close_Person_Worry", "ML4":"F4_Prob_Estimates", 
                                       "ML5":"F2_Anxiety_Avoidance", "ML6":"F6_Skepticism"})
for i in df.columns:
    df[i]  = df[i].astype(str).str.replace('nan', '')
    
df


# ### Trait variables (predictors) 

# In[3]:


trait_factor_names = ["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing",  "TF4_Physiological_Anx",  "TF5_Depression",  "TF6_Cognitive_Anx"]
raw_trait_factor_names = ["ML"+str(i) for i in range(1,7)]

trait_factor_dict = dict(zip(raw_trait_factor_names, trait_factor_names))

df = pd.read_csv(os.path.join(root_dir, 'output', 'questionnaires', 'trait_factors_overview.csv')).filter(regex='^ML', axis=1)

df=df.rename(columns=trait_factor_dict)
for i in df.columns:
    df[i]  = df[i].astype(str).str.replace('nan', '')
    
df


# ### Worry about close person (also a bit of media frequency)
# 
# 1. State severity overall increses worry about close person 
# 2. Catastrophizing and Physiological anxiety increase overall close person worry
# 3. State severity interacts negatively with catastrophizing - people who are higher in cat tend to be less impacted  **investigate further (check for ceiling effect)**
# 4. This relationship between severity and cat changes over time - **investigate further**
# 4. State severity interacts negatively with physiological anxiety - people who are higher in phys anx tend to be less impacted by severity  **investigate further (check for ceiling effect)**
# 5. State severity interacts negatively with discontent - people who are higher in discontent tend to be less impacted by severity  **investigate further (check for ceiling effect)**
# 
# 

# In[4]:


ylims = [[-1,1], [-0.3, 0.3], [-0.02, 0.02]]
model_constant = "severity_slopes_and_session_ND"
folder = "traits_predicting_severity_correlations"

f = "F1_Close_Person_Worry"
f_idx = 1
models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
mod = models["F"+str(f_idx)+"_"+model_constant+"_4_3000"]
mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
mod, res, m = mm.estimate_lmm(mod, [], override=0)
display.display(az.summary(res).head(15))
# PLOT
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                    ['state_severity:'+i for i in trait_factor_names],
                                    ['state_severity:'+i+":session" for i in trait_factor_names]]):
    tdf = pd.DataFrame()
    for tf in beta_group:
        d = res.posterior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf
    axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs[gr_idx].axhline(0, alpha=0.2, color='gray')
    if gr_idx == 0:
        axs[gr_idx].set_title(f)
    elif gr_idx == 1:
        axs[gr_idx].set_title("corr of "+f+" and severity")
    axs[gr_idx].set_ylabel("beta (ci95)")
    axs[gr_idx].set_xlabel("")
    axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
    axs[gr_idx].set_ylim(ylims[gr_idx])


# ### Economic worry (i.e. )
# 
# 1. State severity overall increses economic worry
# 2. Catastrophizing increases overall economic worry
# 3. State severity interacts negatively with catastrophizing - people who are higher in cat tend to be less impacted  **investigate further (check for ceiling effect)**
# 4. This relationship between severity and cat changes over time - **investigate further**
# 5. Economic worry is increased by interactio nof state severity and cognitive anxiety 

# In[5]:


ylims = [[-1,1], [-0.3, 0.3], [-0.02, 0.02]]
model_constant = "severity_slopes_and_session_ND"
folder = "traits_predicting_severity_correlations"

f = "F3_Economic_Impact_Worry"
f_idx = 3
models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
mod = models["F"+str(f_idx)+"_"+model_constant+"_4_3000"]
mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
mod, res, m = mm.estimate_lmm(mod, [], override=0)
display.display(az.summary(res).head(15))
# PLOT
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                    ['state_severity:'+i for i in trait_factor_names],
                                    ['state_severity:'+i+":session" for i in trait_factor_names]]):
    tdf = pd.DataFrame()
    for tf in beta_group:
        d = res.posterior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf
    axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs[gr_idx].axhline(0, alpha=0.2, color='gray')
    if gr_idx == 0:
        axs[gr_idx].set_title(f)
    elif gr_idx == 1:
        axs[gr_idx].set_title("corr of "+f+" and severity")
    axs[gr_idx].set_ylabel("beta (ci95)")
    axs[gr_idx].set_xlabel("")
    axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
    axs[gr_idx].set_ylim(ylims[gr_idx])


# 
# ### Anxiety/Avoidance
# What predicts levels of anxiety/avoidance
# 
# 1. State severity increases anxiety/avoidence in general
# 2. Catastrohpizing (future-oriented) trait generally positively predicts anx/avoid
# 3. Depression (TF5) negatively interacts with state severity - objective increase in severity has less impact in depressed individuals

# In[9]:





# In[15]:





# In[6]:


ylims = [[-1,1], [-0.3, 0.3], [-0.02, 0.02]]
model_constant = "severity_slopes_and_session_ND"
folder = "traits_predicting_severity_correlations"

f = "F2_Anxiety_Avoidance"
f_idx = 2
models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
mod = models["F"+str(f_idx)+"_"+model_constant+"_4_3000"]
mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
mod, res, m = mm.estimate_lmm(mod, [], override=0)
display.display(az.summary(res).head(15))
# PLOT
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                    ['state_severity:'+i for i in trait_factor_names],
                                    ['state_severity:'+i+":session" for i in trait_factor_names]]):
    tdf = pd.DataFrame()
    for tf in beta_group:
        d = res.posterior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf
    axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs[gr_idx].axhline(0, alpha=0.2, color='gray')
    if gr_idx == 0:
        axs[gr_idx].set_title(f)
    elif gr_idx == 1:
        axs[gr_idx].set_title("corr of "+f+" and severity")
    axs[gr_idx].set_ylabel("beta (ci95)")
    axs[gr_idx].set_xlabel("")
    axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
    axs[gr_idx].set_ylim(ylims[gr_idx])


# ### Probability estimates (estimated probability of aversive events)
# 1. state severity increases probability estimates
# 2. Catastrophising increases estimated probability of aversive events
# 3. Physiological anxiety increases estimated probability of aversive events
# 4. Cognitive anxiety decreases estimated probability of aversive events
# 5. "Positive" (discontent?) decreases estimated probability of aversive events
# 6. Objectve seveity interacts with physiological anx to reduce probability estimates (**investigate further (check for ceiling effect)**)

# In[7]:


ylims = [[-1,1], [-0.3, 0.3], [-0.02, 0.02]]
model_constant = "severity_slopes_and_session_ND"
folder = "traits_predicting_severity_correlations"

f = "F4_Prob_Estimates"
f_idx = 4
models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
mod = models["F"+str(f_idx)+"_"+model_constant+"_4_3000"]
mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
mod, res, m = mm.estimate_lmm(mod, [], override=0)
display.display(az.summary(res).head(15))
# PLOT
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                    ['state_severity:'+i for i in trait_factor_names],
                                    ['state_severity:'+i+":session" for i in trait_factor_names]]):
    tdf = pd.DataFrame()
    for tf in beta_group:
        d = res.posterior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf
    axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs[gr_idx].axhline(0, alpha=0.2, color='gray')
    if gr_idx == 0:
        axs[gr_idx].set_title(f)
    elif gr_idx == 1:
        axs[gr_idx].set_title("corr of "+f+" and severity")
    axs[gr_idx].set_ylabel("beta (ci95)")
    axs[gr_idx].set_xlabel("")
    axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
    axs[gr_idx].set_ylim(ylims[gr_idx])


# ### Covid-related worry 
# 1. state severity increases covid-worry
# 2. Catastrophising increases covid-worry
# 3. "Discontent" increases covid-worry
# 4. Cognitive anxiety *decreases* covid worry ovrall but state severtiy increases it

# In[8]:


ylims = [[-1,1], [-0.3, 0.3], [-0.02, 0.02]]
model_constant = "severity_slopes_and_session_ND"
folder = "traits_predicting_severity_correlations"

f = "F5_Worry"
f_idx = 5
models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
mod = models["F"+str(f_idx)+"_"+model_constant+"_4_3000"]
mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
mod, res, m = mm.estimate_lmm(mod, [], override=0)
display.display(az.summary(res).head(15))
# PLOT
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                    ['state_severity:'+i for i in trait_factor_names],
                                    ['state_severity:'+i+":session" for i in trait_factor_names]]):
    tdf = pd.DataFrame()
    for tf in beta_group:
        d = res.posterior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf
    axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs[gr_idx].axhline(0, alpha=0.2, color='gray')
    if gr_idx == 0:
        axs[gr_idx].set_title(f)
    elif gr_idx == 1:
        axs[gr_idx].set_title("corr of "+f+" and severity")
    axs[gr_idx].set_ylabel("beta (ci95)")
    axs[gr_idx].set_xlabel("")
    axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
    axs[gr_idx].set_ylim(ylims[gr_idx])


# ### Skepticism
# 1. State severity is *negatively associated* with state skepticism
# 2. Catastrophizing is negatively associated with skepticism

# In[9]:


ylims = [[-1,1], [-0.3, 0.3], [-0.02, 0.02]]
model_constant = "severity_slopes_and_session_ND"
folder = "traits_predicting_severity_correlations"

f = "F6_Skepticism"
f_idx = 6
models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
mod = models["F"+str(f_idx)+"_"+model_constant+"_4_3000"]
mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
mod, res, m = mm.estimate_lmm(mod, [], override=0)
display.display(az.summary(res).head(15))
# PLOT
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                    ['state_severity:'+i for i in trait_factor_names],
                                    ['state_severity:'+i+":session" for i in trait_factor_names]]):
    tdf = pd.DataFrame()
    for tf in beta_group:
        d = res.posterior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf
    axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs[gr_idx].axhline(0, alpha=0.2, color='gray')
    if gr_idx == 0:
        axs[gr_idx].set_title(f)
    elif gr_idx == 1:
        axs[gr_idx].set_title("corr of "+f+" and severity")
    axs[gr_idx].set_ylabel("beta (ci95)")
    axs[gr_idx].set_xlabel("")
    axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
    axs[gr_idx].set_ylim(ylims[gr_idx])


# ### Summary
# 
# **Worry about close person (also a bit of media frequency)**
# 
# 1. State severity overall increses worry about close person 
# 2. Catastrophizing and Physiological anxiety increase overall close person worry
# 3. State severity interacts negatively with catastrophizing - people who are higher in cat tend to be less impacted  **investigate further (check for ceiling effect)**
# 4. This relationship between severity and cat changes over time - **investigate further**
# 4. State severity interacts negatively with physiological anxiety - people who are higher in phys anx tend to be less impacted by severity  **investigate further (check for ceiling effect)**
# 5. State severity interacts negatively with discontent - people who are higher in discontent tend to be less impacted by severity  **investigate further (check for ceiling effect)**
# 
# **Economic worry**
# 1. State severity overall increses economic worry
# 2. Catastrophizing increases overall economic worry
# 3. State severity interacts negatively with catastrophizing - people who are higher in cat tend to be less impacted  **investigate further (check for ceiling effect)**
# 4. This relationship between severity and cat changes over time - **investigate further**
# 5. Economic worry is increased by interactio nof state severity and cognitive anxiety 
# 
# **Anxiety/Avoidance**
# What predicts levels of anxiety/avoidance
# 
# 1. State severity increases anxiety/avoidence in general
# 2. Catastrohpizing (future-oriented) trait generally positively predicts anx/avoid
# 3. Depression (TF5) negatively interacts with state severity - objective increase in severity has less impact in depressed individuals
# 
# **Probability estimates (estimated probability of aversive events)**
# 1. state severity increases probability estimates
# 2. Catastrophising increases estimated probability of aversive events
# 3. Physiological anxiety increases estimated probability of aversive events
# 4. Cognitive anxiety decreases estimated probability of aversive events
# 5. "Positive" (discontent?) decreases estimated probability of aversive events
# 6. Objectve seveity interacts with physiological anx to reduce probability estimates (**investigate further (check for ceiling effect)**)
# 
# **Covid-related worry** 
# 1. state severity increases covid-worry
# 2. Catastrophising increases covid-worry
# 3. "Discontent" increases covid-worry
# 4. Cognitive anxiety *decreases* covid worry ovrall but state severtiy increases it
# 
# **Skpeticism**
# 1. State severity is *negatively associated* with state skepticism
# 2. Catastrophizing is negatively associated with skepticism
# 
# ### To do 
# 1. Check variance inflation in models. 
# 2. Check for ceiling effects 

# In[10]:


df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete.csv"))
df["session"] =  df["session"] + 1
dfl = df.loc[:,[f, "state_severity", "PROLIFICID", "session"]+trait_factor_names].dropna()

ylims = [[-1,1], [-0.3, 0.3], [-0.02, 0.02]]
model_constant = "severity_slopes_and_session_ND"
folder = "traits_predicting_severity_correlations"

f = "F6_Skepticism"
f_idx = 6
models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
mod = models["F"+str(f_idx)+"_"+model_constant+"_4_3000"]
mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
mod, res, m = mm.estimate_lmm(mod, dfl, override=0)
display.display(az.summary(res).head(15))
# PLOT
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                    ['state_severity:'+i for i in trait_factor_names],
                                    ['state_severity:'+i+":session" for i in trait_factor_names]]):
    tdf = pd.DataFrame()
    for tf in beta_group:
        d = res.posterior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf
    axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs[gr_idx].axhline(0, alpha=0.2, color='gray')
    if gr_idx == 0:
        axs[gr_idx].set_title(f)
    elif gr_idx == 1:
        axs[gr_idx].set_title("corr of "+f+" and severity")
    axs[gr_idx].set_ylabel("beta (ci95)")
    axs[gr_idx].set_xlabel("")
    axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
    axs[gr_idx].set_ylim(ylims[gr_idx])


# In[ ]:





# In[11]:


eq = mod["lmm"]["eq"]
m = bmb.Model(eq, tdf, family="gaussian", link="identity")
m.build()
prior = m.prior_predictive(1000)


# In[25]:


# plot prior
fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(18, 6))
for gr_idx, beta_group in enumerate([trait_factor_names+['state_severity'],
                                    ['state_severity:'+i for i in trait_factor_names],
                                    ['state_severity:'+i+":session" for i in trait_factor_names]]):
    tdf = pd.DataFrame()
    for tf in beta_group:
        d = prior.prior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"trait_factor"] = tf
        tdf = pd.concat([tdf, dt])
    tdf
    axs[gr_idx] = sns.pointplot(x="trait_factor", y= "sample",
                  data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                  markers="d", scale=.75, ci=None, ax =axs[gr_idx]);
    bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
    axs[gr_idx].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
    axs[gr_idx].axhline(0, alpha=0.2, color='gray')
    if gr_idx == 0:
        axs[gr_idx].set_title(f)
    elif gr_idx == 1:
        axs[gr_idx].set_title("corr of "+f+" and severity")
    axs[gr_idx].set_ylabel("beta (ci95)")
    axs[gr_idx].set_xlabel("")
    axs[gr_idx].set_xticklabels(beta_group, rotation = 30, ha="right")
    #axs[gr_idx].set_ylim(ylims[gr_idx])

