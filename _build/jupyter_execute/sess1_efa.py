#!/usr/bin/env python
# coding: utf-8
---
title: 'EFA: Session 1'
author: "Ondrej Zika"
date: "1/10/2022"
output: html_document
---
# In[1]:


# install core package venv and let it handle the rest
if (!requireNamespace("pacman")) install.packages("pacman")
packages_cran <- c("renv")
pacman::p_install(packages_cran)

# load packages
library(psych)
library(GPArotation)
library(corrplot)
#library(data.table)
library(ggplot2)
library(dplyr)
library(tidyr)

source("lib/utils.R")
here::i_am("flag_project_root.R")
mainf <- here::here()
rootf <- here::here("")
dataf <- here::here("data/")
outputf <- here::here("output/")


# ## EFA: Session 1
# 
# ### Analysis description 
# - only session 1
# - only continuous variables
# - just to check how things are running, e.g. how to select factor numbers 
# - steps roughly based on [Modern Psychometrics in R: Chapter 2](https://link.springer.com/book/10.1007/978-3-319-93177-7)
# 
# ### Overview of variables

# In[ ]:


# 1) Load csv with information about variable types 
vardf <- read.csv(paste0(outputf, "/questionnaires/covid_questionnaire_questions_overview.csv"))
print(vardf)


# ### Preparatory steps

# In[ ]:


# Preparatory steps

#vars2use <- vardf[vardf$fa_incl==1,["varname"]]

vars2use <- vardf[vardf$fa_incl==1,"varname"]

# read preprocessed data from session 1 
df<-read.csv(paste0(dataf, "/session1/clean_dataset.csv"))
df.orig <- df

# filter only relevant columns
df <- df[,colnames(df) %in% vars2use]

# replace missing values with median (controversial?, cor() can't deal with it but it seems that the fa() package does the same)
for(i in 1:ncol(df)){
  df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
}


# ### Correlation matrix (Spearman)

# In[ ]:


# Create correlation matrix 
cdf = cor(df, method = c("spearman")) #how does cor2 deal with missing values?
cp <- corrplot(cdf) 


# ### Check for how many factors
# #### Eigenvalues

# In[ ]:


## Factor number assessment: 
# a/ Eigenvalues
evals <- eigen(cdf)$values 
scree(cdf, factors = FALSE)


# #### Parallel analysis

# In[ ]:


# b/ Parallel analysis
set.seed(123) 
resPA <- fa.parallel(df, fa = "pc", cor = "cor",fm = "ml")


# #### Very simple structure

# In[ ]:


# c/ Very simple srtucutre
resvss <- vss(cdf, fm = "ml", n.obs = nrow(df), plot = TRUE) 
resvss


# ### Run EFA

# In[ ]:


nFactors = 6
# Run factor analysis
fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "ml", scores = "regression")
fa3.load <- data.frame(unclass(fa3$loadings))
fa3.load$varname <- rownames(fa3.load)
fa3.load <- merge(fa3.load, vardf, by="varname")

# save the estimated loadings
save(list = c("df", "fa3", "fa3.load"), file = paste0(outputf, "/fa/fa_sess1_loadings.Rdata"))


# ### Visualize EFA: psych package diagram

# In[ ]:


# I don't like this plot very much so better plots below
fa.diagram(fa3)


# In[ ]:


# I don't like this plot very much so better plots below



# ### Loadings and pre-defined categories
# - we have created the questionnaire to roughly reflect: objective measures, affective measures (worries and avoidance behaviour), probability estimates of aversive events, time estimates)
# - here I just show how the estimated loadings map onto our guesses

# In[ ]:


library(reshape2)
fa3.load.long <- fa3.load %>% 
         melt(id.vars = c("varname", "agg_manual"),  # variables to carry forward
              measure.vars = colnames(fa3.load)[grepl("ML",colnames(fa3.load))],  #variables to stack
              value.name = "loading",     # name of the value variable 
              variable.name = "factor" ) # name of the variable   

## Question 1: How do the pre-defined categories                      
pal<-assign_colors("manual-cats")
g <- ggplot(fa3.load.long, aes(x=varname, y=loading, fill=agg_manual)) +
  geom_bar(stat="identity") +
  scale_fill_manual(values =pal, name = "Manual labels (pre-FA)") +
  facet_grid(rows=vars(factor),  scales="free") + 
  scale_x_discrete() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
g


# ### Loadings by factor
# - I just highlighted indicators with higher loading than 0.2
# - most seem to make sense (yay)

# In[ ]:


## Question 2: Which questions factor together
n.fac <- length(unique(fa3.load.long$factor))
# just some manual labelling
#factor_names <- c("F1: close-person-threat", "F2: economic", "F3: worry", "F4: infection-prob", "F5: state", "F6: anx-avoidance", "F7: time-pred", "F8: skeptics", "F9: successful-avoidance")
factor_names <- sprintf("F%s",seq(1:nFactors))
# print a quick overview of what indicators are groupped together 
fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])][,c("varname",sprintf("ML%s",seq(1:nFactors)))]
fa3.load$factor <- apply( abs(fa3.load[,colnames(fa3.load)[grepl("ML",colnames(fa3.load))]]),  1, which.max)
#cfa.str <- ""
#for (i in seq(nFactors)) {cfa.str <- paste(cfa.str, "Factor ", toString(i), " (", factor_names[i], "):\n", paste(fa3.load[fa3.load$factor==i,"varname"], collapse = " + "), "\n\n", sep="")}
#cat(cfa.str)

## better way to display
fa3.load$idx <- seq(nrow(fa3.load))
library("textshape")
temp <- fa3.load[,c("idx","factor","varname")] %>%
  pivot_wider(names_from = "factor", values_from = "varname")
temp <- temp[,!(colnames(temp) %in% c("idx"))]
temp <- plyr::ldply(apply(temp, 2, sort, decreasing=F),rbind)
temp = t(column_to_rownames(temp, loc = 1))
colnames(temp) <- sprintf("F%s",colnames(temp))

temp <- temp[,sprintf("F%s",seq(nFactors))]
colnames(temp) <- factor_names
print(as.table(temp))

for (i in 1:n.fac) {
  fa3.load.long$current_fac <- 0
  fa3.load.long$current_fac[fa3.load.long$factor %in% paste0("ML",toString(i)) & abs(fa3.load.long$loading) > 0.2] <- 1
  
  tdf <- fa3.load.long[fa3.load.long$factor %in% paste0("ML",toString(i)),]
  g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
  geom_bar(stat="identity") +
  ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
  print(g)
}

#write.csv(data.frame(temp), here::here(outputf, "questionnaires", "covid_factors_overview.csv")) 


# ### Factor scores correlation with individual measures
# - not very informative, the results are very similar to when we group the variables based on our pre-defined categories 
# 

# In[ ]:


## Factor scores
fa3.scores <- data.frame(fa(df, nfactors = nFactors, rotate = "oblimin", fm = "ml", scores="regression", missing=TRUE, impute="median")$scores)
colnames(fa3.scores) <- factor_names
fa3.df <- cbind(df.orig, fa3.scores )
psych.vars <- c("stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", factor_names)

# filter only relevant columns
fa3.df <- fa3.df[,colnames(fa3.df) %in% psych.vars]

# replace missing values with median 
for(i in 1:ncol(fa3.df)){
  fa3.df[is.na(fa3.df[,i]), i] <- median(fa3.df[,i], na.rm = TRUE)
}


# Create correlation matrix 
fa3.cdf = cor(fa3.df, method = c("pearson")) #how does cor2 deal with missing values?
cp2 <- corrplot(fa3.cdf, method = "number") 


# 
# 
# 
# 
