#!/usr/bin/env python
# coding: utf-8

# ## Information seeking

# In[ ]:





# In[1]:


from groo.groo import get_root
root_dir = get_root(".root_dir_covid")
    
import os
import sys
print(os.path.join(root_dir, "covid-fear", "scripts"))
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))


import numpy as np
import pandas as pd
from cov_functions import *
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
import warnings
import itertools
warnings.filterwarnings('ignore')
from scipy.spatial.distance import correlation as Dcorr
import bambi as bmb
import arviz as az



# ### Load main dataset

# In[2]:


df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete.csv"))


# ### Overview of the used question
# **How often did you get information about COVID-19 from the media?** 
# (*in the time period since the last survey*, roughly 14 days)
# 
# - Multiple times per day (5)
# - Once a day (4)
# - 3-4 times a week (3) 
# - Once a week (2)
# - Few times a month (1) 
# - Less than few times a month (0)
# 

# In[3]:


var = ["q6_media_freq_num", "q6_media_valence"]
lbl = ["Media frequency", "Media valence"]

time_var = ["stai_ta", "TF1_Worry", "TF2_Self_Worth", "TF3_Catastrophizing",  "TF4_Anxiety",  "TF5_Depression"]  
time_var_hum = ["STAI-TRAIT", "TF1: Worry", "TF2: Self worth", "TF3: Catastrophizing", "TF4: Anxiety", "TF5: Depression"]
# loop over the different 

for tv, tv_h in zip(time_var, time_var_hum):
    for vidx, v in enumerate(var):

        #country
        fig, axs = plt.subplots(ncols=3, figsize=(16,4))
        pal = sns.color_palette("rocket")
        sns.pointplot(x="session", y=v,
                      data=df, dodge=.532, join=True, color=pal[3],
                      markers="d", scale=.75, ci=68, ax=axs[0]);
        sns.pointplot(x="session", y=v, hue="GROUP",
                      data=df, dodge=.532, join=True, palette=sns.color_palette("viridis",2),
                      markers="d", scale=.75, ci=68, ax=axs[0]);
        plt.title(lbl[vidx]);

        #anxiety
        sns.pointplot(x="session", y=v, hue=tv+"_ms",
                      data=df, dodge=.1, join=True, palette=sns.color_palette("rocket",2),
                      markers="d", scale=.75, ci=68, ax=axs[1]);
        
        sns.pointplot(x="session", y=v, hue=tv+"_ts", hue_order=["low", "mid", "high"],
                      data=df, dodge=.1, join=True, palette=sns.color_palette("rocket",3),
                      markers="d", scale=.75, ci=68, ax=axs[2]);
        plt.title(lbl[vidx]);

    g = sns.FacetGrid(df, col="session", height=4, col_wrap=4)
    g.map(sns.regplot, "q6_media_freq_num", tv, color=".3")
    g.set_ylabels(tv_h)
#g.map(corrfunc_r, x="q6_media_freq_num", y="stai_ta")

#.set(ylim=(-1, 11), yticks=[0, 5, 10])


# ### model

# In[4]:


dfl = df.loc[:,["q6_media_freq_num", "stai_ta","session", "sr_age", "sr_gender", "GROUP"]].dropna()
dfl["session"] = pd.Categorical(dfl["session"] )
model = bmb.Model('q6_media_freq_num ~ (stai_ta|session)', dfl)
results = model.fit(draws=5000, chains=2)


# In[5]:


az.plot_trace(results)
az.summary(results)


# In[6]:


plt.figure()

az.plot_forest(
    results,
    var_names=["stai_ta|session"],
    figsize=(8, 12),
    combine_dims={"draw", "chain"}
);

