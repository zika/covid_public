#!/usr/bin/env python
# coding: utf-8

# In[1]:


from groo.groo import get_root
root_dir = get_root(".root_dir_covid")

import os

print(root_dir)
import sys
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))


import numpy as np
import pandas as pd
from cov_functions import *
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
import warnings
import itertools
warnings.filterwarnings('ignore')
from scipy.spatial.distance import correlation as Dcorr
import bambi as bmb
import arviz as az
import json
import pickle
import cov_model_management as mm
import ipywidgets as ipw



# load main dataset
df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete.csv"))


# ### Interactive plot of correlations with state severity

# In[2]:


factors =[ "F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism"]
trait_factor_names = ["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing","TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"]


# In[3]:


data = df


# - what  characterizes people who decouple from reality, look at their IDs
# - separate from 
# - prob/worry is it about objective/sibjective 

# In[4]:


tab_contents = trait_factor_names
children = [ipw.Text(description=name) for name in tab_contents]
tab = ipw.Tab()
tab.children = children
for idx, name in enumerate(tab_contents):
    tab.set_title(idx, name)
tab


# In[5]:


widget1 = 1

if widget1 == 1:
    def plot_state_severity(covid_factor, trait_factor):
        ax = sns.lmplot(
            data=data, y="state_severity", x=covid_factor,
            hue=trait_factor+"_ts", ci=68, row="session", col = trait_factor+"_ts", col_order=["low", "mid", "high"], palette=sns.color_palette("mako_r", 3),
            height=4, aspect=1
        )
        ax.map(corrfunc, covid_factor, "state_severity", drop_missing=True)
    var1 = ipw.Dropdown(options=factors, value=factors[0], description="Select a COVID factor")
    var2 = ipw.Dropdown(options=trait_factor_names, value=trait_factor_names[0], description="Select a TRAIT factor")

    ipw.interact(plot_state_severity, covid_factor = var1, trait_factor = var2)

