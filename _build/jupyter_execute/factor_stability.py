#!/usr/bin/env python
# coding: utf-8
---
title: 'EFA: Factor stability'
author: "Ondrej Zika"
date: "1/10/2022"
output: html_document
---
# In[1]:


# install core package venv and let it handle the rest
if (!requireNamespace("pacman")) install.packages("pacman")
packages_cran <- c("renv")
pacman::p_install(packages_cran)

# load packages
library(psych)
library(GPArotation)
library(corrplot)
#library(data.table)
library(ggplot2)
library(dplyr)
library(tidyr)
library(reshape2)
library(textshape)
library(sjmisc)

here::i_am("flag_project_root.R")
mainf <- here::here()
rootf <- here::here("")
dataf <- here::here("data/")
outputf <- here::here("output/")
source(here::here("covid-fear/scripts/factor_analysis/lib/utils.R"))


# ## Assesing data stability
# ### Suggested number of factorss

# In[ ]:


# 1) Load csv with information about variable types 
vardf <- read.csv(paste0(outputf, "/questionnaires/covid_questionnaire_questions_overview.csv"))
vars2use <- vardf[vardf$fa_incl==1,"varname"]

sessions = c("1", "2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15")
#sessions = c("1", "5");#, "2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15")


# In[ ]:


### first the data need to be z-scored across sessions
for (s in sessions) {
  prefix = "";sid = s
  if (str_contains(s, "extra")) { prefix="extra_"; sid=substr(s, nchar(s), nchar(s)) } 
  if (s == "1") {
      dftemp <- read.csv(paste0(dataf, "/", prefix, "session", sid, "/clean_dataset.csv"))
      dftemp<-dftemp[unique(dftemp$PROLIFICID),]
      dftemp$sid <- s
      for(i in vars2use){
        dftemp[is.na(dftemp[,i]), i] <- median(dftemp[,i], na.rm = TRUE)
      }
      df.allsess <- dftemp[,colnames(dftemp) %in% c(as.character(vars2use), "PROLIFICID", "SESSIONID", "sid")]
  } else {
      dftemp <- read.csv(paste0(dataf, "/", prefix, "session", sid, "/clean_dataset.csv"))
      dftemp<-dftemp[unique(dftemp$PROLIFICID),]
      dftemp$sid <- s
      for(i in vars2use){
        dftemp[is.na(dftemp[,i]), i] <- median(dftemp[,i], na.rm = TRUE)
      }
      df.allsess <- rbind(df.allsess, dftemp[,colnames(dftemp) %in% c(as.character(vars2use), "PROLIFICID", "SESSIONID","sid")])
  }
}
for(i in vars2use){
    #df.allsess[,i] <- (df.allsess[,i] - mean(df.allsess[,i],na.rm = TRUE)) / sd(df.allsess[,i],na.rm = TRUE)
    #print(unique(df.allsess[,i]))
    df.allsess[,i] <- (df.allsess[,i] - min(df.allsess[,i]))  / (max(df.allsess[,i]) - min(df.allsess[,i])) 
}


# In[ ]:


df.all <- data.frame()
sno <- 1
nFactors = 6

for (s in sessions) {
  prefix = "";sid = s
  if (str_contains(s, "extra")) { prefix="extra_"; sid=substr(s, nchar(s), nchar(s)) } 
  df.reduced  <- df.allsess[df.allsess$sid %in% s,]
  for(i in vars2use){
    df.reduced[is.na(df.reduced[,i]), i] <- median(df.reduced[,i], na.rm = TRUE)
  }
  
  df <- df.reduced[,colnames(df.reduced) %in% as.character(vars2use)]
  set.seed(123) 
  #resPA <- fa.parallel(df, fa = "pc", cor = "cor",fm = "ml")

  # Run factor analysis
  cdf = cor(df, method = c("pearson")) #how does cor2 deal with missing values?
  fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "ml", scores = "regression")
  fa3.load <- data.frame(unclass(fa3$loadings))
  if (s=="1") {
    factors.s1 <- fa3.load
  } else {
    # loop over session 1 factors 
    pre <- fa3.load
    fa3.load.temp <- fa3.load
    for (c in colnames(factors.s1)) {
      fa3.load.temp$s1fac <- factors.s1[c]
      # calculate correlations
      cdf = cor(fa3.load.temp, method = c("pearson"))
      # get the row with max correlation - this is how factors are marched
      id = which.max(cdf[nFactors+1,1:nFactors])
      fa3.load[c] <- fa3.load.temp[,id]
    }
  }
  fa3.load$varname <- rownames(fa3.load)
  fa3.load$sessid <- sno
  df.all <- rbind(df.all,fa3.load)
  sno <- sno +1

}

df.all.long <- df.all %>% 
          melt(id.vars = c("varname", "sessid"),  # variables to carry forward
          measure.vars = c(sprintf("ML%d",seq(1:nFactors))),  #variables to stack
          value.name = "loading",     # name of the value variable 
          variable.name = "factor" ) # name of the variable  


  


# 
# 

# In[ ]:


for (c in colnames(factors.s1)) {
  tdf <- select(df.all, c(c,"sessid", "varname"))
  tdf$sessid <- paste0("sess", as.character(df.all$sessid))
  df <- tdf %>%
      pivot_wider(names_from = "sessid", values_from = c)
  df<-as.data.frame(df[,grepl( "sess" , names( df ) )])
  for(i in 1:ncol(df)){
      df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
    }
  cdf = cor(df, method = c("pearson")) #how does cor2 deal with missing values?
  cp <- corrplot(cdf, method="number", title=c,  mar=c(0,0,1,0), col.lim = c(-0.5,1)) 
  mean(cp$corr)
}


# ### Plot distribution of loadings per session

# In[ ]:


i<-1
for (i in 1:nFactors) {
  # Label questions loading higer than 0.1
  df.all.long$current_fac <- 0
  df.all.long$current_fac[df.all.long$factor %in% paste0("ML",toString(i)) & abs(df.all.long$loading) > 0.2] <- 1
  
  # Select data
  tdf <- df.all.long[df.all.long$factor %in% paste0("ML",toString(i)),]
    

  g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
  geom_bar(stat="identity") +
  #ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
    facet_grid(rows=vars(sessid))
  print(g)
  
  
}
### decided factor names
# ML1: Close_Person_Worry
# ML2: Covid-related_Anxiety_Avoidance
# ML3: Economic_Impact_Worry
# ML4: Prob_Infected
# ML5: Covid-related_Worry
# ML6: Covid-skepticism


# ### Loadings session 1 - also save factor names

# In[ ]:


i<-1




# ### Average loadings per factor

# In[ ]:


i<-1
loadings.mean <- df.all.long %>%
  group_by(factor, varname) %>%
  summarise_at("loading", mean, na.rm = TRUE)


for (i in 1:nFactors) {
  # Select data
  tdf <- loadings.mean[loadings.mean$factor %in% paste0("ML",toString(i)),]
    

  g <- ggplot(tdf, aes(x=varname, y=loading)) +
  geom_bar(stat="identity") +
  #ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  #scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))  
    #facet_grid(rows=vars(sessid))
  print(g)
  
  
}

library(tidyr)
lds <- loadings.mean %>%
        pivot_wider(names_from = "factor", values_from = "loading")
cdf = cor(as.matrix(lds[,grepl( "ML",colnames(lds))]), method = c("pearson")) #how does cor2 deal with missing values?

cp <- corrplot(cdf,method = "number") 


# ### Method 1: Get factor scores for averaged loadings / loadings from session 1
# 

# In[ ]:


do.th1 <- 1

  if (do.th1 == 1) {
  sessions = c("1", "2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15")

  ## run FA on session 1 (make sure to use df.allsess which uses data standardized across sessions)
  df.reduced  <- df.allsess[df.allsess$sid %in% "1",]
  cdf = cor(df.reduced[,colnames(df.reduced ) %in% as.character(vars2use)] , method = c("spearman")) #how does cor2 deal with missing values?
  fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "ml")
  fa3.load <- data.frame(unclass(fa3$loadings))
  fa3.load$sessid <- 1
  fa3.load$varname <- rownames(fa3.load)
  fa3.load <- merge(fa3.load, vardf, by="varname")
  fa3.load.long <- fa3.load %>% 
         melt(id.vars = c("varname", "agg_manual"),  # variables to carry forward
              measure.vars = colnames(fa3.load)[grepl("ML",colnames(fa3.load))],  #variables to stack
              value.name = "loading",     # name of the value variable 
              variable.name = "factor" ) # name of the variable   
  orig_factors = unique(fa3.load.long$factor)
  for (i in orig_factors) {
    fa3.load.long$current_fac <- 0
    fa3.load.long$current_fac[fa3.load.long$factor %in% i & abs(fa3.load.long$loading) > 0.2] <- 1
    
    tdf <- fa3.load.long[fa3.load.long$factor %in% i,]
    g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
    geom_bar(stat="identity") +
    ggtitle(paste0("Factor ",i, " \""), "\"")  +
    scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
    print(g)
  }
  
  fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])] #[,c("varname",sprintf("ML%s",seq(1:nFactors)))]
  #fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])][,c("varname",sprintf("ML%s",seq(1:nFactors)))]
  fa3.load$factor <- apply( abs(fa3.load[,colnames(fa3.load)[grepl("ML",colnames(fa3.load))]]),  1, which.max)
  
  fa3.load$idx <- seq(nrow(fa3.load))
  library("textshape")
  temp <- fa3.load[,c("idx","factor","varname")] %>%
    pivot_wider(names_from = "factor", values_from = "varname")
  temp <- temp[,!(colnames(temp) %in% c("idx"))]
  temp <- plyr::ldply(apply(temp, 2, sort, decreasing=F),rbind)
  temp = t(column_to_rownames(temp, loc = 1))
  print(colnames(temp))
  colnames(temp) <- sprintf("F%s",colnames(temp))
  temp <- temp[,sprintf("F%s",seq(nFactors))]
  colnames(temp) <- orig_factors
  print(as.table(temp))
  # export factors from session 1
  write.csv(data.frame(temp), here::here(outputf, "questionnaires", "covid_factors_overview.csv")) 
  
  
  ### decided factor names
  # ML1: Economic_Impact_Worry
  # ML2: Covid-related_Worry
  # ML3: Close_Person_Worry
  # ML4: Prob_Infected
  # ML5: Covid-related_Anxiety_Avoidance
  # ML6: Covid-skepticism

  
  

  ## usig FA from Sess 1 - get factor scores for all sessions
  
  for (s in sessions) {
    prefix = "";sid = s
    if (str_contains(s, "extra")) { prefix="extra_"; sid=substr(s, nchar(s), nchar(s)) } 
    df.reduced  <- df.allsess[df.allsess$sid %in% s,] #on standardized data
    
    # This line gets the factor scores using FA from sess 1 for the current session 
    # factor.scores(df.reduced[,colnames(df.reduced ) %in% as.character(vars2use)], fa3)$scores
    # vars2use is a vector of questions to be used in the analysis 
    
    # Using regression (z-scores output betas)
    #fa3.scores <- cbind(df.reduced[,c("SESSIONID", "PROLIFICID")],  data.frame(factor.scores(df.reduced[,colnames(df.reduced ) %in% as.character(vars2use)], fa3, method="Thurstone")$scores)) #https://www.personality-project.org/r/html/factor.scores.html
    
    
    # using component loadings (without estimation of weights - I assume this is just multiplication)
    fa3.scores <- cbind(df.reduced[,c("SESSIONID", "PROLIFICID")],  data.frame(factor.scores(as.matrix(df.reduced[,colnames(df.reduced ) %in% as.character(vars2use)]), fa3, method="components")$scores) )

    
    write.csv(fa3.scores, paste0(dataf, "/", prefix, "session", sid, "/factor_scores.csv"), row.names = FALSE)
    
    
  }

}


# ### Methods 2 Apply averaged ratings  to data and record
# #- without zscoring, variability in large questions drives the factor scores and all factor look the same on temporal plot
# - zscoring within session makes the plots completely flat 
# - normalize to 0-1 ranage (not z-score)
# - here we take loadings, multiply the original questionaires and zscore acros FACTORS (didn't work)

# In[ ]:


# this bit avergaes over factors 
do.th2 <- 0

  if (do.th2 == 1) {

  sessions = c("1", "2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15")
  #sessions = c( "1")
  
  library(dplyr)
  
  for (s in sessions) {
    
    
    prefix = "";sid = s
    if (str_contains(s, "extra")) { prefix="extra_"; sid=substr(s, nchar(s), nchar(s)) } 
    df<-read.csv(paste0(dataf, "/", prefix, "session", sid, "/clean_dataset.csv"))
    df<-df[unique(df$PROLIFICID),]
    df.reduced <- df[,c("PROLIFICID", "SESSIONID")]
    rownames(df) <- df$PROLIFICID
    for(i in vars2use){
      df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
      #df[,i] <- (df[,i] - mean(df[,i])) / sd(df[,i])
    }
    df.2<-df[,colnames(df) %in% c(as.character(vars2use), "PROLIFICID")]
    
    ## df.2 is all raw scores of participants in long format 
    df.2.long <- df.2 %>% 
      melt(id.vars = c("PROLIFICID"),  # variables to carry forward
           # measure.vars = as.character(vars2use),  #variables to stack
           value.name = "score",     # name of the value variable 
           variable.name = "varname" ) # name of the variable  
    
    ## loadings.super.long are the loadings 
    a <- 1
    for (f in 1:nFactors) {
      for (sub in df$PROLIFICID) {
         temp.df <- loadings.mean[loadings.mean$factor %in% paste0("ML", toString(f)), ]
         temp.df$PROLIFICID <- sub
         if (a == 1) {
             loadings.super.long <- temp.df
         } else {
           loadings.super.long <- rbind(loadings.super.long, temp.df)
         }
         a <- a + 1
      }
     df.merge <- merge(df.2.long, loadings.super.long, by=c("varname", "PROLIFICID"))  
     df.merge$loaded_score <- df.merge$score * df.merge$loading
     df.merge.sub <- df.merge %>%
      group_by(PROLIFICID) %>%
      summarise_at(c("loaded_score"), sum, na.rm = TRUE) 
     
     colnames(df.merge.sub)[colnames(df.merge.sub) == 'loaded_score'] <- paste0("ML", toString(f))
    df.reduced <- merge(df.reduced, df.merge.sub, by="PROLIFICID")
    
  
  
  
    }
   cols <- c("ML1", "ML2", "ML3", "ML4")
   
    ndf<-df.reduced[,cols]
    ndf2<-  t(scale(t(ndf)))
    ndf<-apply(ndf, 2, scale)
    for(i in 1:dim(ndf)[1]){
      ndf[i,] <- (ndf[i,] - mean(as.numeric(ndf[i,]))) / sd(as.numeric(ndf[i,]))
    }
    # score df.reduced[,c("ML1", "ML2", "ML3", "ML4")]
    ndf <- cbind(ndf,df.reduced[,c("PROLIFICID", "SESSIONID")])
    #write.csv(ndf, paste0(dataf, "/", prefix, "session", sid, "/factor_scores.csv"), row.names = FALSE)
    
  
    
  }

}  




# This bit uses normalization across all sessions (to preserve session variability)

# In[ ]:


sessions = c("1", "2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15")
do.th3 <- 0

  if (do.th3 == 1) {

  for (s in sessions) {
    prefix = "";sid = s
    if (str_contains(s, "extra")) { prefix="extra_"; sid=substr(s, nchar(s), nchar(s)) } 
    #df<-read.csv(paste0(dataf, "/", prefix, "session", sid, "/clean_dataset.csv"))
    df.reduced  <- df.allsess[df.allsess$sid %in% s,]
    #
  
    #for(i in vars2use){
    #  df.reduced[is.na(df.reduced[,i]), i] <- median(df.reduced[,i], na.rm = TRUE)
    #  #df[,i] <- (df[,i] - mean(df[,i])) / sd(df[,i])
    #}
    df.2<-df.reduced[,colnames(df.reduced) %in% c(as.character(vars2use), "PROLIFICID")]
    df.reduced <- df.reduced[,c("PROLIFICID", "SESSIONID")]
    
    ## df.2 is all raw scores of participants in long format 
    df.2.long <- df.2 %>% 
      melt(id.vars = c("PROLIFICID"),  # variables to carry forward
           # measure.vars = as.character(vars2use),  #variables to stack
           value.name = "score",     # name of the value variable 
           variable.name = "varname" ) # name of the variable  
    
    ## loadings.super.long are the loadings 
    a <- 1
    for (f in 1:nFactors) {
      for (sub in df.reduced$PROLIFICID) {
         temp.df <- loadings.mean[loadings.mean$factor %in% paste0("ML", toString(f)), ]
         temp.df$PROLIFICID <- sub
         if (a == 1) {
             loadings.super.long <- temp.df
         } else {
           loadings.super.long <- rbind(loadings.super.long, temp.df)
         }
         a <- a + 1
      }
     df.merge <- merge(df.2.long, loadings.super.long, by=c("varname", "PROLIFICID"))  
     df.merge$loaded_score <- df.merge$score * df.merge$loading
     df.merge.sub <- df.merge %>%
      group_by(PROLIFICID) %>%
      summarise_at(c("loaded_score"), sum, na.rm = TRUE) 
     
     colnames(df.merge.sub)[colnames(df.merge.sub) == 'loaded_score'] <- paste0("ML", toString(f))
    df.reduced <- merge(df.reduced, df.merge.sub, by="PROLIFICID")
    
    
  
  
    }
    write.csv(df.reduced, paste0(dataf, "/", prefix, "session", sid, "/factor_scores.csv"), row.names = FALSE)
  
    
  
    
  }

  
}



# In[ ]:


i<-1
for (i in 1:nFactors) {
 # Label questions loading higer than 0.1
 df.all.long$current_fac <- 0
 df.all.long$current_fac[df.all.long$factor %in% paste0("ML",toString(i)) & abs(df.all.long$loading) > 0.2] <- 1
 
 # Select data
 tdf <- df.all.long[df.all.long$factor %in% paste0("ML",toString(i)),]
 g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(sessid))) +
 geom_bar(position="stack", stat="identity", colour="black") +
# ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"") +
 #scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
 theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
 scale_colour_distiller(
  type = "seq",
  palette = 1,
  direction = -1,
  values = NULL,
  space = "Lab",
  na.value = "grey50",
  guide = "colourbar",
  aesthetics = "colour"
 )
 # facet_grid(rows=vars(sessid))
 print(g)
 
 
}

