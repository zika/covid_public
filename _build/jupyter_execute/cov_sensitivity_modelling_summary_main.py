#!/usr/bin/env python
# coding: utf-8

# ## Main modelling summary 
# ### Model
# $$
# Worry \sim  session + Severity + TF_1 + TF_1*Severity  + TF_2 + TF_2*Severity + (session|subject) 
# $$

# In[1]:


from groo.groo import get_root
root_dir = get_root(".root_dir_covid")
import os, sys, json
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))
import numpy as np
import pandas as pd
from cov_functions import *
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
import warnings
import itertools
warnings.filterwarnings('ignore')
from scipy.spatial.distance import correlation as Dcorr
import bambi as bmb
import arviz as az
import pickle
import bammm_local as mm

# load main dataset
df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete.csv"))
factors =[ "F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism"]
print("State seveirty NaNs: "+str(df["state_severity"].isna().sum())+" out of entries: "+ str(df.shape[0]))

no_trait_factors = 5

if no_trait_factors == 6:
        trait_factor_names = ["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing",  "TF4_Physiological_Anx",  "TF5_Depression",  "TF6_Cognitive_Anx"]
elif no_trait_factors==5:
    trait_factor_names = ["TF1_Worry", "TF2_Self_Worth", "TF3_Catastrophizing",  "TF4_Anxiety",  "TF5_Depression"]



# In[2]:


# these are effects estimated by MLE in R
# generated at the bottom of sensitivity_trait_on_rs_two_models_f5.Rmd
td = pd.read_csv(os.path.join(root_dir, "output", "r_models", "overall_severity_models.csv"))



# In[ ]:





# In[3]:


models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r")) 
ylims = [[-0.7,0.7], [-0.15, 0.15], [-0.02, 0.02]]
folder = "traits_predicting_severity_correlations"
cats = ["low", "high"]
cats_tr = ["low", "mid", "high"]
print(len(cats))
for tt in trait_factor_names:
    splitvar = tt+"_ms" #"stai_ta_ms" #"TF4_Physiological_Anx_ms" # "stai_ta_ms" 
    data = df.groupby(["session", splitvar])[[ "state_severity"]+factors].mean()
    
    splitvar_tr = tt+"_ts" #"stai_ta_ms" #"TF4_Physiological_Anx_ms" # "stai_ta_ms" 
    data_tr = df.groupby(["session", splitvar_tr])[[ "state_severity"]+factors].mean()

    #plt.subplots_adjust(wspace=0.4)
    for f_idx, f in enumerate(factors):
        plt.figure()
        fig, ax = plt.subplots(ncols=3,nrows=1,figsize=(12,3), gridspec_kw={'width_ratios': [2,1, 1]})
        g = sns.lineplot(data=data, x="session", y="state_severity", hue=splitvar, hue_order=cats, palette=sns.color_palette("mako_r", len(cats)), ax=ax[0])
        g.set_title(f + " by "+tt)
        g.set_ylabel(f)
        sns.move_legend(ax[0], "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
        if f_idx == 0:
            g.set_ylabel("State severity (dashed)\n"+f+"(full)")
        for lidx, l in enumerate(ax[0].lines):
            ax[0].lines[lidx].set_linestyle("--")
        g2 = ax[0].twinx()
        g = sns.lineplot(data=data, x="session", y=f, hue=splitvar, hue_order=cats, palette=sns.color_palette("mako_r", len(cats)), ax=g2)
        sns.move_legend(g2, "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
        #sns.move_legend(g2, "lower center", bbox_to_anchor=(.5, 1), ncol=3, title=None, frameon=False)
        g.set_ylabel("")
        

        model_type_plot = "r"
        if model_type_plot=="pymc":
            ## regression fits 
            model_constant = "severity_onlysession_slope_f5"
            mod = models["F"+str(f_idx+1)+"_"+model_constant+"_4_2000"]
            # assumes that models have been estimated
            mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
            mod, res,m  = mm.estimate_lmm(mod, [], override=0)
            beta_group = ["state_severity", tt, "state_severity:"+tt]
            tdf = pd.DataFrame()
            for tf in beta_group:
                d = res.posterior[tf].stack(draws=("chain", "draw"))
                dt = pd.DataFrame(d, columns=["sample"])
                dt.loc[:,"trait_factor"] = tf
                tdf = pd.concat([tdf, dt])

            ax[1] = sns.pointplot(x="trait_factor", y= "sample", x_order = beta_group,
                          data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                          markers="d", scale=.75, ci=None, ax =ax[1]);
            bounds = tdf.groupby("trait_factor")['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
            bounds = bounds.set_index("trait_factor", drop=False)
            bounds = bounds.loc[beta_group]
            ax[1].vlines(bounds.trait_factor, bounds.iloc[:,1], bounds.iloc[:,2], colors='black')
            ax[1].axhline(0, alpha=0.2, color='gray')
            ax[1].set_title(f+"\n(regression coefs)")
            ax[1].set_ylabel("beta (ci95)")
            ax[1].set_xlabel("")
            ax[1].set_xticklabels(beta_group, rotation = 30, ha="right")
            #ax[1].set_ylim(ylims[1])
            ax[1].set_xlim([-0.5,2.5])

            ## regression fits by half
            model_constant = "severity_noslope_"
            mod1 = models["F"+str(f_idx+1)+"_"+model_constant+"early_f5_4_3000"]
            # assumes that models have been estimated
            mod1["current_sys_location"] = os.path.join(root_dir, mod1["location"])
            mod1, res1,m  = mm.estimate_lmm(mod1, [], override=0)

            mod2 = models["F"+str(f_idx+1)+"_"+model_constant+"late_f5_4_3000"]
            # assumes that models have been estimated
            mod2["current_sys_location"] = os.path.join(root_dir, mod2["location"])
            mod2, res2,m  = mm.estimate_lmm(mod2, [], override=0)

            tdf = pd.DataFrame()
            for tf in beta_group:
                d = res1.posterior[tf].stack(draws=("chain", "draw"))
                dt = pd.DataFrame(d, columns=["sample"])
                dt.loc[:,"trait_factor"] = tf
                dt.loc[:,"timewindow"] = "early"
                tdf = pd.concat([tdf, dt])

                d = res2.posterior[tf].stack(draws=("chain", "draw"))
                dt = pd.DataFrame(d, columns=["sample"])
                dt.loc[:,"trait_factor"] = tf
                dt.loc[:,"timewindow"] = "late"
                tdf = pd.concat([tdf, dt])
            ax[2] = sns.pointplot(x="trait_factor", y= "sample", hue="timewindow", hue_order=["early", "late"],
                          data=tdf, dodge=0.2, join=False, palette=sns.color_palette("mako",6),
                          markers="d", scale=.75, ci=None, ax =ax[2]);
            bounds = tdf.groupby(["trait_factor", "timewindow"])['sample'].quantile((0.025,0.975)).unstack().reset_index().sort_values(by="trait_factor")
            for iidx, i in enumerate(["early", "late"]):    
                bounds_loc = bounds.loc[bounds["timewindow"].isin([i]),:]
                bounds_loc = bounds_loc.set_index("trait_factor", drop=False)
                bounds_loc = bounds_loc.loc[beta_group]
                x_base = np.arange(len(bounds_loc.trait_factor))
                ax[2].vlines(-0.1+x_base+0.2*iidx, bounds_loc.iloc[:,2], bounds_loc.iloc[:,3], colors='black')
            ax[2].axhline(0, alpha=0.2, color='gray')
            ax[2].set_title(f+"\n(regression coefs by half)")
            ax[2].set_ylabel("beta (ci95)")
            ax[2].set_xlabel("")
            ax[2].set_xticklabels(beta_group, rotation = 30, ha="right")
            ax[2].set_xlim([-0.5,2.5])
            
            
        elif model_type_plot=="r":
            beta_group = ["state_severity", tt, "state_severity:"+tt]           
            tdf = td.loc[(td["model_name"].isin(["severity_interactions_model_f5"])) & (td["dv"].isin([f])) & (td["effect"].isin(beta_group)),]
            ax[1] = sns.pointplot(x="effect", y= "Estimate", x_order = beta_group,
                      data=tdf, dodge=.532, join=False, palette=sns.color_palette("mako",6),
                      markers="d", scale=.75, ci=None, ax =ax[1])
            ax[1].vlines(tdf.effect, tdf["CI95_low"], tdf["CI95_high"], colors='black')
            ax[1].axhline(0, alpha=0.2, color='gray')
            ax[1].set_title(f+"\n(regression coefs)")
            ax[1].set_ylabel("beta (ci95)")
            ax[1].set_xlabel("")
            ax[1].set_xticklabels(beta_group, rotation = 30, ha="right")
            ax[1].set_xlim([-0.5,2.5])
            
            tdf = td.loc[(td["model_name"].isin(["severity_interactions_model_byhalf_f5"])) & (td["dv"].isin([f])) & (td["effect"].isin(beta_group)),]
            
            ax[2] = sns.pointplot(x="effect", y= "Estimate", hue="cond", hue_order=["early", "late"],
                          data=tdf, dodge=0.2, join=False, palette=sns.color_palette("mako",6),
                          markers="d", scale=.75, ci=None, ax =ax[2]);

            for iidx, i in enumerate(["early", "late"]):    
                tdff = tdf.loc[tdf["cond"].isin([i]),]                
                x_base = np.arange(len(tdff.effect))
                ax[2].vlines(-0.1+x_base+0.2*iidx, tdff["CI95_low"], tdff["CI95_high"], colors='black')   
                
            ax[2].axhline(0, alpha=0.2, color='gray')
            ax[2].set_title(f+"\n(regression coefs by half)")
            ax[2].set_ylabel("beta (ci95)")
            ax[2].set_xlabel("")
            ax[2].set_xticklabels(beta_group, rotation = 30, ha="right")
            ax[2].set_xlim([-0.5,2.5])
            
        
        plt.figure()
        fig, ax = plt.subplots(ncols=3,nrows=1,figsize=(12,3), gridspec_kw={'width_ratios': [2,1, 1]})
        g = sns.lineplot(data=data_tr, x="session", y="state_severity", hue=splitvar_tr, hue_order=cats_tr, palette=sns.color_palette("mako_r", len(cats_tr)), ax=ax[0])
        g.set_title(f + " by "+tt)
        g.set_ylabel(f)
        sns.move_legend(ax[0], "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
        if f_idx == 0:
            g.set_ylabel("State severity (dashed)\n"+f+"(full)")
        for lidx, l in enumerate(ax[0].lines):
            ax[0].lines[lidx].set_linestyle("--")
        g2 = ax[0].twinx()
        g = sns.lineplot(data=data_tr, x="session", y=f, hue=splitvar_tr, hue_order=cats_tr, palette=sns.color_palette("mako_r", len(cats_tr)), ax=g2)
        sns.move_legend(g2, "lower center", bbox_to_anchor=(0.55, 0.75), ncol=3, frameon=False)
        #sns.move_legend(g2, "lower center", bbox_to_anchor=(.5, 1), ncol=3, title=None, frameon=False)
        g.set_ylabel("")
        
        sns.regplot(x=tt, y=f, data=df.loc[df["session"].isin([5]),:], ax=ax[1]);
        ax[1].set_title(["session 5"])
        
        sns.regplot(x=tt, y=f, data=df.loc[df["session"].isin([15]),:], ax=ax[2]);
        ax[2].set_title(["session 15"])

        


# In[4]:


## check by removing variance of state severity 


# In[5]:


bounds


# In[13]:


import statsmodels.formula.api as smf
f = "F4_Prob_Estimates"
#tt = "TF6_Cognitive_Anx"
tt = "TF2_Self_Worth"
tdf = df.loc[df["session"].isin([0]),[f, "state_severity", tt, tt+"_ms"]].dropna()
md = smf.ols(f+" ~ state_severity", data=tdf)
res = md.fit()
tdf["stsev"] = res.predict()
tdf[f+"_regout"] = tdf[f] - tdf["stsev"]

fig, ax = plt.subplots(1,2, figsize=(10,7))
sns.regplot(x=tt, y=f, data=tdf, ax=ax[0])
corrfunc(tdf[tt], tdf[f], tests=["pearson", "spearman"], drop_missing=True, ax=ax[0])
ax[0].set_title("with state severity")

sns.regplot(x=tt, y=f+"_regout", data=tdf, ax=ax[1])
corrfunc(tdf[tt], tdf[f+"_regout"], tests=["pearson", "spearman"], drop_missing=True, ax=ax[1])
ax[1].set_title("state severity regressed out")




# In[ ]:


data = df.groupby(["session", "TF2_Positive_ms"])[[ "state_severity"]+factors].mean()
data = df.groupby(["session", "TF2_Positive_ms"])[factors].mean()
#data = df.groupby(["session", "TF2_Positive_ms"])[f].apply(lambda x: x.mean(skipna=True))
data
tdf = df.loc[df["session"].isin([0]),[f, "state_severity", tt, tt+"_ms"]]
tdf["TF2_Positive_ms2"] = pd.qcut(tdf["TF2_Positive"], [0, .5, 1],labels=['low', 'high'])
print(np.median(tdf["TF2_Positive"]))
print(np.median(df["TF2_Positive"]))
#plt.figure()
#np.min(df.loc[tdf["TF2_Positive_ms2"].isin(["high"]), "TF2_Positive"])
tdf["comparison"] = (tdf["TF2_Positive_ms2"] == tdf["TF2_Positive_ms"])
#tdf[f].groupby("TF2_Positive_ms2").mean()

print(tdf.groupby(["TF2_Positive_ms2"])[f].apply(lambda x: x.mean(skipna=True)))
print(tdf.groupby(["TF2_Positive_ms"])[f].apply(lambda x: x.mean(skipna=True)))

data

